<?php

namespace Application\Model;

class Inspection
{
    public $id;
    public $buyerId;
    public $createdDate;

    public $sellerName;
    public $sellerNamePicture;
    public $sellerId;
    public $sellerIdPicture;
    public $sellerGender;
    public $sellerPhoneNumber;

    public $vehicleStreetAddress;
    public $vehicleCity;
    public $vehicleState;
    public $vehicleZip;
    public $vehicleFullAddress;

    public $vehicleYear;
    public $vehicleMake;
    public $vehicleModel;
    public $vehicleColor;

    public $vehicleVin;
    public $vehicleHasTitle;
    public $vehicleTitle;

    public $vehicleWheelType;
    public $vehicleDoors;
    public $vehiclePurchasePrice;

    public $notes;
    public $pickUpNotes;

    public $totalTowCost;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = $data['Inspection_ID'] ?? null;
        $this->buyerId = $data['Buyer_ID'] ?? null;
        $this->createdDate = $data['DateVal'] ?? null;

        $this->sellerName = $data['SellerName'] ?? null;
        $this->sellerNamePicture = $data['SIG_Img_Filename'] ?? null;
        $this->sellerId = $data['SellerID'] ?? null;
        $this->sellerIdPicture = $data['DL_Img_Filename'] ?? null;
        $this->sellerGender = $data['SellerGender'] ?? null;
        $this->sellerPhoneNumber = $data['Phone'] ?? null;

        $this->vehicleStreetAddress = $data['StreetAddress'] ?? null;
        $this->vehicleCity = $data['City'] ?? null;
        $this->vehicleState = $data['State'] ?? null;
        $this->vehicleZip = $data['Zip'] ?? null;
        $this->vehicleFullAddress = $data['FullAddress'] ?? null;

        $this->vehicleYear = $data['_Year'] ?? null;
        $this->vehicleMake = $data['Make'] ?? null;
        $this->vehicleModel = $data['Model'] ?? null;
        $this->vehicleColor = $data['Color'] ?? null;

        $this->vehicleVin = $data['VIN'] ?? null;
        $this->vehicleHasTitle = $data['Title'] ?? null;
        $this->vehicleTitle = $data['TitleNum'] ?? null;

        $this->vehicleWheelType = $data['Wheels'] ?? null;
        $this->vehicleDoors = $data['Doors'] ?? null;
        $this->vehiclePurchasePrice = $data['PurchAmt'] ?? null;

        $this->notes = $data['Notes'] ?? null;
        $this->pickUpNotes = $data['PickupNotes'] ?? null;

        $this->totalTowCost = $data['TowCost'] ?? null;
    }

    public function toDbArrayForUpdate()
    {
        return [
            'Buyer_ID' => $this->buyerId,
            'DateVal' => $this->createdDate,
            '_Date' => $this->createdDate,

            'SellerName' => $this->sellerName,
            'SIG_Img_Filename' => $this->sellerNamePicture,
            'SellerID' => $this->sellerId,
            'DL_Img_Filename' => $this->sellerIdPicture,
            'SellerGender' => $this->sellerGender,
            'Phone' => $this->sellerPhoneNumber,

            'StreetAddress' => $this->vehicleStreetAddress,
            'City' => $this->vehicleCity,
            'State' => $this->vehicleState,
            'Zip' => $this->vehicleZip,
            'FullAddress' => $this->vehicleFullAddress,

            '_Year' => $this->vehicleYear,
            'Make' => $this->vehicleMake,
            'Model' => $this->vehicleModel,
            'Color' => $this->vehicleColor,

            'VIN' => $this->vehicleVin,
            'Title' => $this->vehicleHasTitle,
            'TitleNum' => $this->vehicleTitle,

            'Wheels' => $this->vehicleWheelType,
            'Doors' => $this->vehicleDoors,
            'PurchAmt' => $this->vehiclePurchasePrice,

            'Notes' => $this->notes,
            'PickupNotes' => $this->pickUpNotes,

            'TowCost' => $this->totalTowCost ?? 0.0,

            'Incomplete' => '0',
        ];
    }

    public function toDbArrayForInsert()
    {
        $fullRow = array_merge(
            $this->toDbArrayForUpdate(),
            [
                'LeadSource' => '',
                'KeysLoc' => '',
                'DatePmtRecd' => '0000-00-00',
                'DMV_Fees' => 0.0,
                'OtherFeesNotes' => 0,
                'OtherFeesAmt' => 0,
                'LeadCost' => 0,
                'BuyerPay' => 0.0,
                'DateTowPaid' => '0000-00-00',
                'TowPaidCode' => '',
                'TowCompany' => '', // TODO: See if it makes sense to populate this
                'TowStatus' => '',
                'TowID' => '',
                'EndBuyer' => '',
                'SellPrice' => 0.0,
                'NOI' => 0.0,
                'NOI_pct' => 0.0,
            ]
        );

        return $fullRow;
    }
}
