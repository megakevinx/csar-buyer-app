<?php

namespace Application\Model;

class TowStatus
{
    public $id;
    public $inspectionId;
    public $createdTime;
    public $isHighPriorityTow;
    public $towFromLocation;
    public $towToLocation;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = $data['TowStatus_ID'] ?? null;
        $this->inspectionId = $data['Inspection_ID'] ?? null;
        $this->createdTime = $data['OrderTime'] ?? null;
        $this->isHighPriorityTow = $data['ER'] ?? null;
        $this->towFromLocation = $data['TowFrom'] ?? null;
        $this->towToLocation = $data['TowTo'] ?? null;
    }

    public function toDbArray()
    {
        return [
            'Inspection_ID' => $this->inspectionId,
            'OrderTime' => $this->createdTime,
            'ER' => $this->isHighPriorityTow,
            'TowFrom' => $this->towFromLocation,
            'TowTo' => $this->towToLocation,

            'Buyer_ID' => 0,
            'Assigned' => '0000-00-00 00:00:00',
            'Amount' => 0,

            'Accepted' => '0000-00-00 00:00:00',
            'Started' => '0000-00-00 00:00:00',
            'PickedUp' => '0000-00-00 00:00:00',
            'DroppedOff' => '0000-00-00 00:00:00',
            'PushedIn' => '0000-00-00 00:00:00',
            'TicketNumber' => '',
            'DatePaid' => '0000-00-00',
            'PayRef' => ''
        ];
    }
}
