<?php

namespace Application\Model;

class Photo
{
    public $id;
    public $inspectionId;
    public $fileName;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = $data['Photo_ID'] ?? null;
        $this->inspectionId = $data['Inspection_ID'] ?? null;
        $this->fileName = $data['ImageFilePath'] ?? null;
    }

    public function toDbArray()
    {
        return [
            'Inspection_ID' => $this->inspectionId,
            'ImageFilePath' => $this->fileName,
        ];
    }
}
