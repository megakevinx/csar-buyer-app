<?php

namespace Application\Model;

class TowDriver
{
    public $id;
    public $name;
    public $towCompanyId;
    public $towCompanyName;

    public function exchangeArray(array $data)
    {
        $this->id = $data['Buyer_ID'] ?? null;
        $this->name = $data['BuyerName'] ?? null;
        $this->towCompanyId = $data['TowCo_ID'] ?? null;
        $this->towCompanyName = $data['CompanyName'] ?? null;
    }
}
