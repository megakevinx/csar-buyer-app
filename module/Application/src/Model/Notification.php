<?php

namespace Application\Model;

class Notification
{
    public $id;
    public $type;
    public $status;
    public $destination;
    public $scheduled;
    public $subject;
    public $content;

    public function exchangeArray(array $data)
    {
        $this->id = $data['Notification_ID'] ?? null;
        $this->type = $data['NotificationType'] ?? null;
        $this->status = $data['NotificationStatus'] ?? null;
        $this->destination = $data['Destination'] ?? null;
        $this->scheduled = $data['Scheduled'] ?? null;
        $this->subject = $data['Subject'] ?? null;
        $this->content = $data['Content'] ?? null;
    }
}
