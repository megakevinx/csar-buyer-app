<?php

namespace Application\Model;

class TowPricingRule
{
    public $id;
    public $towCompanyId;
    public $countyId;
    public $towFromLocation;
    public $towToLocation;
    public $towPrice;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->id = $data['TowPricingRule_ID'] ?? null;
        $this->towCompanyId = $data['TowCompany_ID'] ?? null;
        $this->countyId = $data['Area_ID'] ?? null;
        $this->towFromLocation = $data['TowFromLocation'] ?? null;
        $this->towToLocation = $data['TowToLocation'] ?? null;
        $this->towPrice = $data['TowPrice'] ?? null;
    }
}
