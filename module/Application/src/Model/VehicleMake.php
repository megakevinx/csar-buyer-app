<?php

namespace Application\Model;

class VehicleMake
{
    public $id;
    public $name;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['VehicleMake_ID']) ? $data['VehicleMake_ID'] : null;
        $this->name = !empty($data['Name']) ? $data['Name'] : null;
    }
}
