<?php

namespace Application\Model;

class TowCompany
{
    public $id;
    public $name;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['TowCo_ID']) ? $data['TowCo_ID'] : null;
        $this->name = !empty($data['CompanyName']) ? $data['CompanyName'] : null;
    }
}
