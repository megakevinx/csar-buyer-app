<?php

namespace Application\Service;

use Application\Service\LocationService;
use Application\Repository\TowPricingRuleRepository;

class TowPriceCalculator
{
    private $towPricingRuleRepository;
    private $locationService;

    public function __construct(TowPricingRuleRepository $towPricingRuleRepository, LocationService $locationService)
    {
        $this->towPricingRuleRepository = $towPricingRuleRepository;
        $this->locationService = $locationService;
    }

    public function calculateTowPrice($zipCode, $towCompanyId, $from, $to)
    {
        $county = $this->locationService->getCountyForZip($zipCode);

        if ($county) {
            $rule = $this->towPricingRuleRepository->getBy($towCompanyId, $county->id, $from, $to);

            if ($rule) {
                return $rule->towPrice;
            }
        }

        $rule = $this->towPricingRuleRepository->getBy(null, null, $from, $to);

        if ($rule) {
            return $rule->towPrice;
        }

        return 0.00;
    }
}