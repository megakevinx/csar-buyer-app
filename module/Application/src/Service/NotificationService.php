<?php

namespace Application\Service;

use Application\Repository\NotificationRepository;
use Application\Repository\BuyerRepository;
use Application\Repository\TowCompanyRepository;

use Application\Model\Notification;
use Application\Model\NotificationType;
use Application\Model\NotificationStatus;

class NotificationService
{
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    const DEFAULT_STATE = "FL";

    const TOW_ORDER_EMAIL_DESTINATION = "wbcfast@gmail.com";
    const TOW_ORDER_EMAIL_SUBJECT_FORMAT = "New Tow Order has been created. Ref. Number: %s";
    const TOW_ORDER_EMAIL_CONTENT_FORMAT =
        "A new tow order has been created by Buyer %s.\n" . // Buyer name
        "\n" .
        "- Customer info -\n" .
        "Name: %s\n" . // customer name
        "ID: %s\n" . // customer id
        "Phone: %s\n" . // customer phone
        "Address: %s %s, %s %s\n" . // street addr, city, state, zip
        "\n" .
        "- Vehicle info -\n" .
        "Description: %s %s %s\n" . // year, make, model
        "Color: %s\n" .
        "VIN: %s\n" .
        "Title: %s\n" . // title if has title else "does not have"
        "Wheels: %s\n" .
        "Doors: %s\n" .
        "Purchase price: %s\n" .
        "\n" .
        "- Notes -\n" .
        "\n" .
        "%s\n" . // pickup notes
        "%s - %s - %s - %s\n" . // extra notes
        "\n" .
        "Ref. Number: %s\n";

    private $notificationRepository;
    private $buyerRepository;
    private $towCompanyRepository;

    public function __construct(NotificationRepository $notificationRepository, BuyerRepository $buyerRepository, TowCompanyRepository $towCompanyRepository)
    {
        $this->notificationRepository = $notificationRepository;
        $this->buyerRepository = $buyerRepository;
        $this->towCompanyRepository = $towCompanyRepository;
    }

    private function createNotification(string $type, string $destination, string $subject, string $content)
    {
        $notification = new Notification();

        $notification->status = NotificationStatus::$PENDING;
        $notification->scheduled = date(self::DATE_TIME_FORMAT);

        $notification->type = $type;
        $notification->destination = $destination;
        $notification->subject = $subject;
        $notification->content = $content;

        return $this->notificationRepository->saveNotification($notification);
    }

    private function getSuccessResult(array $createdNotifications)
    {
        return (object)[
            'isSuccess' => true,
            'createdNotifications' => $createdNotifications
        ];
    }

    public function createTowOrderNotification(array $data)
    {
        $createdNotifications = [];

        $towCaptain = $this->buyerRepository->getTowCaptain();
        $buyerCreator = $this->buyerRepository->get($data["createdByUserId"]);

        if (!$buyerCreator && !$towCaptain) {
            return (object)[
                'isSuccess' => false,
                'createdNotifications' => null,
                'errorMessage' =>
                    "Could not create the notifications. Either the buyer or tow company does not exist."
            ];
        }

        if ($towCaptain) {
            $createdNotifications[] = $this->createNotification(
                NotificationType::$SMS,
                $towCaptain->phoneNumber ?? "",
                "",
                "TOW ORDER - " . $towCaptain->firstName . "\nhttp://wbcfast.com/CSV2/tow/tow_login.php"
            );
        }

        if ($buyerCreator) {
            $createdNotifications[] = $this->createNotification(
                NotificationType::$EMAIL,
                self::TOW_ORDER_EMAIL_DESTINATION,
                vsprintf(self::TOW_ORDER_EMAIL_SUBJECT_FORMAT, [ $data['inspectionId'] ]),
                vsprintf(self::TOW_ORDER_EMAIL_CONTENT_FORMAT, [
                    $buyerCreator->firstName,
                    $data['customerName'],
                    $data['customerId'],
                    $data['customerPhone'],
                    $data['customerStreetAddress'], $data['customerCity'], self::DEFAULT_STATE, $data['customerZip'],

                    $data['vehicleYear'], $data['vehicleMake'], $data['vehicleModel'],
                    $data['vehicleColor'],
                    $data['vehicleVin'],
                    $data['vehicleHasTitle'] ? $data['vehicleTitle'] : "Doesn't have",
                    $data['vehicleWheelType'],
                    $data['vehicleBodyStyle'],
                    $data['vehiclePurchasePrice'],

                    $data['pickUpNotes'],
                    $data['customerDataNotes'],
                    $data['vehicleDescriptionNotes'],
                    $data['vehicleIdentificationNotes'],
                    $data['vehiclePurchaseDetailsNotes'],

                    $data['inspectionId']
                ])
            );
        }

        return $this->getSuccessResult($createdNotifications);
    }
}
