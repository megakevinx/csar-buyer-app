<?php

namespace Application\Service;

use Application\Repository\TowStatusRepository;
use Application\Repository\TowDriverRepository;
use Application\Repository\InspectionRepository;

use Application\Model\TowStatus;
use Application\Model\TowDriver;

class TowStatusService
{
    const DATE_TIME_FORMAT = "Y-m-d H:i:s";
    const DEFAULT_TOW_FROM_LOCATION = "Customer";
    const DEFAULT_TOW_TO_LOCATION = "LOT";

    private $towStatusRepository;
    private $towDriverRepository;
    private $inspectionRepository;

    public function __construct(
        TowStatusRepository $towStatusRepository,
        TowDriverRepository $towDriverRepository,
        InspectionRepository $inspectionRepository
    ) {
        $this->towStatusRepository = $towStatusRepository;
        $this->towDriverRepository = $towDriverRepository;
        $this->inspectionRepository = $inspectionRepository;
    }

    public function createTowStatus(array $data)
    {
        $towStatusToSave = new TowStatus();

        $towStatusToSave->inspectionId = $data['inspectionId'];
        $towStatusToSave->createdTime = date(self::DATE_TIME_FORMAT);
        $towStatusToSave->isHighPriorityTow = $data['isHighPriorityTow'] == "yes" ? 1 : 0;
        $towStatusToSave->towFromLocation = self::DEFAULT_TOW_FROM_LOCATION;
        $towStatusToSave->towToLocation = self::DEFAULT_TOW_TO_LOCATION;

        $savedTowStatus = $this->towStatusRepository->save($towStatusToSave);

        return (object)[
            'isSuccess' => true,
            'savedEntity' => $savedTowStatus
        ];
    }

    public function getLastByInspectionId(string $inspectionId)
    {
        $towStatus = $this->towStatusRepository->getLastByInspectionId($inspectionId);

        if (! $towStatus) {
            return (object)[
                'isSuccess' => false,
                'towStatus' => null
            ];
        }

        if ($towStatus->buyerId) {
            $towDriver = $this->towDriverRepository->getByBuyerId($towStatus->buyerId);

            if ($towDriver) {
                $towStatus->towCompanyId = $towDriver->towCompanyId;
            }
        }

        return (object)[
            'isSuccess' => true,
            'towStatus' => $towStatus
        ];
    }

    public function getAllByInspectionId(string $inspectionId)
    {
        $towStatuses = $this->towStatusRepository->getAllByInspectionId($inspectionId);

        if (! $towStatuses) {
            return (object)[
                'isSuccess' => false,
                'towStatuses' => null
            ];
        }

        return (object)[
            'isSuccess' => true,
            'towStatuses' => $towStatuses
        ];
    }
}
