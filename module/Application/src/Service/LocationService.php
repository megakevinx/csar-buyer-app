<?php

namespace Application\Service;

use Application\Repository\ZipRepository;
use Application\Repository\CountyRepository;

class LocationService
{
    // TODO: Move to configuration
    const SUPPORTED_COUNTIES = [
        'hernando',
        'polk',
        'pasco',
        'hillsborough',
        'pinellas',
        'bradenton',
        'manatee'
    ];

    private $zipRepository;
    private $countyRepository;

    public function __construct(ZipRepository $zipRepository, CountyRepository $countyRepository)
    {
        $this->zipRepository = $zipRepository;
        $this->countyRepository = $countyRepository;
    }

    private function isSupportedCounty($countyName)
    {
        return in_array(strtolower($countyName), self::SUPPORTED_COUNTIES);
    }

    public function getCountyForZip(string $zip)
    {
        $zipCode = $this->zipRepository->getByCode($zip);
        $county = null;

        if ($zipCode)
        {
            $county = $this->countyRepository->get($zipCode->countyId);
        }

        return $county;
    }

    public function isZipSupported(string $zip)
    {
        $isSupported = false;

        $county = $this->getCountyForZip($zip);

        if ($county)
        {
            if ($this->isSupportedCounty($county->name))
            {
                $isSupported = true;
            }
        }

        return $isSupported;
    }
}
