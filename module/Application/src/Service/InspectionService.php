<?php

namespace Application\Service;

use Application\Repository\InspectionRepository;
use Application\Repository\PhotoRepository;

use Application\Model\Inspection;
use Application\Model\Photo;
use Application\Model\User;

class InspectionService
{
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    const DEFAULT_STATE = "FL";
    const PHOTOS_PATH = "../CSV2/Photos/";
    const DATA_URL_IDENTIFIER = "base64,";

    const ADMIN_USER_ROLE = "Administrator";
    const BUYER_USER_ROLE = "Buyer";

    private $inspectionRepository;
    private $photoRepository;

    public function __construct(InspectionRepository $inspectionRepository, PhotoRepository $photoRepository)
    {
        $this->inspectionRepository = $inspectionRepository;
        $this->photoRepository = $photoRepository;
    }

    public function search(array $searchTerms, User $loggedInUser)
    {
        if ($loggedInUser->role == self::ADMIN_USER_ROLE) {
            return [
                'deals' => $this->inspectionRepository->find($searchTerms),
                'totalDealsCount' => $this->inspectionRepository->getCount($searchTerms)
            ];
        } else if ($loggedInUser->role == self::BUYER_USER_ROLE) {
            return [
                'deals' => $this->inspectionRepository->findForBuyer($searchTerms, $loggedInUser->id),
                'totalDealsCount' => $this->inspectionRepository->getCountForBuyer($searchTerms, $loggedInUser->id)
            ];
        }
    }

    public function getInspection(string $inspectionId)
    {
        $inspection = $this->inspectionRepository->getById($inspectionId);

        if (! $inspection) {
            return (object)[
                'isSuccess' => false,
                'inspection' => null
            ];
        }

        if ($inspection->sellerNamePicture) {
            $inspection->sellerNamePicture = $this->getBase64DataUrl($inspection->sellerNamePicture);
        }
        if ($inspection->sellerIdPicture) {
            $inspection->sellerIdPicture = $this->getBase64DataUrl($inspection->sellerIdPicture);
        }

        return (object)[
            'isSuccess' => true,
            'inspection' => $inspection
        ];
    }

    private function getBase64DataUrl(string $fileName)
    {
        try {
            $path = $this->constructFilePath($fileName);
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);

            if ($data === false) {
                error_log("Error while opening picture: " . $fileName);
                return null;
            }

            return 'data:image/' . $type . ';base64,' . base64_encode($data);
        } catch (Exception $e) {
            error_log("Error while opening picture: " . $fileName . ". " . $e->getMessage());
            return null;
        }
    }

    private function handleSave(array $data, callable $getEntityToSave, callable $saveEntity)
    {
        $inspectionToSave = $getEntityToSave();

        if (! $inspectionToSave) {
            return (object)[
                'isSuccess' => false,
                'message' => 'Could not get entity to save'
            ];
        }

        $inspectionToSave->sellerName = $data['customerName'];
        $inspectionToSave->sellerId = $data['customerId'];
        $inspectionToSave->sellerGender = $data['customerGender'];
        $inspectionToSave->sellerPhoneNumber = $data['customerPhone'];

        $inspectionToSave->vehicleStreetAddress = $data['customerStreetAddress'];
        $inspectionToSave->vehicleCity = $data['customerCity'];
        $inspectionToSave->vehicleState = self::DEFAULT_STATE;
        $inspectionToSave->vehicleZip = $data['customerZip'];
        $inspectionToSave->vehicleFullAddress =
            $inspectionToSave->vehicleStreetAddress . ", " . $inspectionToSave->vehicleCity . ", " .
            $inspectionToSave->vehicleState . " " . $inspectionToSave->vehicleZip;

        $inspectionToSave->vehicleYear = $data['vehicleYear'];
        $inspectionToSave->vehicleMake = $data['vehicleMake'];
        $inspectionToSave->vehicleModel = $data['vehicleModel'];
        $inspectionToSave->vehicleColor = $data['vehicleColor'];

        $inspectionToSave->vehicleVin = $data['vehicleVin'];
        $inspectionToSave->vehicleHasTitle = $data['vehicleHasTitle'];
        $inspectionToSave->vehicleTitle = $data['vehicleTitle'];

        $inspectionToSave->vehicleWheelType = $data['vehicleWheelType'];
        $inspectionToSave->vehicleDoors = $data['vehicleBodyStyle'];
        $inspectionToSave->vehiclePurchasePrice = $data['vehiclePurchasePrice'];

        $notes = array_filter(
            [
                $data['customerDataNotes'], $data['vehicleDescriptionNotes'],
                $data['vehicleIdentificationNotes'], $data['vehiclePurchaseDetailsNotes']
            ],
            function ($note) {
                return (! empty($note));
            }
        );

        if ($notes) {
            $inspectionToSave->notes = join(" - ", $notes);
        }

        $inspectionToSave->pickUpNotes = $data['pickUpNotes'] ?? "";

        $fileNamePrefix = date('YmdHis');

        if ($data['customerNamePicture']) {
            $fileName = $this->savePicture("SIG.jpg", $data['customerNamePicture']);

            if ($fileName) {
                $inspectionToSave->sellerNamePicture = $fileName;
            }
        }

        if ($data['customerIdPicture']) {
            $fileName = $this->savePicture("DL.jpg", $data['customerIdPicture']);

            if ($fileName) {
                $inspectionToSave->sellerIdPicture = $fileName;
            }
        }

        $savedInspection = $saveEntity($inspectionToSave);

        $savedPhotos = [];
        if ($data['vehiclePictures']) {
            foreach ($data['vehiclePictures'] as $picture) {
                $fileName = $this->savePicture("PIC.jpg", $picture);

                if ($fileName) {
                    $photoToSave = new Photo();
                    $photoToSave->inspectionId = $savedInspection->id;
                    $photoToSave->fileName = $fileName;

                    $savedPhotos[] = $this->photoRepository->save($photoToSave);
                } else {
                    error_log("Error while saving picture for deal: " .  $savedInspection->id);
                }
            }
        }

        return (object)[
            'isSuccess' => true,
            'savedEntity' => $savedInspection,
            'savedPhotos' => $savedPhotos
        ];
    }

    public function createInspection(array $data)
    {
        return $this->handleSave(
            $data,
            function () use ($data) {
                $inspectionToSave = new Inspection();
                $inspectionToSave->buyerId = $data['createdByUserId'];
                $inspectionToSave->createdDate = date(self::DATE_TIME_FORMAT);

                return $inspectionToSave;
            },
            function ($inspectionToSave) {
                return $this->inspectionRepository->save($inspectionToSave);
            }
        );
    }

    public function updateInspection(array $data)
    {
        return $this->handleSave(
            $data,
            function () use ($data) {
                return $this->inspectionRepository->getById($data['dealNumber']);
            },
            function ($inspectionToSave) {
                return $this->inspectionRepository->update($inspectionToSave);
            }
        );
    }

    private function savePicture(string $fileNameSuffix, string $fileContentsAsBase64DataUrl)
    {
        try {
            $fileName = $this->getFileName($fileNameSuffix);
            $fileContents = $this->getFileContents($fileContentsAsBase64DataUrl);

            file_put_contents($this->constructFilePath($fileName), base64_decode($fileContents));

            return $fileName;
        } catch (Exception $e) {
            error_log("Error while saving picture: " . $e->getMessage());
            return null;
        }
    }

    // Strips down the metadata prefix from the incoming base 64 data url
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs
    private function getFileContents(string $base64DataUrl)
    {
        return explode(self::DATA_URL_IDENTIFIER, $base64DataUrl)[1];
    }

    private function getFileName(string $suffix)
    {
        return date('YmdHis') . "_" . uniqid() . "_" . $suffix;
    }

    private function constructFilePath(string $fileName)
    {
        return self::PHOTOS_PATH . $fileName;
    }
}
