<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\Photo;

class PhotoRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(Photo $photo)
    {
        $photoData = $photo->toDbArray();

        $this->tableGateway->insert($photoData);
        $photo->id = $this->tableGateway->lastInsertValue;

        return $photo;
    }
}
