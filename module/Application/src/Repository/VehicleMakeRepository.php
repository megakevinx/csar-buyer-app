<?php

namespace Application\Repository;

use Zend\Db\TableGateway\TableGatewayInterface;

class VehicleMakeRepository extends BaseRepository
{
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->setTableGateway($tableGateway);
    }
}
