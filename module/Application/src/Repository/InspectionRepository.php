<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;

use Application\Model\Inspection;

class InspectionRepository
{
    const RECENT_DEAL_DAY_THRESHOLD = "2";

    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getById(string $inspectionId)
    {
        $resultSet = $this->tableGateway->select(['Inspection_ID' => $inspectionId]);

        $row = $resultSet->current();

        return $row;
    }

    public function save(Inspection $inspection)
    {
        $inspectionData = $inspection->toDbArrayForInsert();

        $this->tableGateway->insert($inspectionData);
        $inspection->id = $this->tableGateway->lastInsertValue;

        return $inspection;
    }

    public function update(Inspection $inspection)
    {
        $inspectionData = $inspection->toDbArrayForUpdate();

        $this->tableGateway->update($inspectionData, ['Inspection_ID' => $inspection->id]);

        return $inspection;
    }

    public function getMostRecentForBuyer(string $buyerId)
    {
        $gateway = $this->tableGateway;

        $results = $this->tableGateway->select(
            function (Select $select) use ($buyerId, $gateway) {
                $select->where->equalTo('Buyer_ID', $buyerId);

                $select->where->equalTo('Cancelled', 0);
                $select->where->equalTo('Incomplete', 0);

                $select->where->literal('_Date > DATE_SUB(NOW(), INTERVAL ' . self::RECENT_DEAL_DAY_THRESHOLD . ' DAY)');

                $select->order('Inspection_ID DESC');

                $sql = new Sql($gateway->getAdapter());
                $selectString = $sql->getSqlStringForSqlObject($select);
            }
        );

        $inspections = [];

        foreach ($results as $row) {
            $inspections[] = $row;
        }

        return $inspections;
    }

    public function find(array $filters)
    {
        $select = $this->tableGateway->select(
            function (Select $select) use ($filters) {
                $this->applySearchFilters($select, $filters);
                $select->order('Inspection_ID DESC');
                $this->paginate($select, $filters);
            }
        );

        return $this->getQueryResultsAsArray($select);
    }

    public function findForBuyer(array $filters, $buyerId)
    {
        $select = $this->tableGateway->select(
            function (Select $select) use ($filters, $buyerId) {
                $select->where->equalTo('Buyer_ID', $buyerId);
                $this->applySearchFilters($select, $filters);
                $select->order('Inspection_ID DESC');
                $this->paginate($select, $filters);
            }
        );

        return $this->getQueryResultsAsArray($select);
    }

    private function paginate(Select $select, array $filters)
    {
        $pageIndex = (int)$filters['pageIndex'];
        $pageSize = (int)$filters['pageSize'];

        $select->limit($pageSize)->offset((($pageIndex - 1) * $pageSize));
    }

    private function getQueryResultsAsArray($results)
    {
        $inspections = [];

        foreach ($results as $row) {
            $inspections[] = $row;
        }

        return $inspections;
    }

    public function getCount(array $filters)
    {
        return $this->exectueCountQuery(function ($sql) {
            return $sql->select()
                ->from('Inspection')
                ->columns([ 'num' => new \Zend\Db\Sql\Expression('COUNT(*)') ]);
        }, $filters);
    }

    public function getCountForBuyer(array $filters, $buyerId)
    {
        return $this->exectueCountQuery(function ($sql) use ($buyerId) {
            return $sql->select()
                ->from('Inspection')
                ->columns([ 'num' => new \Zend\Db\Sql\Expression('COUNT(*)') ])
                ->where(['Buyer_ID' => $buyerId]);
        }, $filters);
    }

    private function exectueCountQuery(callable $getSelect, array $filters)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $sql = new Sql($dbAdapter);

        $select = $getSelect($sql);

        $this->applySearchFilters($select, $filters);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        return (int)$results[0]['num'];
    }

    private function applySearchFilters(Select $select, array $filters)
    {
        if ($filters['fromDate'] ?? null) {
            $select->where->greaterThanOrEqualTo('DateVal', $filters['fromDate']);
        }
        if ($filters['toDate'] ?? null) {
            $select->where->lessThanOrEqualTo('DateVal', $filters['toDate']);
        }
        if ($filters['vehicleYear'] ?? null) {
            $select->where->equalTo('_Year', $filters['vehicleYear']);
        }
        if ($filters['vehicleMake'] ?? null) {
            $select->where->equalTo('Make', $filters['vehicleMake']);
        }
        if ($filters['vehicleModel'] ?? null) {
            $select->where->equalTo('Model', $filters['vehicleModel']);
        }
        if ($filters['vehicleColor'] ?? null) {
            $select->where->like('Color', '%' . $filters['vehicleColor'] . '%');
        }

        return $select;
    }
}
