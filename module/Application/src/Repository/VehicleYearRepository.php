<?php

namespace Application\Repository;

class VehicleYearRepository
{
    const EARLIEST_YEAR = 1990;

    public function getAll()
    {
        $result = [];

        for ($i = self::EARLIEST_YEAR; $i <= date("Y", time()); $i++) {
            $result[] = $i;
        }

        return $result;
    }
}
