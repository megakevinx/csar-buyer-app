<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

use Application\Model\TowStatus;

class TowStatusRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(TowStatus $towStatus)
    {
        $towStatusData = $towStatus->toDbArray();

        $this->tableGateway->insert($towStatusData);
        $towStatus->id = $this->tableGateway->lastInsertValue;

        return $towStatus;
    }

    /**
     * Gets the most recent tow statues associated with the given inspection
     * @return TowStatus
     */
    public function getLastByInspectionId(string $inspectionId)
    {
        $resultSet = $this->tableGateway->select(
            function (Select $select) use ($inspectionId) {
                $select->where->equalTo('Inspection_ID', $inspectionId);
                $select->order('TowStatus_ID DESC');
            }
        );

        $row = $resultSet->current();

        return $row;
    }

    public function getAllByInspectionId(string $inspectionId)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $sql = new Sql($dbAdapter);

        $select = $sql->select()
            ->from('TowStatus')->columns(['TowStatus_ID', 'Inspection_ID', 'ER', 'TowFrom', 'TowTo', 'OrderTime'])
            ->where(['TowStatus.Inspection_ID' => $inspectionId])
            ->order('TowStatus.TowStatus_ID DESC');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results) {
            $towStatuses = [];
            foreach ($results as $row) {
                $towStatus = new TowStatus();
                $towStatus->exchangeArray($row);

                $towStatuses[] = $towStatus;
            }

            return $towStatuses;
        } else {
            return null;
        }
    }
}
