<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\User;

class BuyerRepository
{
    const BUYER_USER_ROLE = 'Buyer';
    const ADMIN_USER_ROLE = 'Administrator';

    private $dbAdapter;

    public function __construct(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function get(int $id)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('Buyer')->columns(['Buyer_ID', 'BuyerName', 'BuyerEmail', 'BuyerPhone', 'PassWd'])
            ->where(['Buyer.Buyer_ID' => $id]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $userData = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($userData)
        {
            $user = new User();
            $user->id = $userData[0]['Buyer_ID'];
            $user->firstName = $userData[0]['BuyerName'];
            $user->password = $userData[0]['PassWd'];
            $user->email = $userData[0]['BuyerEmail'];
            $user->phoneNumber = $userData[0]['BuyerPhone'];
            $user->role = self::BUYER_USER_ROLE;

            return $user;
        }
        else {
            return null;
        }
    }

    public function getByName(string $name)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('Buyer')->columns(['Buyer_ID', 'BuyerName', 'BuyerEmail', 'BuyerPhone', 'PassWd', 'Admin'])
            ->where(['Buyer.BuyerName' => $name, 'Buyer.Active' => 1]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $userData = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($userData)
        {
            $user = new User();
            $user->id = $userData[0]['Buyer_ID'];
            $user->firstName = $userData[0]['BuyerName'];
            $user->password = $userData[0]['PassWd'];
            $user->email = $userData[0]['BuyerEmail'];
            $user->phoneNumber = $userData[0]['BuyerPhone'];
            $user->role = $userData[0]['Admin'] == 1 ? self::ADMIN_USER_ROLE : self::BUYER_USER_ROLE;

            return $user;
        }
        else {
            return null;
        }
    }

    public function getTowCaptain()
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('Buyer')->columns(['Buyer_ID', 'BuyerName', 'BuyerEmail', 'BuyerPhone', 'PassWd'])
            ->where(['Buyer.TowCaptain' => 1]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $userData = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($userData)
        {
            $user = new User();
            $user->id = $userData[0]['Buyer_ID'];
            $user->firstName = $userData[0]['BuyerName'];
            $user->password = $userData[0]['PassWd'];
            $user->email = $userData[0]['BuyerEmail'];
            $user->phoneNumber = $userData[0]['BuyerPhone'];
            $user->role = self::BUYER_USER_ROLE;

            return $user;
        }
        else {
            return null;
        }
    }
}
