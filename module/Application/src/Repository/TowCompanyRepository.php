<?php

namespace Application\Repository;

use Zend\Db\TableGateway\TableGatewayInterface;

class TowCompanyRepository extends BaseRepository
{
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->setTableGateway($tableGateway);
        $this->setIdentityField('TowCo_ID');
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select(['Active' => 1]);

        $rowsToReturn = [];

        foreach ($resultSet as $row) {
            $rowsToReturn[] = $row;
        }

        return $rowsToReturn;
    }
}
