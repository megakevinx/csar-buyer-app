<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

abstract class BaseRepository
{
    protected $tableGateway;
    protected $identityFieldName;

    protected function setTableGateway(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    protected function setIdentityField(string $identityFieldName)
    {
        $this->identityFieldName = $identityFieldName;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select();

        $rowsToReturn = [];

        foreach ($resultSet as $row) {
            $rowsToReturn[] = $row;
        }

        return $rowsToReturn;
    }

    public function get($id)
    {
        $resultSet = $this->tableGateway->select([$this->identityFieldName => $id]);

        $row = $resultSet->current();

        return $row;
    }
}
