<?php

namespace Application\Repository;

use Zend\Db\TableGateway\TableGatewayInterface;

class CountyRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function get(int $id)
    {
        $resultSet = $this->tableGateway->select(['Area_ID' => $id]);

        $row = $resultSet->current();

        return $row;
    }
}
