<?php

namespace Application\Repository;

use Zend\Db\TableGateway\TableGatewayInterface;

class TowPricingRuleRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getBy($towCompanyId, $countyId, $towFromLocation, $towToLocation)
    {
        $resultSet = $this->tableGateway->select([
            'TowCompany_ID' => $towCompanyId,
            'Area_ID' => $countyId,
            'TowFromLocation' => $towFromLocation,
            'TowToLocation' => $towToLocation,
        ]);

        $row = $resultSet->current();

        return $row;
    }
}
