<?php

namespace Application\Repository;

use Zend\Db\TableGateway\TableGatewayInterface;

class ZipRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getByCode(string $code)
    {
        $resultSet = $this->tableGateway->select(['ZipCode' => $code]);

        $row = $resultSet->current();

        return $row;
    }
}
