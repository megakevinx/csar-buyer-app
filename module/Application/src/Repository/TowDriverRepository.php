<?php

namespace Application\Repository;

use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

use Application\Model\TowDriver;

class TowDriverRepository extends BaseRepository
{
    private $dbAdapter;

    public function __construct(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function getAll()
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('TowCo_Driver')->columns(['TowCo_Driver_ID'])
            ->join('TowCo', 'TowCo_Driver.TowCo_ID = TowCo.TowCo_ID', ['TowCo_ID', 'CompanyName'])
            ->join('Buyer', 'TowCo_Driver.Buyer_ID = Buyer.Buyer_ID', ['Buyer_ID', 'BuyerName']);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results) {
            $towDrivers = [];
            foreach ($results as $row) {
                $towDriver = new TowDriver();
                $towDriver->exchangeArray($row);

                $towDrivers[] = $towDriver;
            }

            return $towDrivers;
        } else {
            return null;
        }
    }

    public function getByBuyerId(string $buyerId)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
        ->from('TowCo_Driver')->columns(['TowCo_Driver_ID'])
        ->join('TowCo', 'TowCo_Driver.TowCo_ID = TowCo.TowCo_ID', ['TowCo_ID', 'CompanyName'])
        ->join('Buyer', 'TowCo_Driver.Buyer_ID = Buyer.Buyer_ID', ['Buyer_ID', 'BuyerName']);

        $select->where->equalTo('TowCo_Driver.Buyer_ID', $buyerId);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results) {
            foreach ($results as $row) {
                $towDriver = new TowDriver();
                $towDriver->exchangeArray($row);

                return $towDriver;
            }
        } else {
            return null;
        }
    }
}
