<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\EditDealForm;

use Application\Service\InspectionService;
use Application\Service\TowStatusService;
use Application\Service\LocationService;

class DealController extends BaseRestfulController
{
    private $inspectionService;
    private $towStatusService;
    private $locationService;

    public function __construct(
        InspectionService $inspectionService,
        TowStatusService $towStatusService,
        LocationService $locationService
    ) {
        $this->inspectionService = $inspectionService;
        $this->towStatusService = $towStatusService;
        $this->locationService = $locationService;
    }

    // GET api/deal
    public function getList()
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        $data = $this->getRequest()->getQuery()->toArray();

        $result = $this->inspectionService->search($data, $this->identity());

        return new JsonModel([
            'deals' => $result['deals'],
            'totalDealsCount' => $result['totalDealsCount']
        ]);
    }

    // GET api/deal/{id}
    public function get($id)
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        $inspectionResult = $this->inspectionService->getInspection($id);
        $towStatusResult = $this->towStatusService->getAllByInspectionId($id);

        if (! $inspectionResult->isSuccess) {
            return $this->badRequest([
                'errorCode' => "NOT_FOUND",
                'errorMessage' => "There is no Deal with the given number.",
                'inspectionResult' => $inspectionResult,
                'towStatusResult' => $towStatusResult,
            ]);
        }

        return $this->ok([
            'deal' => $inspectionResult->inspection,
            'tows' => $towStatusResult->towStatuses
        ]);
    }

    // PUT api/deal/{id}
    public function update($id, $data)
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        $form = new EditDealForm();
        $form->setData($data);

        if (!$form->isValid()) {
            return $this->badRequest([
                'errorCode' => "INPUT_DATA_INVALID",
                'errorMessage' => "The input data is invalid",
                'errorMessages' => $form->getMessages()
            ]);
        }

        $data = $form->getData();

        if (! $this->locationService->isZipSupported($data['customerZip'])) {
            return $this->badRequest([
                'errorCode' => "ZIP_NOT_SUPPORTED",
                'errorMessage' => "We don't offer our services in that area, please choose a diferent Zip code"
            ]);
        }

        $inspectionResult = $this->inspectionService->updateInspection($data);

        if (! $inspectionResult->isSuccess) {
            return $this->badRequest([
                'errorCode' => "NOT_FOUND",
                'errorMessage' => "The Deal you want to edit does not exist. Ref Number: " . $data['dealNumber']
            ]);
        }

        return $this->ok([
            'dealReferenceNumber' => $inspectionResult->savedEntity->id,
            'inspectionResult' => $inspectionResult
        ]);
    }
}
