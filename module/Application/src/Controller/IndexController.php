<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    const ADMIN_ROLE = 'Administrator';
    const BUYER_ROLE = 'Buyer';

    private function processWithAuth(array $authorizedRoles)
    {
        $user = $this->identity();

        if (!$user || !in_array($user->role, $authorizedRoles)) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }

    private function allowBuyerAndAdmin()
    {
        return $this->processWithAuth([self::BUYER_ROLE, self::ADMIN_ROLE]);
    }

    public function indexAction()
    {
        $user = $this->identity();

        if (!$user || !in_array($user->role, [self::BUYER_ROLE, self::ADMIN_ROLE])) {
            return $this->redirect()->toRoute('login');
        }
        else {
            return $this->redirect()->toRoute('select-operation');
        }
    }

    public function selectOperationAction()
    {
        return $this->allowBuyerAndAdmin();
    }

    // public function adminAction()
    // {
    //     $user = $this->identity();

    //     if (!$user || $user->role !== self::ADMIN_ROLE) {
    //         return $this->redirect()->toRoute('login');
    //     }

    //     return new ViewModel();
    // }

    // public function buyerAction()
    // {
    //     return $this->allowBuyerAndAdmin();
    // }

    public function enterNewDealAction()
    {
        return $this->allowBuyerAndAdmin();
    }

    public function pendingDealsAction()
    {
        return new ViewModel();
    }

    public function addToADealAction()
    {
        return $this->allowBuyerAndAdmin();
    }

    public function editOldDealAction()
    {
        return $this->allowBuyerAndAdmin();
    }
}
