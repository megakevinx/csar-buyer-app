<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Zend\Form\Form;
use Application\Controller\Form\CreateTowOrderForm;
use Application\Controller\Form\EditTowOrderForm;

use Application\Service\InspectionService;
use Application\Service\LocationService;
use Application\Service\TowStatusService;
use Application\Service\NotificationService;

class TowOrderController extends BaseRestfulController
{
    private $inspectionService;
    private $locationService;
    private $towStatusService;
    private $notificationService;

    public function __construct(
        InspectionService $inspectionService,
        LocationService $locationService,
        TowStatusService $towStatusService,
        NotificationService $notificationService
    ) {
        $this->inspectionService = $inspectionService;
        $this->locationService = $locationService;
        $this->towStatusService = $towStatusService;
        $this->notificationService = $notificationService;
    }

    public function handleRequest(array $data, Form $form, callable $saveInspection, callable $onSaveInspectionError)
    {
        if (! $this->identity()) {
            $data['customerNamePicture'] = null;
            $data['customerIdPicture'] = null;
            $data['vehiclePictures'] = null;

            error_log(
                json_encode([
                    'errorCode' => "NOT_AUTHORIZED",
                    'data' => $data
                ])
            );

            return $this->notAuthorized();
        }

        $form->setData($data);

        if (! $form->isValid()) {

            $data['customerNamePicture'] = null;
            $data['customerIdPicture'] = null;
            $data['vehiclePictures'] = null;

            error_log(
                json_encode([
                    'errorCode' => "INPUT_DATA_INVALID",
                    'errorMessage' => "The input data is invalid",
                    'errorMessages' => $form->getMessages(),
                    'data' => $data
                ])
            );

            return $this->badRequest([
                'errorCode' => "INPUT_DATA_INVALID",
                'errorMessage' => "The input data is invalid",
                'errorMessages' => $form->getMessages()
            ]);
        }

        $data = $form->getData();

        if (! $this->locationService->isZipSupported($data['customerZip'])) {

            $data['customerNamePicture'] = null;
            $data['customerIdPicture'] = null;
            $data['vehiclePictures'] = null;

            error_log(
                json_encode([
                    'errorCode' => "ZIP_NOT_SUPPORTED",
                    'errorMessage' => "We don't offer our services in that area, please choose a diferent Zip code",
                    'data' => $data
                ])
            );

            return $this->badRequest([
                'errorCode' => "ZIP_NOT_SUPPORTED",
                'errorMessage' => "We don't offer our services in that area, please choose a diferent Zip code"
            ]);
        }

        $data["createdByUserId"] = $this->identity()->id;

        $inspectionResult = $saveInspection($data);

        if (! $inspectionResult->isSuccess) {
            return $onSaveInspectionError($data, $inspectionResult);
        }

        $data["inspectionId"] = $inspectionResult->savedEntity->id;

        $towStatusResult = $this->towStatusService->createTowStatus($data);
        $data["towId"] = $towStatusResult->savedEntity->id;

        $notificationResult = $this->notificationService->createTowOrderNotification($data);

        return $this->ok([
            'dealReferenceNumber' => $inspectionResult->savedEntity->id,
            'towNumber' => $towStatusResult->savedEntity->id,
            'inspectionResult' => $inspectionResult,
            'towStatusResult' => $towStatusResult,
            'notificationResult' => $notificationResult
        ]);
    }

    // POST api/tow-order
    public function create($data)
    {
        return $this->handleRequest(
            $data,
            new CreateTowOrderForm(),
            function ($dataToSave) {
                return $this->inspectionService->createInspection($dataToSave);
            },
            function ($dataToSave, $saveOperationResult) {

                $dataToSave['customerNamePicture'] = null;
                $dataToSave['customerIdPicture'] = null;
                $dataToSave['vehiclePictures'] = null;

                error_log(
                    json_encode([
                        'errorCode' => "UNSPECIFIED_ERROR",
                        'errorMessage' => "There has been an unspecified error while saving the deal.",
                        'result' => $saveOperationResult,
                        'data' => $dataToSave
                    ])
                );

                return $this->badRequest([
                    'errorCode' => "UNSPECIFIED_ERROR",
                    'errorMessage' => "There has been an unspecified error while saving the deal.",
                    'result' => $saveOperationResult,
                    'submittedData' => $dataToSave
                ]);
            }
        );
    }

    // PUT api/tow-order/{id}
    public function update($id, $data)
    {
        return $this->handleRequest(
            $data,
            new EditTowOrderForm(),
            function ($dataToSave) {
                return $this->inspectionService->updateInspection($dataToSave);
            },
            function ($dataToSave, $saveOperationResult) {

                $dataToSave['customerNamePicture'] = null;
                $dataToSave['customerIdPicture'] = null;
                $dataToSave['vehiclePictures'] = null;

                error_log(
                    json_encode([
                        'errorCode' => "NOT_FOUND",
                        'errorMessage' => "The Deal you want to edit does not exist. Ref Number: " . $dataToSave['dealNumber'],
                        'result' => $saveOperationResult,
                        'data' => $dataToSave
                    ])
                );

                return $this->badRequest([
                    'errorCode' => "NOT_FOUND",
                    'errorMessage' => "The Deal you want to edit does not exist. Ref Number: " . $dataToSave['dealNumber'],
                    'result' => $saveOperationResult
                ]);
            }
        );
    }
}
