<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Repository\TowCompanyRepository;
use Application\Repository\TowDriverRepository;
use Application\Repository\VehicleYearRepository;
use Application\Repository\VehicleMakeRepository;
use Application\Repository\InspectionRepository;

class ConfigurationController extends BaseRestfulController
{
    private $towCompanyRepository;
    private $towDriverRepository;
    private $vehicleYearRepository;
    private $vehicleMakeRepository;
    private $inspectionRepository;

    public function __construct(
        TowCompanyRepository $towCompanyRepository,
        TowDriverRepository $towDriverRepository,
        VehicleYearRepository $vehicleYearRepository,
        VehicleMakeRepository $vehicleMakeRepository,
        InspectionRepository $inspectionRepository
    ) {
        $this->towCompanyRepository = $towCompanyRepository;
        $this->towDriverRepository = $towDriverRepository;
        $this->vehicleYearRepository = $vehicleYearRepository;
        $this->vehicleMakeRepository = $vehicleMakeRepository;
        $this->inspectionRepository = $inspectionRepository;
    }

    // GET api/configuration/enter-new-deal
    public function enterNewDealAction()
    {
        // if (!$this->userIsRole('Administrator')) {
        //     return $this->notAuthorized();
        // }

        if ($this->getRequest()->isGet()) {
            return $this->ok([
                "vehicleYears" => $this->vehicleYearRepository->getAll(),
                "vehicleMakes" => $this->vehicleMakeRepository->getAll(),
                "vehicleWheelTypes" => [
                    ["id" => "steel", "name" => "Steel"],
                    ["id" => "aluminum", "name" => "Aluminum"],
                    ["id" => "no wheels", "name" => "No Wheels"]
                ],
                "vehicleBodyStyles" => [
                    ["id" => "four", "name" => "4 Door"],
                    ["id" => "two", "name" => "2 Door"]
                ]
            ]);
        } else {
            return $this->methodNotAllowed();
        }
    }

    // GET api/configuration/add-to-a-deal
    public function addToADealAction()
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        if ($this->getRequest()->isGet()) {
            return $this->ok([
                "recentDeals" => $this->inspectionRepository->getMostRecentForBuyer($this->identity()->id),
            ]);
        } else {
            return $this->methodNotAllowed();
        }
    }

    // GET api/configuration/edit-old-deal
    public function editOldDealAction()
    {
        if ($this->getRequest()->isGet()) {
            return new JsonModel([
                "vehicleYears" => $this->vehicleYearRepository->getAll(),
                "vehicleMakes" => $this->vehicleMakeRepository->getAll()
            ]);
        } else {
            return $this->methodNotAllowed();
        }
    }
}
