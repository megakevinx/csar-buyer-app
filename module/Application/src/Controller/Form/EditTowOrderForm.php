<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class EditTowOrderForm extends CreateTowOrderForm
{
    public function __construct()
    {
        parent::__construct('edit-tow-order-form');
    }

    protected function addElements()
    {
        parent::addElements();
        $this->add(['name' => 'dealNumber']);
    }

    protected function addInputFilter()
    {
        parent::addInputFilter();
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'dealNumber',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ]
            ],
        ]);
    }
}
