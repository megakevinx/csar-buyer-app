<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class CreateDealForm extends Form
{
    public function __construct($name = null)
    {
        if ($name) {
            parent::__construct($name);
        } else {
            parent::__construct('create-deal-form');
        }

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add(['name' => 'customerName']);
        $this->add(['name' => 'customerNamePicture']);
        $this->add(['name' => 'customerId']);
        $this->add(['name' => 'customerIdPicture']);
        $this->add(['name' => 'customerGender']);
        $this->add(['name' => 'customerPhone']);
        $this->add(['name' => 'customerStreetAddress']);
        $this->add(['name' => 'customerZip']);
        $this->add(['name' => 'customerCity']);

        $this->add(['name' => 'vehicleYear']);
        $this->add(['name' => 'vehicleMake']);
        $this->add(['name' => 'vehicleModel']);
        $this->add(['name' => 'vehicleColor']);
        $this->add(['name' => 'vehiclePictures']);

        $this->add(['name' => 'vehicleVin']);
        $this->add(['name' => 'vehicleHasTitle']);
        $this->add(['name' => 'vehicleTitle']);

        $this->add(['name' => 'vehicleWheelType']);
        $this->add(['name' => 'vehicleBodyStyle']);
        $this->add(['name' => 'vehiclePurchasePrice']);

        $this->add(['name' => 'customerDataNotes']);
        $this->add(['name' => 'vehicleDescriptionNotes']);
        $this->add(['name' => 'vehicleIdentificationNotes']);
        $this->add(['name' => 'vehiclePurchaseDetailsNotes']);

        $this->add(['name' => 'pickUpNotes']);
    }

    private function requiredIfVehicleHasTitle($value, $formValues)
    {
        if ($formValues["vehicleHasTitle"] === 'yes') {
            if ($value) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    protected function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'customerName',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 45 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'customerNamePicture',
            'required' => false
        ]);

        $inputFilter->add([
            'name' => 'customerId',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 35 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'customerIdPicture',
            'required' => false
        ]);

        $inputFilter->add([
            'name' => 'customerGender',
            'required' => false, // TODO: Change in the future
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['Male', 'Female'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'customerPhone',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/ /',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [ 'min' => 10, 'max' => 15 ],
                ],
                [
                    'name' => 'Regex',
                    'options' => [ 'pattern' => '/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/' ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'customerStreetAddress',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 75 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'customerZip',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'customerCity',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 75 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleYear',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 1990, 'inclusive' => true ],
                ],
                [
                    'name' => 'IsInt'
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleMake',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleModel',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleColor',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 25 ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehiclePictures',
            'required' => false
        ]);

        $inputFilter->add([
            'name' => 'vehicleVin',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 20 ],
                ],
                [
                    'name' => 'Alnum'
                ]
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleHasTitle',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['yes', 'no'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleTitle',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [ 'min' => 1, 'max' => 45 ],
                ],
                [
                    'name' => 'Alnum'
                ],
                [
                    'name' => 'Callback',
                    'options' =>
                    [
                        'callback' => function ($value, $formValues) {
                            return $this->requiredIfVehicleHasTitle($value, $formValues);
                        }
                    ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleWheelType',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['steel', 'aluminum', 'no wheels'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehicleBodyStyle',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['four', 'two'] ],
                ]
            ],
        ]);

        $inputFilter->add([
            'name' => 'vehiclePurchasePrice',
            'required' => true,
            'filters' => [
                [
                    'name' => 'StringTrim'
                ],
                [
                    'name' => 'PregReplace',
                    'options' => [
                        'pattern' => '/,/',
                        'replacement' => ''
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'IsFloat',
                    'options' => [
                        'locale' => 'en'
                    ],
                ],
                [
                    'name' => 'LessThan',
                    'options' => [ 'max' => 999.99, 'inclusive' => true ],
                ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'customerDataNotes',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleDescriptionNotes',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehicleIdentificationNotes',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'vehiclePurchaseDetailsNotes',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);

        $inputFilter->add([
            'name' => 'pickUpNotes',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ]
        ]);
    }
}
