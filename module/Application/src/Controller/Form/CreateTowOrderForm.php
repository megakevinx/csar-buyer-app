<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class CreateTowOrderForm extends CreateDealForm
{
    public function __construct($name = null)
    {
        if ($name) {
            parent::__construct($name);
        } else {
            parent::__construct('create-tow-order-form');
        }

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        parent::addElements();

        $this->add(['name' => 'isHighPriorityTow']);
    }

    protected function addInputFilter()
    {
        parent::addInputFilter();
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'isHighPriorityTow',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'InArray',
                    'options' => [ 'haystack' => ['yes', 'no'] ],
                ]
            ],
        ]);
    }
}
