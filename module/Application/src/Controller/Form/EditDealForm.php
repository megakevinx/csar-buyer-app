<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class EditDealForm extends CreateDealForm
{
    public function __construct()
    {
        parent::__construct('edit-deal-form');
    }

    protected function addElements()
    {
        parent::addElements();
        $this->add(['name' => 'dealNumber']);
    }

    protected function addInputFilter()
    {
        parent::addInputFilter();
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'dealNumber',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ]
            ],
        ]);
    }
}
