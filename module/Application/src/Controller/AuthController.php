<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
//use Zend\Authentication\Result;
use Zend\Uri\Uri;

use Zend\Form\Form;
use Application\Controller\Form\LoginForm;
use Application\Controller\Form\ExternalLoginForm;

use Application\Service\AuthService;

class AuthController extends AbstractActionController
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function loginAction()
    {
        $form = new LoginForm();
        return $this->handleLoginRequest(
            $form,
            function ($data) {
                return $this->authService->login(
                    $data['email'], $data['password'], $data['remember_me'], false
                );
            },
            function ($form, $isLoginError) {
                return new ViewModel([
                    'form' => $form,
                    'isLoginError' => $isLoginError
                ]);
            }
        );
    }

    public function externalLoginAction()
    {
        $form = new ExternalLoginForm();
        return $this->handleLoginRequest(
            $form,
            function ($data) {
                return $this->authService->login(
                    $data['email'], $data['password'], false, true
                );
            },
            function () {
                return $this->redirect()->toRoute('login');
            }
        );
    }

    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction() 
    {
        if ($this->identity())
        {
            $this->authService->logout();
        }

        return $this->redirect()->toRoute('login');
    }

    private function handleLoginRequest(Form $form, callable $login, callable $respond)
    {
        // Store login status.
        $isLoginError = false;

        // Check if user has submitted the form
        if ($this->getRequest()->isPost())
        {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid())
            {
                // Get filtered and validated data
                $data = $form->getData();

                // Perform login attempt.
                $result = $login($data);

                // Check result.
                if ($result->isSuccess)
                {
                    // TODO: Add more redirects for each role
                    if ($result->identity->role === 'Buyer')
                    {
                        return $this->redirect()->toRoute('select-operation');
                    }
                    else if ($result->identity->role === 'Administrator')
                    {
                        return $this->redirect()->toRoute('select-operation');
                    }
                    else
                    {
                        $isLoginError = true;
                    }
                }
                else
                {
                    $isLoginError = true;
                }
            }
            else
            {
                $isLoginError = true;
            }
        }

        return $respond($form, $isLoginError);
    }
}
