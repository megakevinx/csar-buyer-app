<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\Mvc\MvcEvent;
use Zend\Session\AbstractManager;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function onBootstrap(MvcEvent $e)
    {
        $sessionManager = $e->getApplication()->getServiceManager()->get('Zend\Session\SessionManager');
        $this->forgetInvalidSession($sessionManager);
    }

    protected function forgetInvalidSession(AbstractManager $sessionManager)
    {
        try {
            $sessionManager->start();
            return;
        } catch (\Exception $e) {
        }
        /**
         * Session validation failed: toast it and carry on.
         */
        // @codeCoverageIgnoreStart
        session_unset();
        // @codeCoverageIgnoreEnd
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [

                // Services
                Service\AuthService::class => function ($container) {

                    // Get contents of 'access_filter' config key (the AuthManager service
                    // will use this data to determine whether to allow currently logged in user
                    // to execute the controller action or not.
                    // $config = $container->get('Config');
                    // if (isset($config['access_filter']))
                    //     $config = $config['access_filter'];
                    // else
                    //     $config = [];

                    return new Service\AuthService(
                        $container->get(\Zend\Authentication\AuthenticationService::class),
                        $container->get(\Zend\Session\SessionManager::class),
                        $container->get(Repository\UserRepository::class)
                    );
                },
                \Zend\Authentication\AuthenticationService::class => function ($container) {
                    return new AuthenticationService(
                        new SessionStorage('Zend_Auth', 'session', $container->get(\Zend\Session\SessionManager::class)),
                        $container->get(Service\AuthAdapter::class)
                    );
                },
                Service\AuthAdapter::class => function ($container) {
                    return new Service\AuthAdapter(
                        $container->get(Repository\UserRepository::class),
                        $container->get(Repository\BuyerRepository::class)
                    );
                },
                Service\InspectionService::class => function ($container) {
                    return new Service\InspectionService(
                        $container->get(Repository\InspectionRepository::class),
                        $container->get(Repository\PhotoRepository::class)
                    );
                },
                Service\LocationService::class => function ($container) {
                    return new Service\LocationService(
                        $container->get(Repository\ZipRepository::class),
                        $container->get(Repository\CountyRepository::class)
                    );
                },
                Service\TowStatusService::class => function ($container) {
                    return new Service\TowStatusService(
                        $container->get(Repository\TowStatusRepository::class),
                        $container->get(Repository\TowDriverRepository::class),
                        $container->get(Repository\InspectionRepository::class)
                    );
                },
                Service\NotificationService::class => function ($container) {
                    return new Service\NotificationService(
                        $container->get(Repository\NotificationRepository::class),
                        $container->get(Repository\BuyerRepository::class),
                        $container->get(Repository\TowCompanyRepository::class)
                    );
                },
                Service\TowPriceCalculator::class => function ($container) {
                    return new Service\TowPriceCalculator(
                        $container->get(Repository\TowPricingRuleRepository::class),
                        $container->get(Service\LocationService::class)
                    );
                },

                // Repositories
                Repository\UserRepository::class => function ($container) {
                    return new Repository\UserRepository(
                        $container->get(AdapterInterface::class),
                        $container->get(Model\UserTableGateway::class),
                        $container->get(Model\RoleTableGateway::class),
                        $container->get(Model\UserRoleAppTableGateway::class)
                    );
                },
                Repository\BuyerRepository::class => function ($container) {
                    return new Repository\BuyerRepository(
                        $container->get(AdapterInterface::class)
                    );
                },
                Repository\TowCompanyRepository::class => function ($container) {
                    return new Repository\TowCompanyRepository(
                        $container->get(Model\TowCompanyTableGateway::class)
                    );
                },
                Repository\TowDriverRepository::class => function ($container) {
                    return new Repository\TowDriverRepository(
                        $container->get(AdapterInterface::class)
                    );
                },
                Repository\VehicleYearRepository::class => function ($container) {
                    return new Repository\VehicleYearRepository();
                },
                Repository\VehicleMakeRepository::class => function ($container) {
                    return new Repository\VehicleMakeRepository(
                        $container->get(Model\VehicleMakeTableGateway::class)
                    );
                },
                Repository\VehicleModelRepository::class => function ($container) {
                    return new Repository\VehicleModelRepository(
                        $container->get(Model\VehicleModelTableGateway::class)
                    );
                },
                Repository\InspectionRepository::class => function ($container) {
                    return new Repository\InspectionRepository(
                        $container->get(Model\InspectionTableGateway::class)
                    );
                },
                Repository\ZipRepository::class => function ($container) {
                    return new Repository\ZipRepository(
                        $container->get(Model\ZipTableGateway::class)
                    );
                },
                Repository\CountyRepository::class => function ($container) {
                    return new Repository\CountyRepository(
                        $container->get(Model\CountyTableGateway::class)
                    );
                },
                Repository\TowStatusRepository::class => function ($container) {
                    return new Repository\TowStatusRepository(
                        $container->get(Model\TowStatusTableGateway::class)
                    );
                },
                Repository\NotificationRepository::class => function ($container) {
                    return new Repository\NotificationRepository(
                        $container->get(Model\NotificationTableGateway::class)
                    );
                },
                Repository\PhotoRepository::class => function ($container) {
                    return new Repository\PhotoRepository(
                        $container->get(Model\PhotoTableGateway::class)
                    );
                },
                Repository\TowPricingRuleRepository::class => function ($container) {
                    return new Repository\TowPricingRuleRepository(
                        $container->get(Model\TowPricingRuleTableGateway::class)
                    );
                },

                // Table Gateways
                Model\UserTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('User', $dbAdapter, null, null);
                },
                Model\RoleTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('UserRole', $dbAdapter, null, null);
                },
                Model\UserRoleAppTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('User_UserRole_Application', $dbAdapter, null, null);
                },
                Model\TowCompanyTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\TowCompany());
                    return new TableGateway('TowCo', $dbAdapter, null, $resultSetPrototype);
                },
                Model\VehicleMakeTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleMake());
                    return new TableGateway('VehicleMake', $dbAdapter, null, $resultSetPrototype);
                },
                Model\VehicleModelTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleModel());
                    return new TableGateway('VehicleModel', $dbAdapter, null, $resultSetPrototype);
                },
                Model\InspectionTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Inspection());
                    return new TableGateway('Inspection', $dbAdapter, null, $resultSetPrototype);
                },
                Model\ZipTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Zip());
                    return new TableGateway('Zip', $dbAdapter, null, $resultSetPrototype);
                },
                Model\CountyTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\County());
                    return new TableGateway('Area', $dbAdapter, null, $resultSetPrototype);
                },
                Model\TowStatusTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\TowStatus());
                    return new TableGateway('TowStatus', $dbAdapter, null, $resultSetPrototype);
                },
                Model\NotificationTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Notification());
                    return new TableGateway('Notification', $dbAdapter, null, $resultSetPrototype);
                },
                Model\PhotoTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Photo());
                    return new TableGateway('Photo', $dbAdapter, null, $resultSetPrototype);
                },
                Model\TowPricingRuleTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\TowPricingRule());
                    return new TableGateway('TowPricingRule', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\AuthController::class => function ($container) {
                    return new Controller\AuthController(
                        $container->get(Service\AuthService::class)
                    );
                },

                Controller\ConfigurationController::class => function ($container) {
                    return new Controller\ConfigurationController(
                        $container->get(Repository\TowCompanyRepository::class),
                        $container->get(Repository\TowDriverRepository::class),
                        $container->get(Repository\VehicleYearRepository::class),
                        $container->get(Repository\VehicleMakeRepository::class),
                        $container->get(Repository\InspectionRepository::class)
                    );
                },

                Controller\VehicleController::class => function ($container) {
                    return new Controller\VehicleController(
                        $container->get(Repository\VehicleModelRepository::class)
                    );
                },

                Controller\TowOrderController::class => function ($container) {
                    return new Controller\TowOrderController(
                        $container->get(Service\InspectionService::class),
                        $container->get(Service\LocationService::class),
                        $container->get(Service\TowStatusService::class),
                        $container->get(Service\NotificationService::class)
                    );
                },

                Controller\DealController::class => function ($container) {
                    return new Controller\DealController(
                        $container->get(Service\InspectionService::class),
                        $container->get(Service\TowStatusService::class),
                        $container->get(Service\LocationService::class)
                    );
                }
            ]
        ];
    }
}
