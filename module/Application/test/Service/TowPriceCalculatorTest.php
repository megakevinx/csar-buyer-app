<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\TowPriceCalculator;

use Application\Service\LocationService;
use Application\Repository\TowPricingRuleRepository;
use Application\Model\County;
use Application\Model\TowPricingRule;

class TowPriceCalculatorTest extends TestCase
{
    public function testCalculateTowPriceUsesFullRule()
    {
        // Arrange
        $params = [
            "customerZip" => "12345",
            "towCompanyId" => 1,
            "towFromLocation" => "from_location",
            "towToLocation" => "to_location",
        ];

        $county = new County();
        $county->id = 1;
        $county->name = "county_name";

        $mockLocationService = $this->createMock(LocationService::class);
        $mockLocationService
            ->expects($this->once())
            ->method("getCountyForZip")
            ->with($this->equalTo($params['customerZip']))
            ->willReturn($county);

        $rule = new TowPricingRule();
        $rule->towPrice = 11.00;

        $mockTowPricingRuleRepository = $this->createMock(TowPricingRuleRepository::class);
        $mockTowPricingRuleRepository
            ->expects($this->once())
            ->method("getBy")
            ->with(
                $this->equalTo($params['towCompanyId']),
                $this->equalTo($county->id),
                $this->equalTo($params["towFromLocation"]),
                $this->equalTo($params['towToLocation'])
            )
            ->willReturn($rule);

        $calculator = new TowPriceCalculator($mockTowPricingRuleRepository, $mockLocationService);

        // Act
        $result = $calculator->calculateTowPrice($params);

        // Assert
        $this->assertEquals($result, 11.00);
    }

    public function testCalculateTowPriceUsesPartialRule()
    {
        // Arrange
        $params = [
            "customerZip" => "12345",
            "towCompanyId" => 1,
            "towFromLocation" => "from_location",
            "towToLocation" => "to_location",
        ];

        $county = new County();
        $county->id = 1;
        $county->name = "county_name";

        $mockLocationService = $this->createMock(LocationService::class);
        $mockLocationService
            ->expects($this->once())
            ->method("getCountyForZip")
            ->with($this->equalTo($params['customerZip']))
            ->willReturn($county);

        $rule = new TowPricingRule();
        $rule->towPrice = 22.00;

        $mockTowPricingRuleRepository = $this->createMock(TowPricingRuleRepository::class);
        $mockTowPricingRuleRepository
            ->expects($this->exactly(2))
            ->method("getBy")
            ->withConsecutive(
                [
                    $this->equalTo($params['towCompanyId']),
                    $this->equalTo($county->id),
                    $this->equalTo($params["towFromLocation"]),
                    $this->equalTo($params['towToLocation'])
                ],
                [
                    $this->equalTo(null),
                    $this->equalTo(null),
                    $this->equalTo($params["towFromLocation"]),
                    $this->equalTo($params['towToLocation'])
                ]
            )
            ->will($this->onConsecutiveCalls(
                null,
                $rule
            ));

        $calculator = new TowPriceCalculator($mockTowPricingRuleRepository, $mockLocationService);

        // Act
        $result = $calculator->calculateTowPrice($params);

        // Assert
        $this->assertEquals($result, 22.00);
    }

    public function testCalculateTowPriceReturnsZeroWhenNoRuleMatches()
    {
        // Arrange
        $params = [
            "customerZip" => "12345",
            "towCompanyId" => 1,
            "towFromLocation" => "from_location",
            "towToLocation" => "to_location",
        ];

        $county = new County();
        $county->id = 1;
        $county->name = "county_name";

        $mockLocationService = $this->createMock(LocationService::class);
        $mockLocationService
            ->expects($this->once())
            ->method("getCountyForZip")
            ->with($this->equalTo($params['customerZip']))
            ->willReturn($county);

        $mockTowPricingRuleRepository = $this->createMock(TowPricingRuleRepository::class);
        $mockTowPricingRuleRepository
            ->expects($this->exactly(2))
            ->method("getBy")
            ->withConsecutive(
                [
                    $this->equalTo($params['towCompanyId']),
                    $this->equalTo($county->id),
                    $this->equalTo($params["towFromLocation"]),
                    $this->equalTo($params['towToLocation'])
                ],
                [
                    $this->equalTo(null),
                    $this->equalTo(null),
                    $this->equalTo($params["towFromLocation"]),
                    $this->equalTo($params['towToLocation'])
                ]
            )
            ->will($this->onConsecutiveCalls(
                null,
                null
            ));

        $calculator = new TowPriceCalculator($mockTowPricingRuleRepository, $mockLocationService);

        // Act
        $result = $calculator->calculateTowPrice($params);

        // Assert
        $this->assertEquals($result, 0.00);
    }

    public function testCalculateTowPriceReturnsZeroWhenNoCountyIsFoundAndNoPartialRuleMatch()
    {
        // Arrange
        $params = [
            "customerZip" => "12345",
            "towCompanyId" => 1,
            "towFromLocation" => "from_location",
            "towToLocation" => "to_location",
        ];

        $mockLocationService = $this->createMock(LocationService::class);
        $mockLocationService
            ->expects($this->once())
            ->method("getCountyForZip")
            ->with($this->equalTo($params['customerZip']))
            ->willReturn(null);

        $mockTowPricingRuleRepository = $this->createMock(TowPricingRuleRepository::class);
        $mockTowPricingRuleRepository
        ->expects($this->once())
        ->method("getBy")
        ->with(
            $this->equalTo(null),
            $this->equalTo(null),
            $this->equalTo($params["towFromLocation"]),
            $this->equalTo($params['towToLocation'])
        )
        ->willReturn(null);

        $calculator = new TowPriceCalculator($mockTowPricingRuleRepository, $mockLocationService);

        // Act
        $result = $calculator->calculateTowPrice($params);

        // Assert
        $this->assertEquals($result, 0.00);
    }
}
