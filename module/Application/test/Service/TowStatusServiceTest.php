<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\TowStatusService;

use Application\Service\TowPriceCalculator;
use Application\Repository\TowStatusRepository;
use Application\Repository\TowDriverRepository;
use Application\Model\TowStatus;
use Application\Model\TowDriver;

class TowStatusServiceTest extends TestCase
{
    public function testCreateTowStatusUsesCorrectIncomingData()
    {
        // Arrange
        $params = [
            "inspectionId" => "12345",
            "isHighPriorityTow" => "yes",
            "towFromLocation" => "from_location",
            "towToLocation" => "to_location",
            "towDriverId" => 1
        ];

        $mockTowStatusRepository = $this->createMock(TowStatusRepository::class);
        $mockTowStatusRepository
            ->expects($this->once())
            ->method("save")
            ->will($this->returnArgument(0));

        $mockTowDriverRepository = $this->createMock(TowDriverRepository::class);

        $mockTowPriceCalculator = $this->createMock(TowPriceCalculator::class);
        $mockTowPriceCalculator
            ->expects($this->once())
            ->method("calculateTowPrice")
            ->willReturn(10.00);

        $service = new TowStatusService($mockTowStatusRepository, $mockTowDriverRepository, $mockTowPriceCalculator);

        // Act
        $result = $service->createTowStatus($params);

        // Assert
        $this->assertTrue($result->isSuccess);

        $this->assertEquals($result->savedEntity->inspectionId, $params["inspectionId"]);
        $this->assertEquals($result->savedEntity->isHighPriorityTow, 1);
        $this->assertEquals($result->savedEntity->towFromLocation, $params["towFromLocation"]);
        $this->assertEquals($result->savedEntity->towToLocation, $params["towToLocation"]);
        $this->assertEquals($result->savedEntity->buyerId, $params["towDriverId"]);
        $this->assertEquals($result->savedEntity->price, 10.00);

        $this->assertNotNull($result->savedEntity->createdTime);
        $this->assertNotNull($result->savedEntity->assignedDate);
    }

    public function testCreateTowStatusSetsHighPriorityTowToFalseWhenNotYes()
    {
        // Arrange
        $params = [
            "inspectionId" => "12345",
            "isHighPriorityTow" => "no",
            "towFromLocation" => "from_location",
            "towToLocation" => "to_location",
            "towDriverId" => 1
        ];

        $mockTowStatusRepository = $this->createMock(TowStatusRepository::class);
        $mockTowStatusRepository
            ->expects($this->once())
            ->method("save")
            ->will($this->returnArgument(0));

        $mockTowDriverRepository = $this->createMock(TowDriverRepository::class);

        $mockTowPriceCalculator = $this->createMock(TowPriceCalculator::class);
        $mockTowPriceCalculator
            ->expects($this->once())
            ->method("calculateTowPrice")
            ->willReturn(10.00);

        $service = new TowStatusService($mockTowStatusRepository, $mockTowDriverRepository, $mockTowPriceCalculator);

        // Act
        $result = $service->createTowStatus($params);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertEquals($result->savedEntity->isHighPriorityTow, 0);
    }
}
