<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            // Entry Points
            'select-operation' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/select-operation',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'selectOperation',
                    ],
                ],
            ],
            'enter-new-deal' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/enter-new-deal',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'enterNewDeal',
                    ],
                ],
            ],
            'pending-deals' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/pending-deals',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'pendingDeals',
                    ],
                ],
            ],
            'add-to-a-deal' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/add-to-a-deal',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'addToADeal',
                    ],
                ],
            ],
            'edit-old-deal' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/edit-old-deal',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'editOldDeal',
                    ],
                ],
            ],

            // Authentication
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'login',
                    ],
                ],
            ],
            'external-login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/external-login',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'externalLogin',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],

            // API
            'configuration-enter-new-deal' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/configuration/enter-new-deal',
                    'defaults' => [
                        'controller' => Controller\ConfigurationController::class,
                        'action' => 'enterNewDeal'
                    ],
                ],
            ],
            'configuration-add-to-a-deal' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/configuration/add-to-a-deal',
                    'defaults' => [
                        'controller' => Controller\ConfigurationController::class,
                        'action' => 'addToADeal'
                    ],
                ],
            ],
            'configuration-edit-old-deal' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/configuration/edit-old-deal',
                    'defaults' => [
                        'controller' => Controller\ConfigurationController::class,
                        'action' => 'editOldDeal'
                    ],
                ],
            ],
            'vehicle' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/vehicle/:action',
                    'defaults' => [
                        'controller' => Controller\VehicleController::class,
                    ],
                ],
            ],
            'tow-order' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/tow-order[/:id]',
                    'defaults' => [
                        'controller' => Controller\TowOrderController::class,
                    ],
                ],
            ],
            'deal' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/deal[/:id]',
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                    ],
                ],
            ],

        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
