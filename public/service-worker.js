var cacheName = 'CsarBuyerApp-v0.9';
var dataCacheName = 'CsarBuyerAppData-v0.9';

var resourcesToCacheOnInstall = [
    'pending-deals',

    'api/configuration/enter-new-deal',
    'api/configuration/edit-old-deal',

    'js/select-operation.main.js',
    'js/enter-new-deal.bundle.js',
    'js/pending-deals.bundle.js',
    'js/add-to-a-deal.bundle.js',
    'js/edit-old-deal.bundle.js',

    'js/lib/jquery.js',
    'js/lib/jquery.validate.js',
    'js/lib/additional-methods.js',
    'js/lib/material-components-web.min.js',
    'js/lib/bootstrap.js',
    'js/lib/bootstrap-datepicker.js',
    'js/lib/sketchpad.js',

    'css/material/drawer.css',
    'css/material/toolbar.css',
    'css/material/typography.css',
    'css/bootstrap.css',
    'css/bootstrap-datepicker3.standalone.css',
    'css/style.css',

    'img/loading_icon.gif',
    'img/loading_small.gif',
    'img/logo.png',
    'img/seller_id_placeholder.png',
    'img/seller_name_placeholder.png',
    'img/vehicle_placeholder.png',

    'fonts/glyphicons-halflings-regular.woff2'
];

var resourcesToCacheCacheFirstUpdateCache = [

];

var resourcesToCacheNetworkFirstUpdateCache = [
    'select-operation',
    'enter-new-deal',
    'pending-deals',

    'api/configuration/enter-new-deal',
    'api/configuration/pending-deals',
    'api/configuration/edit-old-deal',

    'js/select-operation.main.js',
    'js/enter-new-deal.bundle.js',
    'js/pending-deals.bundle.js',
    'js/add-to-a-deal.bundle.js',
    'js/edit-old-deal.bundle.js',

    'css/style.css'
];

var cachingStrategies = {
    cacheFirstUpdateCache: function(e) {
        e.respondWith(
            caches.match(e.request.url).then(function(response) {
                if (response) {
                    caches.open(cacheName).then(function(cache) {
                        cache.add(e.request);
                    });

                    return response;
                }
                else {
                    return fetch(e.request).then(function(response) {
                        if (response.ok) {
                            caches.open(cacheName).then(function(cache) {
                                cache.put(e.request.url, response.clone());
                            });
                        }

                        return response.clone();
                    });
                }
            })
        );
    },

    networkFirstUpdateCache: function(e) {
        e.respondWith(
            fetch(e.request)
            .then(function(response) {
                if (response.ok) {
                    caches.open(cacheName).then(function(cache) {
                        cache.put(e.request.url, response.clone());
                    });
                }

                return response.clone();
            })
            .catch(function(error) {
                return caches.match(e.request.url);
            })
        );
    },

    cacheFirst: function(e) {
        /*
        * The app is asking for app shell files. In this scenario the app uses the
        * "Cache, falling back to the network" offline strategy:
        * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
        */
        e.respondWith(
            caches.match(e.request.url).then(function(response) {
                return response || fetch(e.request);
            })
        );
    }
};

// Service Worker Event Handlers
// =============================

self.addEventListener('install', function(e) {

    console.log('[ServiceWorker] Install');

    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(resourcesToCacheOnInstall);
        })
    );
});

self.addEventListener('activate', function(e) {

    console.log('[ServiceWorker] Activate');

    e.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if (key !== cacheName && key !== dataCacheName) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key);
                }
            }));
        })
    );

    return self.clients.claim();
});

self.addEventListener('fetch', function(e) {

    console.log('[Service Worker] Fetch', e.request.url);

    // We can use this to know the client from which the fetch event is being originated.
    // It seems that navigate requests don't have a client assigned.
    // self.clients.get(e.clientId).then(function (client) {
    //     console.log("client url: "client.url);
    // });

    // To know if it's a navigation request.
    // We can use this to differentiate between requests to
    // enter-new-deal and api/configuration/enter-new-deal
    // if (e.request.mode == 'navigate') { 

    // }

    // We do this to make sure we only get the useful part of the url.
    // That is, the string after all of zend's public nonsense and prod's folder structure
    var cleanResourceUrl = e.request.url.replace(self.registration.scope, "");

    if (resourcesToCacheNetworkFirstUpdateCache.includes(cleanResourceUrl)) {
        cachingStrategies.networkFirstUpdateCache(e);
    }
    else {
        cachingStrategies.cacheFirst(e);
    }
});
