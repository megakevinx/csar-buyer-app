import ko from "knockout";

import RoutingEngine from "./app/routing/RoutingEngine";
import AlertsManager from "./app/alerts/AlertsManager";
import GenericApiClient from "./app/apiclients/GenericApiClient";
import UrlHelper from "./app/helpers/UrlHelper";

import EditOldDealViewModel from "./app/viewmodels/EditOldDealViewModel";

$(document).ready(function () {

    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('./service-worker.js')
            .then(function() { console.log('Service Worker Registered'); });
    }

    $("div.validation-error-alert a.close").click(function(eventObject) {
        $(eventObject.target.parentElement).hide();
    });

    $('.date-input-field').datepicker({
        format: "yyyy-mm-dd"
    });

    ko.bindingHandlers.fadeVisible = {
        init: function(element, valueAccessor) {
            // Initially set the element to be instantly visible/hidden depending on the value
            var value = valueAccessor();
            $(element).toggle(ko.unwrap(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
        },
        update: function(element, valueAccessor) {
            // Whenever the value subsequently changes, slowly fade the element in or out
            var value = valueAccessor();
            ko.unwrap(value) ? $(element).slideDown() : $(element).slideUp();
        }
    };

    fetch("api/configuration/edit-old-deal")
    .then(response => response.json())
    .then(data => {
        let vehicleYears = data.vehicleYears;
        let vehicleMakes = data.vehicleMakes;

        ko.applyBindings(
            new EditOldDealViewModel(
                vehicleYears, vehicleMakes,
                new AlertsManager(),
                new RoutingEngine(),
                new GenericApiClient(),
                new UrlHelper(),
                $
            )
        );

        $("#overlay").fadeOut();
    })
    .catch(error => {
        console.log(error);
    });
});
