/**
 * Validates vehicle purchase details data
 */

export default class VehiclePurchaseDetailsValidator {

    constructor(formSelector, errorMessagesContainerSelector) {
        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        this.validationOptions = {
            rules: {
                vehicle_wheels: {
                    required: true
                },
                vehicle_body_style: {
                    required: true
                },
                vehicle_purchase_price: {
                    required: true,
                    currency: ["$", false],
                    max: 999.99
                },
                vehicle_purchase_details_notes: {
                    required: false,
                    maxlength: 500,
                    normalizer: this.trimNormalizer
                }
            },

            messages: {
                vehicle_wheels: {
                    required: "Please select the vehicle's wheels"
                },
                vehicle_body_style: {
                    required: "Please select the vehicle's body style",
                },
                vehicle_purchase_price: {
                    required: "Please enter the vehicle's purchase price",
                    currency: "The vehicle's purchase price is not valid",
                    max: "The vehicle's purchase price must be below 999.99",
                },
                vehicle_purchase_details_notes: {
                    maxlength: "The notes must contain fewer than 500 characters"
                }
            },

            ignore: '',
            errorElement : 'div',
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    trimNormalizer(value) {
        return $.trim(value);
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }
}
