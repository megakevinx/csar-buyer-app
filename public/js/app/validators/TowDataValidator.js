/**
 * Validates basic tow data
 */

export default class TowDataValidator {

    constructor(formSelector, errorMessagesContainerSelector) {
        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        this.validationOptions = {
            rules: {
                is_high_priority_tow: {
                    required: true
                }
            },

            messages: {
                is_high_priority_tow: {
                    required: "Please specify whether it's an emergency tow"
                }
            },

            ignore: '',
            errorElement : 'div',
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }
}
