/**
 * Validates vehicle identification data
 */

export default class VehicleIdentificationValidator {

    constructor(formSelector, errorMessagesContainerSelector) {

        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        // $.validator.addMethod(
        //     "vin",
        //     function(value, element) {
        //         var re = new RegExp("^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$");;
        //         return this.optional(element) || re.test(value);
        //     },
        //     "The VIN is not valid"
        // );

        $.validator.addMethod("vin", function(v) {
            if (v === '') {
                return true;
            }
            if (v.length !== 17) {
                return false;
            }
            if (!/^[a-hj-npr-z0-9]{8}[0-9xX][a-hj-npr-z0-9]{8}$/i.test(v)) {
                return false;
            }
            v = v.toUpperCase();
        
            var chars   = {
                A: 1,   B: 2,   C: 3,   D: 4,   E: 5,   F: 6,   G: 7,   H: 8,
                J: 1,   K: 2,   L: 3,   M: 4,   N: 5,           P: 7,           R: 9,
                S: 2,   T: 3,   U: 4,   V: 5,   W: 6,   X: 7,   Y: 8,   Z: 9,
                '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '0': 0
            },
            weights = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2],
            sum     = 0,
            length  = v.length;
        
            for (var i = 0; i < length; i++) {
                sum += chars[v.charAt(i) + ''] * weights[i];
            }
        
            var reminder = sum % 11;
            if (reminder === 10) {
                reminder = 'X';
            }
            return (reminder + '') === v.charAt(8);
        }, "The specified vehicle identification number (VIN) is invalid.");

        this.validationOptions = {
            rules: {
                vehicle_vin: {
                    required: true,
                    vin: true
                },
                vehicle_has_title: {
                    required: true
                },
                vehicle_title: {
                    required: {
                        depends: function(elem) {
                            return $("[name='vehicle_has_title']:checked").val() == 'yes';
                        }
                    },
                    maxlength: 45,
                    alphanumeric: true
                },
                vehicle_identification_notes: {
                    required: false,
                    maxlength: 500,
                    normalizer: this.trimNormalizer
                }
            },

            messages: {
                vehicle_vin: {
                    required: "Please enter the vehicle's VIN",
                    vin: "The vehicle's VIN is not valid"
                },
                vehicle_has_title: {
                    required: "Please specify whether the vehicle has title"
                },
                vehicle_title: {
                    required: "Please enter the vehicle's title",
                    maxlength: "The customer's city must contain fewer than 45 characters",
                    alphanumeric: "The vehicle's title is not valid"
                },
                vehicle_identification_notes: {
                    maxlength: "The notes must contain fewer than 500 characters"
                }
            },

            ignore: '',
            errorElement : 'div',
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    trimNormalizer(value) {
        return $.trim(value);
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }
}
