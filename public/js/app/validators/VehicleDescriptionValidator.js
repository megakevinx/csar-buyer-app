/**
 * Validates vehicle description data
 */

export default class VehicleDescriptionValidator {

    constructor(formSelector, errorMessagesContainerSelector) {
        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        this.validationOptions = {
            rules: {
                vehicle_year: {
                    required: true,
                    digits: true
                },
                vehicle_make: {
                    required: true
                },
                vehicle_model: {
                    required: true
                },
                vehicle_color: {
                    required: true,
                    maxlength: 25,
                    normalizer: this.trimNormalizer
                },
                vehicle_description_notes: {
                    required: false,
                    maxlength: 500,
                    normalizer: this.trimNormalizer
                }
            },

            messages: {
                vehicle_year: {
                    required: "Please select the vehicle's year",
                    digits: "The vehicle's year is not valid"
                },
                vehicle_make: {
                    required: "Please select the vehicle's make"
                },
                vehicle_model: {
                    required: "Please select the vehicle's model"
                },
                vehicle_color: {
                    required: "Please enter the vehicle's color",
                    maxlength: "The vehicle's color must contain fewer than 25 characters"
                },
                vehicle_description_notes: {
                    maxlength: "The notes must contain fewer than 500 characters"
                }
            },

            ignore: '',
            errorElement : 'div',
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    trimNormalizer(value) {
        return $.trim(value);
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }
}
