/**
 * Validates customer data
 */

export default class CustomerDataValidator {

    constructor(formSelector, errorMessagesContainerSelector) {
        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        this.validationOptions = {
            rules: {
                customer_name: {
                    required: true,
                    normalizer: this.trimNormalizer
                },
                customer_id: {
                    required: true,
                    maxlength: 35,
                    normalizer: this.trimNormalizer
                },
                customer_gender: {
                    required: true
                },
                customer_phone: {
                    required: true,
                    phoneUS: true
                },
                customer_street_address: {
                    required: true,
                    maxlength: 75,
                    normalizer: this.trimNormalizer
                },
                customer_zip: {
                    required: true,
                    zipcodeUS: true
                },
                customer_city: {
                    required: true,
                    maxlength: 75,
                    normalizer: this.trimNormalizer
                },
                customer_data_notes: {
                    required: false,
                    maxlength: 500,
                    normalizer: this.trimNormalizer
                }
            },

            messages: {
                customer_name: {
                    required: "Please enter the customer's name"
                },
                customer_id: {
                    required: "Please enter the customer's ID or Driver's License",
                    maxlength: "The customer's ID number must contain fewer than 35 characters"
                },
                customer_gender: {
                    required: "Please enter the customer's gender"
                },
                customer_phone: {
                    required: "Please enter the customer's phone number",
                    phoneUS: "The customer's phone number is not valid"
                },
                customer_street_address: {
                    required: "Please enter the customer's street address",
                    maxlength: "The customer's street address must contain fewer than 75 characters"
                },
                customer_zip: {
                    required: "Please enter the customer's Zip code",
                    zipcodeUS: "The customer's Zip code is not valid"
                },
                customer_city: {
                    required: "Please enter the customer's city",
                    maxlength: "The customer's city must contain fewer than 75 characters"
                },
                customer_data_notes: {
                    maxlength: "The notes must contain fewer than 500 characters"
                }
            },

            ignore: '',
            errorElement : 'div',
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    trimNormalizer(value) {
        return $.trim(value);
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }
}
