import "sammy";

export default class RoutingEngine {
    setup(routeConfig) {
        return Sammy(routeConfig);
    }
} 