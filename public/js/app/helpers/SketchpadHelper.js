/**
 * Helper that wraps around the Sketchpad object and exposes
 * its key functionality
 */
export default class SketchpadHelper {
    /**
     * Creates a new SketchpadHelper
     * @param {string} elementSelector
     * @param {string} containerSelector
     */
    constructor(elementSelector, containerSelector) {
        this.elementSelector = elementSelector;
        this.containerSelector = containerSelector;

        this.sketchpad = null;
        this.reset();
    }

    createSketchpadInstance() {
        let sketchpadHeight = $(this.containerSelector).height();

        if ($(window).height() > $(window).width()) {
            sketchpadHeight = $(this.containerSelector).width() * 0.7;
        }

        return new Sketchpad({
            element: this.elementSelector,
            width: $(this.containerSelector).width(),
            height: sketchpadHeight,
            penSize: 1
        });
    }

    /**
     * Creates a new sketchpad instance and ditches the old one. It will create a
     * sketchpad with the correct size adapting to the size of the screen.
     */
    reset() {
        this.sketchpad = this.createSketchpadInstance();
        this.baseFill('white');
    }

    /**
     * Fills the entire canvas with the given collor
     * @param {string} color
     */
    baseFill(color) {
        let canvas = this.sketchpad.canvas;
        var ctx = canvas.getContext("2d");
        ctx.fillStyle = color;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    }

    /**
     * Clears the canvas
     */
    clear() {
        this.sketchpad.clear();
        this.baseFill('white');
    }

    /**
     * Gets the Data URL representation for the picture in the canvas
     */
    getAsDataUrl() {
        return this.sketchpad.canvas.toDataURL("image/jpeg");
    }
}
