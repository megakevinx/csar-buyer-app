export default class UrlHelper {
    setWindowLocationHash(hash) {
        window.location.hash = hash;
    }

    getQueryStringParam(paramKey) {
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has(paramKey)) {
            return searchParams.get(paramKey);
        }
        else return null;
    }

    reloadPage() {
        window.location.assign(window.location.pathname);
    }

    goToPage(url) {
        window.location.href = url;
    }
}
