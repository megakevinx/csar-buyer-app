/**
 * Provides functions to help with file management
 */
export default class PromisedFileReader {
    constructor() {}

    /**
     * Returns the base64 Data URL representation of a given file
     * @param {*} file 
     */
    readAsDataURL(file) {
        return new Promise((resolve, reject) => {
            let reader = new FileReader();

            reader.onload = e => {
                // Here is your base 64 encoded file. Do with it what you want.
                resolve(reader.result)
            };

            reader.readAsDataURL(file);
        });
    }
}