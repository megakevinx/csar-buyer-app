/**
 * Uses img and canvass DOM Elements to srink an image.
 */
export default class ImageProcessor {
    constructor() {
        this.MAX_WIDTH = 800;
        this.MAX_HEIGHT = 600;
    }

    shrinkImage(imageDataUrl) {
        return new Promise((resolve, reject) => {
            let img = document.createElement("img");

            img.onload = e => {
                let canvas = document.createElement("canvas");
                let ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                let width = img.width;
                let height = img.height;

                if (width > height) {
                    if (width > this.MAX_WIDTH) {
                        height *= this.MAX_WIDTH / width;
                        width = this.MAX_WIDTH;
                    }
                }
                else {
                    if (height > this.MAX_HEIGHT) {
                        width *= this.MAX_HEIGHT / height;
                        height = this.MAX_HEIGHT;
                    }
                }

                canvas.width = width;
                canvas.height = height;

                ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
                let smallerImg = canvas.toDataURL();

                resolve(smallerImg);
            };

            img.src = imageDataUrl;
        });
    }
}
