export default class GenericApiClient {
    constructor() {

    }

    handleResponse(response) {
        const INTERNAL_SERVER_ERROR = 500;

        if (response.status >= INTERNAL_SERVER_ERROR) {
            throw new Error(response.type + " " + response.statusText + " " + response.status.toString() + " " + response.url);
        }
        else {
            return response.json();
        }
    }

    getVehicleModels(vehicleMake, vehicleYear) {
        let url = "api/vehicle/models?" + $.param({ "make": vehicleMake, "year": vehicleYear });
        return fetch(url).then(this.handleResponse);
    }

    getLocation(zipCode) {
        return fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + zipCode + "&sensor=true")
            .then(this.handleResponse);
    }

    /**
     * Creates a new deal and tow order
     * @param {*} towOrderData 
     */
    createTowOrder(towOrderData) {
        return fetch("api/tow-order", {
            method: "post",
            credentials: "include",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(towOrderData)
        }).then(this.handleResponse);
    }

    /**
     * Updates an existing deal and creates a new tow order for it
     * @param {*} towOrderData 
     */
    updateTowOrder(towOrderData) {
        return fetch(`api/tow-order/${ towOrderData.dealNumber }`, {
            method: "put",
            credentials: "include",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(towOrderData)
        }).then(this.handleResponse);
    }

    /**
     * Updates an existing deal
     */
    updateDeal(dealData) {
        return fetch(`api/deal/${ dealData.dealNumber }`, {
            method: "put",
            credentials: "include",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(dealData)
        }).then(this.handleResponse);
    }

    getDeal(dealNumber) {
        return fetch(`api/deal/${ dealNumber }`, {
            method: "get",
            credentials: "include"
        })
            .then(this.handleResponse);
    }

    searchDeals(searchParams) {
        let url = "api/deal?" + $.param(searchParams);

        return fetch(url, {
            credentials: "include"
        }).then(this.handleResponse);
    }
}