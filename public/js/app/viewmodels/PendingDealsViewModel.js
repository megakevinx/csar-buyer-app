import ko from "knockout";

/*eslint-disable */
import AlertsManager from "../alerts/AlertsManager";
import GenericApiClient from "../apiclients/GenericApiClient";
import LocalDataRepository from "../repositories/LocalDataRepository";
/*eslint-enable */

/**
 * Main view model for the pending deals screen.
 */
export default class PendingDealsViewModel {
    /**
     * @param {AlertsManager} alertsManager
     * @param {GenericApiClient} genericApiClient
     * @param {LocalDataRepository} localDataRepository
     * @param {jQuery} $
     */
    constructor(alertsManager, genericApiClient, localDataRepository, $) {
        // Configuration

        this.alertsManager = alertsManager;
        this.genericApiClient = genericApiClient;
        this.localDataRepository = localDataRepository;
        this.$ = $;

        this.errorAlertsIds = {
            zipNotSupported: "ZIP_NOT_SUPPORTED_ALERT",
            invalidInputData: "INPUT_DATA_INVALID_ALERT",
        };

        this.isLoadingRequest = ko.observable(false);

        // Properties
        this.pendingTowOrders = ko.observableArray();

        // Initialize
        this.loadTowOrders();
    }

    /**
     * Loads all of the pending tow orders from the local data storage.
     * Assigns this.pendingTowOrders to a list of view models.
     */
    loadTowOrders() {
        this.pendingTowOrders.removeAll();

        this.localDataRepository.getAllTowOrders()
            .then(towOrderRows => {
                let towOrderViewModels = towOrderRows.map((towOrderRow) => {
                    return new TowOrderViewModel(towOrderRow, this);
                });

                towOrderViewModels.forEach(towOrderViewModel => {
                    this.pendingTowOrders.push(towOrderViewModel);
                });
            });
    }

    submitTowOrder(towOrderData) {
        this.isLoadingRequest(true);

        console.log("POST Request sent to api/tow-order");

        this.genericApiClient.createTowOrder(towOrderData)
            .then(response => {

                this.isLoadingRequest(false);
                console.log(response);

                if (response.status == 200) {
                    this.alertsManager.dismiss(this.errorAlertsIds.zipNotSupported);
                    this.alertsManager.dismiss(this.errorAlertsIds.invalidInputData);

                    this.alertsManager.showSuccess(
                        this.$("div#tow_data_alerts_container"),
                        "A new deal has been created and the tow was ordered." +
                        `Deal Number: <b>${ response.dealReferenceNumber }</b>. ` +
                        `Tow Number: <b>${ response.towNumber }</b>`
                    );

                    this.localDataRepository.removeTowOrder(towOrderData.id).then(() => {
                        this.loadTowOrders();
                    });
                }
                else if (response.status == 400) {
                    let editPendingTowOrderUrl = this.getModifyTowOrderLink(towOrderData);

                    if (response.errorCode == "ZIP_NOT_SUPPORTED") {
                        let editPendingTowOrderCustomerDataLink = `<a href='${ editPendingTowOrderUrl }#customer-data'>Customer Data screen</a>`;

                        this.alertsManager.showDanger(
                            this.$("div#tow_data_alerts_container"),
                            `We don't offer our services in the customer's area, please choose a diferent Zip code in the ${ editPendingTowOrderCustomerDataLink }`,
                            this.errorAlertsIds.zipNotSupported
                        );
                    }
                    else if (response.errorCode == "INPUT_DATA_INVALID") {
                        let editPendingTowOrderLink = `<a href='${ editPendingTowOrderUrl }#home'>modify the tow order</a>`;

                        this.alertsManager.showDanger(
                            this.$("div#tow_data_alerts_container"),
                            response.errorMessage + `Please ${ editPendingTowOrderLink } and try to submit again`,
                            this.errorAlertsIds.invalidInputData
                        );
                    }
                }
                else if (response.status == 401) {
                    this.alertsManager.showDanger(
                        this.$("div#tow_data_alerts_container"),
                        "Your session has expired. Please try logging back in again."
                    );
                }
                else {
                    this.alertsManager.showDanger(
                        this.$("div#tow_data_alerts_container"),
                        "The has been a server side error. Please try again later or contact your system's administrator. Error: " + JSON.stringify(response)
                    );
                }
            })
            .catch(error => {
                this.isLoadingRequest(false);
                console.log(error);

                this.alertsManager.showDanger(
                    this.$("div#tow_data_alerts_container"),
                    "There has been an error. Please make sure that you have good internet connection, try again later or contact your system's administrator. Error: " + error.message
                );
            });
    }

    getModifyTowOrderLink(towOrderData) {
        return `enter-new-deal?preload=true&id=${ towOrderData.id }`;
    }

    cancelTowOrder(towOrderData) {
        this.alertsManager.dismiss(this.errorAlertsIds.zipNotSupported);
        this.alertsManager.dismiss(this.errorAlertsIds.invalidInputData);

        this.localDataRepository.removeTowOrder(towOrderData.id).then(() => {

            this.alertsManager.showSuccess(
                this.$("div#tow_data_alerts_container"),
                "Your pending deal on a" +
                `${ towOrderData.vehicleColor } ${ towOrderData.vehicleYear } ${ towOrderData.vehicleMake } ${ towOrderData.vehicleModel }` +
                " was sucessfully deleted."
            );

            this.loadTowOrders();
        });
    }
}

/**
 * View model that represents a single Tow Order in the list.
 */
class TowOrderViewModel {
    /**
     * @param {Object} data
     * @param {PendingDealsViewModel} containerViewModel
     */
    constructor(data, containerViewModel) {
        // Configuration
        this.parent = containerViewModel;
        this.innerData = data;

        // Properties
        this.towCompanyId = ko.observable(data.towCompanyId);
        //this.towFromLocation = ko.observable(data.towFromLocation);
        //this.towToLocation = ko.observable(data.towToLocation);
        this.isHighPriorityTow = ko.observable(data.isHighPriorityTow);

        this.customerName = ko.observable(data.customerName);
        this.customerId = ko.observable(data.customerId);
        this.customerIdPicture = ko.observable(data.customerIdPicture);
        this.customerPhone = ko.observable(data.customerPhone);
        this.customerStreetAddress = ko.observable(data.customerStreetAddress);
        this.customerZip = ko.observable(data.customerZip);
        this.customerCity = ko.observable(data.customerCity);

        this.vehicleYear = ko.observable(data.vehicleYear);
        this.vehicleMake = ko.observable(data.vehicleMake);
        this.vehicleModel = ko.observable(data.vehicleModel);
        this.vehicleColor = ko.observable(data.vehicleColor);

        this.vehicleVin = ko.observable(data.vehicleVin);
        this.vehicleTitle = ko.observable(data.vehicleTitle);

        this.vehicleWheelType = ko.observable(data.vehicleWheelType);
        this.vehicleBodyStyle = ko.observable(data.vehicleBodyStyle);
        this.vehiclePurchasePrice = ko.observable(data.vehiclePurchasePrice);
    }

    getTowCompanyDisplayName() {
        return this.parent.getTowCompanyDisplayName(this.innerData);
    }

    submitTowOrderOnClick() {
        this.parent.submitTowOrder(this.innerData);
    }

    getModifyTowOrderLink() {
        return this.parent.getModifyTowOrderLink(this.innerData);
    }

    cancelTowOrderOnClick() {
        this.parent.cancelTowOrder(this.innerData);
    }
}
