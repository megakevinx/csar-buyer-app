import ko from "knockout";

/**
 * Main view model for the pending deals screen.
 */
export default class AddToANewDealViewModel {
    /**
     * @param {Array} recentDeals
     */
    constructor(recentDeals) {
        // Configuration
        this.recentDeals = ko.observableArray();

        // Initialize
        if (recentDeals) {
            this.loadDeals(recentDeals);
        }
    }

    loadDeals(dealsToLoad) {
        let dealViewModels = dealsToLoad.map(d => new DealViewModel(d));

        dealViewModels.forEach(dvm => {
            this.recentDeals.push(dvm);
        });
    }
}

/**
 * View model that represents a single Deal in the list.
 */
class DealViewModel {
    /**
     * @param {Object} data
     */
    constructor(data) {
        // Properties
        this.dealId = ko.observable(data.id);
        this.createdDate = ko.observable(data.createdDate);

        this.customerName = ko.observable(data.sellerName);
        this.customerIdPicture = ko.observable();
        this.customerStreetAddress = ko.observable(data.vehicleStreetAddress);
        this.customerZip = ko.observable(data.vehicleZip);
        this.customerCity = ko.observable(data.vehicleCity);

        this.vehicleYear = ko.observable(data.vehicleYear);
        this.vehicleMake = ko.observable(data.vehicleMake);
        this.vehicleModel = ko.observable(data.vehicleModel);
        this.vehicleColor = ko.observable(data.vehicleColor);

        this.vehicleVin = ko.observable(data.vehicleVin);
        this.vehicleTitle = ko.observable(data.vehicleTitle);

        this.vehiclePurchasePrice = ko.observable(data.vehiclePurchasePrice);
    }

    getOpenDealUrl() {
        return `enter-new-deal?clone=true&id=${ this.dealId() }`;
    }
}
