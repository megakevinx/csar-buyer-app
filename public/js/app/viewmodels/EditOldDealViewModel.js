// @ts-check

/**
 * Main ViewModel for the edit old deal screen.
 */

import ko from "knockout";

export default class EditOldDealViewModel {
    constructor(
        vehicleYears, vehicleMakes,
        alertsManager,
        routingEngine,
        genericApiClient,
        urlHelper,
        $
    ) {
        // Configuration
        this.screens = {
            home: "home",
            searchResults: "search-results"
        };

        this.errorAlertsIds = {
            unableToLoadModels: "UNABLE_TO_LOAD_VEHICLE_MODELS",
            dealNotFound: "DEAL_NOT_FOUND"
        };

        this.alertsManager = alertsManager;

        this.genericApiClient = genericApiClient;
        this.urlHelper = urlHelper;
        this.$ = $;

        this.vehicleYears = vehicleYears;
        this.vehicleMakes = vehicleMakes;
        this.vehicleModels = ko.observableArray([]);

        this.currentScreen = ko.observable(this.screens.home);

        this.isLoadingRequest = ko.observable(false);
        this.manualVehicleModelEntryEnabled = ko.observable(false);

        // Pagination TODO: refactor away
        this.pageSize = ko.observable(10);
        this.pageIndex = ko.observable(1);
        this.totalRowCount = ko.observable(0);

        this.numberOfPages = ko.pureComputed(function () {
            return this.totalRowCount() / this.pageSize();
        }, this);

        this.dataPages = ko.pureComputed(function () {
            // This produces an array of ints from 1 to numberOfPages
            return Array.from({ length: this.numberOfPages() }, (_, i) => i + 1);
        }, this);

        // We need to do this instead of a normal class method
        // because ko binds "this" to the child "viewmodel" when
        // this is called from withing a sub-scope like a foreach
        let self = this;
        this.showPage = function (selectedPageIndex) {
            self.pageIndex(selectedPageIndex);
            self.search();
        };

        // Properties
        this.dealNumber = ko.observable();

        this.fromDate = ko.observable();
        this.toDate = ko.observable();

        this.selectedVehicleYear = ko.observable();
        this.selectedVehicleMake = ko.observable();
        this.selectedVehicleModel = ko.observable();
        this.vehicleColor = ko.observable();

        this.searchResultDeals = ko.observableArray();

        // Start app
        routingEngine
            .setup(this.getRouteConfig(this, this.screens))
            .run("#home");
    }

    getRouteConfig(viewModel, screens) {
        return function() {
            // here, "this" is Sammy
            this.get("#home", () => {
                viewModel.currentScreen(screens.home);
            });

            this.get("#search-results", () => {
                viewModel.currentScreen(screens.searchResults);
            });
        };
    }

    navigateTo(screenId) {
        this.urlHelper.setWindowLocationHash(screenId);
    }

    shouldDisplayScreen(screenId) {
        return this.currentScreen() == screenId;
    }

    vehicleYearOnSelect() {
        this.loadVehicleModels();
    }

    vehicleMakeOnSelect() {
        this.loadVehicleModels();
    }

    loadVehicleModels() {
        if (this.selectedVehicleYear() && this.selectedVehicleMake()) {

            console.log("GET Request sent to api/vehicle/models");

            this.genericApiClient.getVehicleModels(
                this.selectedVehicleMake(),
                this.selectedVehicleYear()
            ).then(response => {
                this.alertsManager.dismiss(this.errorAlertsIds.unableToLoadModels);
                this.manualVehicleModelEntryEnabled(false);
                this.vehicleModels(response.models);
            }).catch(error => {
                console.log(error);

                this.manualVehicleModelEntryEnabled(true);

                this.alertsManager.showWarning(
                    this.$("div#vehicle_description_alerts_container"),
                    "Could not load vehicle models. Please enter one manually.",
                    this.errorAlertsIds.unableToLoadModels
                );
            });
        }
    }

    fetchDealOnClick() {
        this.isLoadingRequest(true);

        this.genericApiClient.getDeal(this.dealNumber())
            .then(response => {
                this.isLoadingRequest(false);

                if (response.deal) {
                    this.urlHelper.goToPage(`enter-new-deal?edit=true&id=${ this.dealNumber() }`);
                }
                else {
                    this.alertsManager.showDanger(
                        this.$("div#fetch_deal_alerts_container"),
                        "Couldn't find any deal with that number.",
                        this.errorAlertsIds.dealNotFound
                    );
                }
            })
            .catch(error => {
                this.isLoadingRequest(false);
                console.log(error);
                this.alertsManager.showDanger(
                    this.$("div#fetch_deal_alerts_container"),
                    "Couldn't find any deal with that number. Make sure that you're connected to the internet.",
                    this.errorAlertsIds.dealNotFound
                );
            });
    }

    loadDeals(dealsToLoad) {
        this.searchResultDeals.removeAll();

        let dealViewModels = dealsToLoad.map(d => new DealViewModel(d));

        dealViewModels.forEach(dvm => {
            this.searchResultDeals.push(dvm);
        });
    }

    searchDealOnClick() {
        this.showPage(1);
    }

    search() {
        let searchParams = {
            pageIndex: this.pageIndex(),
            pageSize: this.pageSize(),
            fromDate: this.fromDate(),
            toDate: this.toDate(),
            vehicleYear: this.selectedVehicleYear(),
            vehicleMake: this.selectedVehicleMake(),
            vehicleModel: this.selectedVehicleModel(),
            vehicleColor: this.vehicleColor()
        };

        this.isLoadingRequest(true);

        this.genericApiClient.searchDeals(searchParams)
            .then(response => {
                this.isLoadingRequest(false);
                if (response.deals && response.deals.length > 0) {
                    this.loadDeals(response.deals);
                    this.totalRowCount(response.totalDealsCount);
                    this.navigateTo(this.screens.searchResults);
                    this.alertsManager.dismiss(this.errorAlertsIds.dealNotFound);

                }
                else {
                    this.searchResultDeals.removeAll();
                    this.totalRowCount(0);
                    this.alertsManager.showDanger(
                        this.$("div#search_deal_alerts_container"),
                        "Couldn't find any deals with the given search criteria.",
                        this.errorAlertsIds.dealNotFound
                    );
                }
            }).catch(error => {
                this.isLoadingRequest(false);
                console.log(error);
                this.alertsManager.showDanger(
                    this.$("div#search_deal_alerts_container"),
                    "Couldn't find any deals with the given search criteria. Make sure that you're connected to the internet.",
                    this.errorAlertsIds.dealNotFound
                );
            });
    }

    // Navigation
    showNextPage() {
        let newPageIndex = this.pageIndex();

        if (!(this.pageIndex() >= this.numberOfPages())) {
            newPageIndex = (this.pageIndex() + 1);
        }

        this.showPage(newPageIndex);
    }

    showPreviousPage() {
        let newPageIndex = this.pageIndex();

        if (!(this.pageIndex() <= 1)) {
            newPageIndex = (this.pageIndex() - 1);
        }

        this.showPage(newPageIndex);
    }
}

// TODO: eliminate duplicated code
/**
 * View model that represents a single Deal in the list.
 */
class DealViewModel {
    /**
     * @param {Object} data
     */
    constructor(data) {
        // Properties
        this.dealNumber = ko.observable(data.id);
        this.createdDate = ko.observable(data.createdDate);

        this.customerName = ko.observable(data.sellerName);
        this.customerIdPicture = ko.observable();
        this.customerStreetAddress = ko.observable(data.vehicleStreetAddress);
        this.customerZip = ko.observable(data.vehicleZip);
        this.customerCity = ko.observable(data.vehicleCity);

        this.vehicleYear = ko.observable(data.vehicleYear);
        this.vehicleMake = ko.observable(data.vehicleMake);
        this.vehicleModel = ko.observable(data.vehicleModel);
        this.vehicleColor = ko.observable(data.vehicleColor);

        this.vehicleVin = ko.observable(data.vehicleVin);
        this.vehicleTitle = ko.observable(data.vehicleTitle);

        this.vehiclePurchasePrice = ko.observable(data.vehiclePurchasePrice);
    }

    getOpenDealUrl() {
        return `enter-new-deal?edit=true&id=${ this.dealNumber() }`;
    }
}