// eslint-disable-next-line no-unused-vars
import EnterNewDealViewModel from "../EnterNewDealViewModel";

/**
 * Strategy
 */
export default class BaseStrategy {

    constructor(viewModel) {
        this.viewModel = viewModel;
    }

    initViewModel() { }

    notifyInputDataFormCompleted() { }

    notifyDealSubmitted() { }

    notifyTowOrderStoredLocally() {  }

    notifyEnteringNewDeal() {
        return {
            then: (callback) => callback()
        };
    }

    getSubmitTowOrderApiCall() {
        return towOrderData => this.viewModel.genericApiClient.createTowOrder(towOrderData);
    }

    prepareTowOrderData(towOrderData) {
        return towOrderData;
    }

    notifyDealSubmissionFailed(towOrderData, error) {
        this.viewModel.localDataRepository.saveTowOrder(towOrderData)
            .then(() => {

                this.notifyTowOrderStoredLocally();

                this.viewModel.towOrderStoredLocally(true);

                let pendingOrdersScreenLink = "<a href='pending-deals'>Pending Deals screen</a>";

                this.viewModel.alertsManager.showWarning(
                    this.viewModel.$("div#tow_data_alerts_container"),
                    "Could not submit the information at this moment. All of the data has been stored in the " +
                    pendingOrdersScreenLink + " for you to try to submit it again later. Error: " + error.message
                );
            })
            .catch(error => {
                console.log(error);
                this.viewModel.alertsManager.showDanger(
                    this.viewModel.$("div#tow_data_alerts_container"),
                    "Could not store the deal locally after failing submission. Make sure your browser is up to date. Error: " + error.message
                );
            });
    }

    loadDealInfo(deal) {
        this.viewModel.customerName(deal.sellerName);
        this.viewModel.customerNamePicture(deal.sellerNamePicture);
        this.viewModel.customerId(deal.sellerId);
        this.viewModel.customerIdPicture(deal.sellerIdPicture);
        this.viewModel.selectedCustomerGender(deal.sellerGender);
        this.viewModel.customerPhone(deal.sellerPhoneNumber);
        this.viewModel.customerStreetAddress(deal.vehicleStreetAddress);
        this.viewModel.customerZip(deal.vehicleZip);
        this.viewModel.customerCity(deal.vehicleCity);

        this.viewModel.selectedVehicleYear(deal.vehicleYear);
        this.viewModel.selectedVehicleMake(deal.vehicleMake.toLowerCase());

        this.viewModel.loadVehicleModels(() => {
            this.viewModel.selectedVehicleModel(deal.vehicleModel.toLowerCase());
        });

        this.viewModel.vehicleColor(deal.vehicleColor);

        this.viewModel.vehicleVin(deal.vehicleVin);
        this.viewModel.vehicleHasTitle(deal.vehicleHasTitle);
        this.viewModel.vehicleTitle(deal.vehicleTitle);

        this.viewModel.selectedVehicleWheelType(deal.vehicleWheelType);
        this.viewModel.selectedVehicleBodyStyle(deal.vehicleDoors);
        this.viewModel.vehiclePurchasePrice(deal.vehiclePurchasePrice);

        this.viewModel.customerDataNotes(deal.notes);
        this.viewModel.vehicleDescriptionNotes(deal.notes);
        this.viewModel.vehicleIdentificationNotes(deal.notes);
        this.viewModel.vehiclePurchaseDetailsNotes(deal.notes);

        this.viewModel.pickUpNotes(deal.pickUpNotes);
    }

    loadTowOrderData(towOrder) {
        this.viewModel.isHighPriorityTow(towOrder.isHighPriorityTow);

        this.viewModel.customerName(towOrder.customerName);
        this.viewModel.customerNamePicture(towOrder.customerNamePicture);
        this.viewModel.customerId(towOrder.customerId);
        this.viewModel.customerIdPicture(towOrder.customerIdPicture);
        this.viewModel.selectedCustomerGender(towOrder.customerGender);
        this.viewModel.customerPhone(towOrder.customerPhone);
        this.viewModel.customerStreetAddress(towOrder.customerStreetAddress);
        this.viewModel.customerZip(towOrder.customerZip);
        this.viewModel.customerCity(towOrder.customerCity);

        this.viewModel.selectedVehicleYear(towOrder.vehicleYear);
        this.viewModel.selectedVehicleMake(towOrder.vehicleMake);

        this.viewModel.loadVehicleModels(() => {
            this.viewModel.selectedVehicleModel(towOrder.vehicleModel);
        });

        this.viewModel.vehicleColor(towOrder.vehicleColor);
        this.viewModel.vehiclePictures(towOrder.vehiclePictures);

        this.viewModel.vehicleVin(towOrder.vehicleVin);
        this.viewModel.vehicleHasTitle(towOrder.vehicleHasTitle);
        this.viewModel.vehicleTitle(towOrder.vehicleTitle);

        this.viewModel.selectedVehicleWheelType(towOrder.vehicleWheelType);
        this.viewModel.selectedVehicleBodyStyle(towOrder.vehicleBodyStyle);
        this.viewModel.vehiclePurchasePrice(towOrder.vehiclePurchasePrice);

        this.viewModel.customerDataNotes(towOrder.customerDataNotes);
        this.viewModel.vehicleDescriptionNotes(towOrder.vehicleDescriptionNotes);
        this.viewModel.vehicleIdentificationNotes(towOrder.vehicleIdentificationNotes);
        this.viewModel.vehiclePurchaseDetailsNotes(towOrder.vehiclePurchaseDetailsNotes);

        this.viewModel.pickUpNotes(towOrder.pickUpNotes);
    }

}
