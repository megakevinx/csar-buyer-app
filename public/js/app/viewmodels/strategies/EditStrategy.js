// eslint-disable-next-line no-unused-vars
import EnterNewDealViewModel from "../EnterNewDealViewModel";
import BaseStrategy from "./BaseStrategy";

/**
 * Strategy
 */
export default class EditStrategy extends BaseStrategy {
    /**
     * @param {EnterNewDealViewModel} viewModel 
     * @param {string} dealNumber
     */
    constructor(viewModel, dealNumber) {
        super(viewModel);
        this.dealNumber = dealNumber;
    }

    initViewModel() {
        this.viewModel.isEditing(true);
        this.viewModel.isLoadingRequest(true);

        this.viewModel.genericApiClient.getDeal(this.dealNumber)
            .then(response => {
                this.viewModel.isLoadingRequest(false);

                let deal = response.deal;
                let tows = response.tows;

                if (deal) {
                    this.viewModel.isEditing(true);
                    this.viewModel.editingDealNumber(deal.id);

                    if (tows) {

                        //let mostRecentTowOrder = tows[0];

                        //this.viewModel.selectedTowCompanyId(mostRecentTowOrder.towCompanyId);
                        //this.viewModel.towCompanyOnSelect();
                        //this.viewModel.selectedTowDriverId(mostRecentTowOrder.buyerId);
                        //this.viewModel.selectedTowFromLocation(mostRecentTowOrder.towFromLocation);
                        //this.viewModel.selectedTowToLocation(mostRecentTowOrder.towToLocation);
                        //this.viewModel.isHighPriorityTow(mostRecentTowOrder.isHighPriorityTow == 0 ? "no" : "yes" );

                        this.viewModel.pastTowOrders(tows);
                    }

                    this.loadDealInfo(deal);
                }
            })
            .catch(error => {
                this.viewModel.isEditing(false);
                this.viewModel.isLoadingRequest(false);
                console.log(error);
                this.viewModel.alertsManager.showDanger(
                    this.viewModel.$("div#tow_data_alerts_container"),
                    `Could not load Deal #${ this.dealNumber }. ` +
                    "Please make sure you're connected to the internet, try again later or contact your system's administrator."
                );
            });
    }

    notifyDealSubmitted() {
        this.viewModel.alertsManager.showSuccess(
            this.viewModel.$("div#tow_data_alerts_container"),
            "The deal has been modified and a new tow was ordered. " +
            `Deal Number: <b>${ this.viewModel.dealReferenceNumber() }</b>. Tow Number: <b>${ this.viewModel.towNumber() }</b>.`
        );

        this.initViewModel();
    }

    getSubmitTowOrderApiCall() {
        return towOrderData => this.viewModel.genericApiClient.updateTowOrder(towOrderData);
    }

    prepareTowOrderData(towOrderData) {
        towOrderData.dealNumber = this.viewModel.editingDealNumber();
        return towOrderData;
    }

    notifyDealSubmissionFailed(towOrderData, error) {
        this.viewModel.alertsManager.showDanger(
            this.viewModel.$("div#tow_data_alerts_container"),
            "There has been an error. Please make sure that you have good internet connection, try again later or contact your system's administrator. Error: " + error.message
        );
    }
}
