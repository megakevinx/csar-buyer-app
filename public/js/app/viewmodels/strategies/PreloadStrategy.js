// eslint-disable-next-line no-unused-vars
import EnterNewDealViewModel from "../EnterNewDealViewModel";
import BaseStrategy from "./BaseStrategy";

/**
 * Strategy
 */
export default class PreloadStrategy extends BaseStrategy {
    /**
     * @param {EnterNewDealViewModel} viewModel 
     * @param {string} localTowOrderId
     */
    constructor(viewModel, localTowOrderId) {
        super(viewModel);
        this.localTowOrderId = localTowOrderId;
    }

    initViewModel() {
        this.viewModel.localDataRepository.getTowOrder(parseInt(this.localTowOrderId))
            .then((towOrder) => {
                if (towOrder) {
                    this.viewModel.isPreloaded(true);
                    this.viewModel.localTowOrderId(towOrder.id);

                    this.loadTowOrderData(towOrder);
                }
            })
            .catch(error => {
                console.log(error);
                this.viewModel.alertsManager.showDanger(
                    this.viewModel.$("div#tow_data_alerts_container"),
                    "Could not load the pending deal. Please make sure your browser is up to date and try again. Error: " + error.message
                );
            });
    }

    notifyDealSubmitted() {
        this.viewModel.localDataRepository.removeTowOrder(this.viewModel.localTowOrderId());

        this.viewModel.alertsManager.showSuccess(
            this.viewModel.$("div#tow_data_alerts_container"),
            "A new deal has been created and the tow was ordered. " +
            `Deal Number: <b>${ this.viewModel.dealReferenceNumber() }</b>. Tow Number: <b>${ this.viewModel.towNumber() }</b>.`
        );
    }

    notifyTowOrderStoredLocally() {
        this.viewModel.localDataRepository.removeTowOrder(this.viewModel.localTowOrderId());
    }
}
