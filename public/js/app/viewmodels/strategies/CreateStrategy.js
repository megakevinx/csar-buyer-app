// eslint-disable-next-line no-unused-vars
import EnterNewDealViewModel from "../EnterNewDealViewModel";
import BaseStrategy from "./BaseStrategy";

/**
 * Strategy
 */
export default class CreateStrategy extends BaseStrategy {
    /**
     * @param {EnterNewDealViewModel} viewModel
     */
    constructor(viewModel) {
        super(viewModel);
    }

    initViewModel() {
        this.viewModel.workCacheRepository.getWorkCache().then((towOrder) => {
            if (towOrder) {
                this.loadTowOrderData(towOrder);

                this.viewModel.alertsManager.showSuccess(
                    this.viewModel.$("div#tow_data_alerts_container"),
                    "Some fields have been filled with data from your previous session. Take a look in the various forms."
                );
            }
        });
    }

    notifyInputDataFormCompleted() {
        let towOrderDataToCache = this.viewModel.getTowOrderDataToPersist();
        this.viewModel.workCacheRepository.saveWorkCache(towOrderDataToCache).then(() => {
            console.log("Saved state to local work cache");
        });
    }

    notifyDealSubmitted() {
        this.viewModel.workCacheRepository.clearWorkCache().then(() => {
            console.log("Cleared local work cache");
        });

        this.viewModel.alertsManager.showSuccess(
            this.viewModel.$("div#tow_data_alerts_container"),
            "A new deal has been created and the tow was ordered. " +
            `Deal Number: <b>${ this.viewModel.dealReferenceNumber() }</b>. Tow Number: <b>${ this.viewModel.towNumber() }</b>.`
        );
    }

    notifyDealSubmissionFailed(towOrderData, error) {
        super.notifyDealSubmissionFailed(towOrderData, error);
        this.viewModel.workCacheRepository.clearWorkCache().then(() => {
            console.log("Cleared local work cache");
        });
    }

    notifyEnteringNewDeal() {
        return this.viewModel.workCacheRepository.clearWorkCache().then(() => {
            console.log("Cleared local work cache");
        });
    }
}
