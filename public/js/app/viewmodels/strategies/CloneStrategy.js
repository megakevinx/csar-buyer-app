// eslint-disable-next-line no-unused-vars
import EnterNewDealViewModel from "../EnterNewDealViewModel";
import BaseStrategy from "./BaseStrategy";

/**
 * Strategy
 */
export default class CloneStrategy extends BaseStrategy {
    /**
     * @param {EnterNewDealViewModel} viewModel 
     * @param {string} dealNumber
     */
    constructor(viewModel, dealNumber) {
        super(viewModel);
        this.dealNumber = dealNumber;
    }

    initViewModel() {
        this.viewModel.isLoadingRequest(true);

        this.viewModel.genericApiClient.getDeal(this.dealNumber)
            .then(response => {
                this.viewModel.isLoadingRequest(false);

                let deal = response.deal;

                if (deal) {
                    this.viewModel.isCloned(true);
                    this.viewModel.clonedDealReferenceNumber(deal.id);

                    this.loadDealInfo(deal);
                }
            })
            .catch(error => {
                this.viewModel.isLoadingRequest(false);
                console.log(error);
                this.viewModel.alertsManager.showDanger(
                    this.viewModel.$("div#tow_data_alerts_container"),
                    `Could not load Deal #${ this.dealNumber }. ` +
                    "Please make sure you're connected to the internet, try again later or contact your system's administrator. Error: " + error
                );
            });
    }

    notifyDealSubmitted() {
        this.viewModel.alertsManager.showSuccess(
            this.viewModel.$("div#tow_data_alerts_container"),
            "A new deal has been created and the tow was ordered. " +
            `Deal Number: <b>${ this.viewModel.dealReferenceNumber() }</b>. Tow Number: <b>${ this.viewModel.towNumber() }</b>.`
        );
    }
}
