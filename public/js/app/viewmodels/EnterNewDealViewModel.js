// @ts-check

/**
 * Main ViewModel for the Enter new deal screen.
 */

import ko from "knockout";

import PreloadStrategy from "./strategies/PreloadStrategy";
import CloneStrategy from "./strategies/CloneStrategy";
import EditStrategy from "./strategies/EditStrategy";
import CreateStrategy from "./strategies/CreateStrategy";

// import TowDataValidator from "../validators/TowDataValidator";
// import CustomerDataValidator from "../validators/CustomerDataValidator";
// import VehicleDescriptionValidator from "../validators/VehicleDescriptionValidator";
// import VehicleIdentificationValidator from "../validators/VehicleIdentificationValidator";
// import VehiclePurchaseDetailsValidator from "../validators/VehiclePurchaseDetailsValidator";

// import AlertsManager from "../alerts/AlertsManager";
// import SketchpadHelper from "../helpers/SketchpadHelper";
// import RoutingEngine from "../routing/RoutingEngine";
// import GenericApiClient from "../apiclients/GenericApiClient";
// import UrlHelper from "../helpers/UrlHelper";
// import ImageProcessor from "../helpers/ImageProcessor";
// import PromisedFileReader from "../helpers/PromisedFileReader";
// import LocalDataRepository from "../repositories/LocalDataRepository";
// import WorkCacheRepository from "../repositories/WorkCacheRepository";
// import DomHelper from "../helpers/DomHelper";

/** This is the main view model for the Enter New Deal screen. */
export default class EnterNewDealViewModel {

    /**
     * param {Array} vehicleYears
     * param {Array} vehicleMakes
     * param {Array} vehicleWheelTypes
     * param {Array} vehicleBodyStyles
     * param {TowDataValidator} towDataValidator
     * param {CustomerDataValidator} customerDataValidator
     * param {VehicleDescriptionValidator} vehicleDescriptionValidator
     * param {VehicleIdentificationValidator} vehicleIdentificationValidator
     * param {VehiclePurchaseDetailsValidator} vehiclePurchaseDetailsValidator
     * param {AlertsManager} alertsManager
     * param {SketchpadHelper} customerSignatureSketchpad
     * param {RoutingEngine} routingEngine
     * param {GenericApiClient} genericApiClient
     * param {UrlHelper} urlHelper
     * param {ImageProcessor} imageProcessor
     * param {PromisedFileReader} fileReader
     * param {LocalDataRepository} localDataRepository
     * param {WorkCacheRepository} workCacheRepository
     * param {DomHelper} domHelper
     * param {jQuery} $
     */
    constructor(
        vehicleYears, vehicleMakes, vehicleWheelTypes, vehicleBodyStyles,
        towDataValidator, customerDataValidator, vehicleDescriptionValidator, vehicleIdentificationValidator, vehiclePurchaseDetailsValidator,
        alertsManager,
        customerSignatureSketchpad,
        routingEngine,
        genericApiClient,
        urlHelper,
        imageProcessor,
        fileReader,
        localDataRepository, workCacheRepository,
        domHelper,
        $
    ) {
        // Configuration
        this.screens = {
            home: "home",
            customerData: "customer-data",
            vehicleInformation: "vehicle-information",
            pickupNotes: "pickup-notes",

            vehicleDescription: "vehicle-description",
            vehicleIdentification: "vehicle-identification",
            vehiclePurchaseDetails: "vehicle-purchase-details",

            captureSignature: "capture-signature"
        };

        this.errorAlertsIds = {
            customerData: "CUSTOMER_DATA_INVALID_ALERT",
            vehicleDescription: "VEHICLE_DESCRIPTION_INVALID_ALERT",
            vehicleIdentification: "VEHICLE_IDENTIFICATION_INVALID_ALERT",
            vehiclePurchaseDetails: "VEHICLE_PURCHASE_DETAILS_INVALID_ALERT",

            zipNotSupported: "ZIP_NOT_SUPPORTED_ALERT",
            invalidInputData: "INPUT_DATA_INVALID_ALERT",

            cityNotFound: "CITY_NOT_FOUND_ALERT",
            unableToLoadModels: "UNABLE_TO_LOAD_VEHICLE_MODELS"
        };

        this.alertsManager = alertsManager;

        this.towDataValidator = towDataValidator;
        this.customerDataValidator = customerDataValidator;
        this.vehicleDescriptionValidator = vehicleDescriptionValidator;
        this.vehicleIdentificationValidator = vehicleIdentificationValidator;
        this.vehiclePurchaseDetailsValidator = vehiclePurchaseDetailsValidator;

        this.isCustomerDataValid = ko.observable(false);
        this.isVehicleDescriptionValid = ko.observable(false);
        this.isVehicleIdentificationValid = ko.observable(false);
        this.isVehiclePurchaseDetailsValid = ko.observable(false);

        this.isVehicleInformationValid = ko.pureComputed(() => {
            return this.isVehicleDescriptionValid() &&
                this.isVehicleIdentificationValid() &&
                this.isVehiclePurchaseDetailsValid();
        });

        this.genericApiClient = genericApiClient;
        this.localDataRepository = localDataRepository;
        this.workCacheRepository = workCacheRepository;
        this.urlHelper = urlHelper;
        this.imageProcessor = imageProcessor;
        this.fileReader = fileReader;
        this.customerSignatureSketchpad = customerSignatureSketchpad;
        this.domHelper = domHelper;
        this.$ = $;

        this.currentScreen = ko.observable(this.screens.home);

        this.isLoadingRequest = ko.observable(false);
        this.manualVehicleModelEntryEnabled = ko.observable(false);
        this.dealSubmitted = ko.observable(false);
        this.towOrderStoredLocally = ko.observable(false);

        this.isPreloaded = ko.observable(false);
        this.localTowOrderId = ko.observable();

        this.isCloned = ko.observable(false);
        this.clonedDealReferenceNumber = ko.observable();

        this.isEditing = ko.observable(false);
        this.editingDealNumber = ko.observable();

        this.genders = ["Male", "Female"];

        this.vehicleYears = vehicleYears;
        this.vehicleMakes = vehicleMakes;
        this.vehicleModels = ko.observableArray();

        this.vehicleWheelTypes = vehicleWheelTypes;
        this.vehicleBodyStyles = vehicleBodyStyles;

        // Properties
        this.dealReferenceNumber = ko.observable();
        this.towNumber = ko.observable();

        this.isHighPriorityTow = ko.observable("no");

        this.pastTowOrders = ko.observableArray();

        this.customerName = ko.observable();
        this.customerNamePicture = ko.observable();
        this.customerId = ko.observable();
        this.customerIdPicture = ko.observable();
        this.selectedCustomerGender = ko.observable();
        this.customerPhone = ko.observable();
        this.customerStreetAddress = ko.observable();
        this.customerZip = ko.observable();
        this.customerCity = ko.observable();

        this.selectedVehicleYear = ko.observable();
        this.selectedVehicleMake = ko.observable();
        this.selectedVehicleModel = ko.observable();
        this.vehicleColor = ko.observable();
        this.vehiclePictures = ko.observableArray();

        this.vehicleVin = ko.observable();
        this.vehicleHasTitle = ko.observable();
        this.vehicleTitle = ko.observable();

        this.selectedVehicleWheelType = ko.observable();
        this.selectedVehicleBodyStyle = ko.observable();
        this.vehiclePurchasePrice = ko.observable();

        this.customerDataNotes = ko.observable();
        this.vehicleDescriptionNotes = ko.observable();
        this.vehicleIdentificationNotes = ko.observable();
        this.vehiclePurchaseDetailsNotes = ko.observable();

        this.pickUpNotes = ko.observable();

        // Start app
        this.strategy = new CreateStrategy(this);
        this.loadStrategy();
        this.strategy.initViewModel();

        routingEngine
            .setup(this.getRouteConfig(this, this.screens))
            .run("#home");
    }

    loadStrategy() {
        let preload = this.urlHelper.getQueryStringParam("preload");
        let clone = this.urlHelper.getQueryStringParam("clone");
        let edit = this.urlHelper.getQueryStringParam("edit");

        let localTowOrderOrDealId = this.urlHelper.getQueryStringParam("id");

        if (localTowOrderOrDealId) {
            if (preload === "true") {
                this.strategy = new PreloadStrategy(this, localTowOrderOrDealId);
            }
            else if (clone === "true") {
                this.strategy = new CloneStrategy(this, localTowOrderOrDealId);
            }
            else if (edit === "true") {
                this.strategy = new EditStrategy(this, localTowOrderOrDealId);
            }
        }
    }

    getRouteConfig(viewModel, screens) {

        return function() {
            // here, "this" is Sammy

            this.get("#home", () => {
                viewModel.currentScreen(screens.home);
            });

            this.get("#customer-data", () => {
                viewModel.currentScreen(screens.customerData);
            });

            this.get("#vehicle-information", () => {
                viewModel.currentScreen(screens.vehicleInformation);
            });

            this.get("#pickup-notes", () => {
                viewModel.currentScreen(screens.pickupNotes);
            });

            this.get("#vehicle-description", () => {
                viewModel.currentScreen(screens.vehicleDescription);
            });

            this.get("#vehicle-identification", () => {
                viewModel.currentScreen(screens.vehicleIdentification);
            });

            this.get("#vehicle-purchase-details", () => {
                viewModel.currentScreen(screens.vehiclePurchaseDetails);
            });

            this.get("#capture-signature", () => {
                viewModel.currentScreen(screens.captureSignature);
                viewModel.customerSignatureSketchpad.reset();
            });

            // this.get("", () => {
            //     console.log("Entered the empty route");
            // });
        };
    }

    navigateTo(screenId) {
        this.urlHelper.setWindowLocationHash(screenId);
    }

    shouldDisplayScreen(screenId) {
        return this.currentScreen() == screenId;
    }

    allDataIsValid(validateTowData = true) {

        let everythingIsValid = validateTowData ? this.towDataValidator.isValid() : true;

        if (!this.customerDataValidator.isValid()) {
            this.isCustomerDataValid(false);
            this.alertsManager.showDanger(
                this.$("div#tow_data_alerts_container"),
                `There's some invalid data in the <a href='#${ this.screens.customerData }'>Customer Data screen</a>`,
                this.errorAlertsIds.customerData
            );
            everythingIsValid = false;
        }
        else {
            this.isCustomerDataValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.customerData);
        }

        if (!this.vehicleDescriptionValidator.isValid()) {
            this.isVehicleDescriptionValid(false);
            this.alertsManager.showDanger(
                this.$("div#tow_data_alerts_container"),
                `There's some invalid data in the <a href='#${ this.screens.vehicleDescription }'>Vehicle Description screen</a>`,
                this.errorAlertsIds.vehicleDescription
            );
            everythingIsValid = false;
        }
        else {
            this.isVehicleDescriptionValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.vehicleDescription);
        }

        if (!this.vehicleIdentificationValidator.isValid()) {
            this.isVehicleIdentificationValid(false);
            this.alertsManager.showDanger(
                this.$("div#tow_data_alerts_container"),
                `There's some invalid data in the <a href='#${ this.screens.vehicleIdentification }'>Vehicle Identification screen</a>`,
                this.errorAlertsIds.vehicleIdentification
            );
            everythingIsValid = false;
        }
        else {
            this.isVehicleIdentificationValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.vehicleIdentification);
        }

        if (!this.vehiclePurchaseDetailsValidator.isValid()) {
            this.isVehiclePurchaseDetailsValid(false);
            this.alertsManager.showDanger(
                this.$("div#tow_data_alerts_container"),
                `There's some invalid data in the <a href='#${ this.screens.vehiclePurchaseDetails }'>Vehicle Purchase Details screen</a>`,
                this.errorAlertsIds.vehiclePurchaseDetails
            );
            everythingIsValid = false;
        }
        else {
            this.isVehiclePurchaseDetailsValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.vehiclePurchaseDetails);
        }

        return everythingIsValid;
    }

    getTowOrderDataToPersist() {
        let towOrderData = {
            isHighPriorityTow: this.isHighPriorityTow(),

            customerName: this.customerName(),
            customerNamePicture: this.customerNamePicture(),
            customerId: this.customerId(),
            customerIdPicture: this.customerIdPicture(),
            customerGender: this.selectedCustomerGender(),
            customerPhone: this.customerPhone(),
            customerStreetAddress: this.customerStreetAddress(),
            customerZip: this.customerZip(),
            customerCity: this.customerCity(),

            vehicleYear: this.selectedVehicleYear(),
            vehicleMake: this.selectedVehicleMake(),
            vehicleModel: this.selectedVehicleModel(),
            vehicleColor: this.vehicleColor(),
            vehiclePictures: this.vehiclePictures(),

            vehicleVin: this.vehicleVin(),
            vehicleHasTitle: this.vehicleHasTitle(),
            vehicleTitle: this.vehicleTitle(),

            vehicleWheelType: this.selectedVehicleWheelType(),
            vehicleBodyStyle: this.selectedVehicleBodyStyle(),
            vehiclePurchasePrice: this.vehiclePurchasePrice(),

            customerDataNotes: this.customerDataNotes(),
            vehicleDescriptionNotes: this.vehicleDescriptionNotes(),
            vehicleIdentificationNotes: this.vehicleIdentificationNotes(),
            vehiclePurchaseDetailsNotes: this.vehiclePurchaseDetailsNotes(),

            pickUpNotes: this.pickUpNotes(),
        };

        return towOrderData;
    }

    submitData(validateTowData, getApiCall, onDealSubmitSuccess) {

        if (!this.allDataIsValid(validateTowData)) {
            return;
        }

        let towOrderData = this.getTowOrderDataToPersist();

        let apiCall = getApiCall();
        towOrderData = this.strategy.prepareTowOrderData(towOrderData);

        this.isLoadingRequest(true);

        console.log("POST/PUT Request sent to api/tow-order or api/Deal");

        apiCall(towOrderData).then(response => {

            this.isLoadingRequest(false);
            console.log(response);

            if (response.status == 200) {
                this.dealReferenceNumber(response.dealReferenceNumber);
                this.towNumber(response.towNumber);
                this.dealSubmitted(true);

                this.alertsManager.dismiss(this.errorAlertsIds.zipNotSupported);
                this.alertsManager.dismiss(this.errorAlertsIds.invalidInputData);

                onDealSubmitSuccess();
            }
            else if (response.status == 400) {
                if (response.errorCode == "ZIP_NOT_SUPPORTED") {

                    let customerDataScreenLink = "<a href='#customer-data'>Customer Data screen</a>";

                    this.alertsManager.showDanger(
                        this.$("div#tow_data_alerts_container"),
                        "We don't offer our services in that area, please choose a diferent Zip code in the " + customerDataScreenLink,
                        this.errorAlertsIds.zipNotSupported
                    );
                }
                else if (response.errorCode == "INPUT_DATA_INVALID" || response.errorCode == "NOT_FOUND") {
                    this.alertsManager.showDanger(
                        this.$("div#tow_data_alerts_container"),
                        response.errorMessage,
                        this.errorAlertsIds.invalidInputData
                    );
                }
                else {
                    this.alertsManager.showDanger(
                        this.$("div#tow_data_alerts_container"),
                        "There has been a server side error. Please try again later or contact your system's administrator. Error: " + JSON.stringify(response)
                    );
                }
            }
            else if (response.status == 401) {
                this.alertsManager.showDanger(
                    this.$("div#tow_data_alerts_container"),
                    "Your session has expired. Please try logging back in again."
                );
            }
            else {
                this.alertsManager.showDanger(
                    this.$("div#tow_data_alerts_container"),
                    "There has been a server side error. Please try again later or contact your system's administrator. Error: " + JSON.stringify(response)
                );
            }
        }).catch(error => {
            this.isLoadingRequest(false);
            console.log(error);
            this.strategy.notifyDealSubmissionFailed(towOrderData, error);
        });
    }

    orderTowOnClick() {
        this.submitData(
            true,
            () => { return this.strategy.getSubmitTowOrderApiCall(); },
            () => { this.strategy.notifyDealSubmitted(); }
        );
    }

    saveChangesOnClick() {

        this.navigateTo(this.screens.home);

        this.submitData(
            false,
            () => { return towOrderData => this.genericApiClient.updateDeal(towOrderData); },
            () => {
                this.alertsManager.showSuccess(
                    this.$("div#tow_data_alerts_container"),
                    `The deal has been modified. Deal Number: <b>${ this.dealReferenceNumber() }</b>.`
                );
            }
        );
    }

    enterNewDealOnClick() {
        if (confirm("Are you sure you want to enter a new deal? You will lose all unsaved progress on the current deal.")) {
            this.strategy.notifyEnteringNewDeal().then(() => {
                this.urlHelper.reloadPage();
            });
        }
    }

    findCityOnClick() {

        let alertCityNotFound = (message) => {
            this.alertsManager.showWarning(
                this.$("div#city_alerts_container"),
                "Could not find a city for the given Zip code. " + (message || ""),
                this.errorAlertsIds.cityNotFound
            );
        };

        if (this.customerZip()) {

            this.isLoadingRequest(true);

            console.log("GET Request sent to google geolocation");

            this.genericApiClient.getLocation(this.customerZip())
                .then(response => {
                    console.log(response);
                    this.isLoadingRequest(false);

                    if (response.status == "OK") {
                        try {
                            let city = response.results[0].address_components[1].long_name;
                            this.customerCity(city);
                            this.alertsManager.dismiss(this.errorAlertsIds.cityNotFound);
                        }
                        catch (error) {
                            alertCityNotFound();
                            console.log(error);
                        }
                    }
                    else {
                        alertCityNotFound();
                        console.log(response);
                    }
                })
                .catch(error => {
                    this.isLoadingRequest(false);
                    alertCityNotFound("Make sure that you're connected to the internet");
                    console.log(error);
                });
        }
        else {
            this.alertsManager.showWarning(
                this.$("div#city_alerts_container"),
                "Please enter a Zip code to try to find a city for it",
                this.errorAlertsIds.cityNotFound
            );
        }
    }

    captureSignatureClearOnClick() {
        this.customerSignatureSketchpad.clear();
    }

    captureSignatureDoneOnClick() {
        let pictureData = this.customerSignatureSketchpad.getAsDataUrl();
        this.customerNamePicture(pictureData);
        this.navigateTo(this.screens.customerData);
    }

    customerIdPictureOnChange(data, e) {
        this.handlePictureInputOnChange(e, this.customerIdPicture);
    }

    vehiclePictureOnChange(data, e) {
        this.handlePictureInputOnChange(e, (img) => {
            this.vehiclePictures.push(img);
        });
    }

    handlePictureInputOnChange(e, setter) {
        let file = e.target.files[0];
        if (file) {
            this.fileReader.readAsDataURL(file)
                .then(imgDataUrl => this.imageProcessor.shrinkImage(imgDataUrl))
                .then(smallerImg => setter(smallerImg));
        }
    }

    clearVehiclePhotos() {
        this.vehiclePictures.removeAll();
    }

    customerDataDoneOnClick() {
        if (this.customerDataValidator.isValid()) {
            this.isCustomerDataValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.customerData);
            this.strategy.notifyInputDataFormCompleted();
            this.navigateTo(this.screens.home);
        }
        else {
            this.isCustomerDataValid(false);
        }
    }

    vehicleInformationDoneOnClick() {
        this.navigateTo(this.screens.home);
    }

    vehicleDescriptionDoneOnClick() {
        if (this.vehicleDescriptionValidator.isValid()) {
            this.isVehicleDescriptionValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.vehicleDescription);
            this.strategy.notifyInputDataFormCompleted();
            this.navigateTo(this.screens.vehicleInformation);
        }
        else {
            this.isVehicleDescriptionValid(false);
        }
    }

    vehicleIdentificationDoneOnClick() {
        if (this.vehicleIdentificationValidator.isValid()) {
            this.isVehicleIdentificationValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.vehicleIdentification);
            this.strategy.notifyInputDataFormCompleted();
            this.navigateTo(this.screens.vehicleInformation);
        }
        else {
            this.isVehicleIdentificationValid(false);
        }
    }

    vehiclePurchaseDetailsDoneOnClick() {
        if (this.vehiclePurchaseDetailsValidator.isValid()) {
            this.isVehiclePurchaseDetailsValid(true);
            this.alertsManager.dismiss(this.errorAlertsIds.vehiclePurchaseDetails);
            this.strategy.notifyInputDataFormCompleted();
            this.navigateTo(this.screens.vehicleInformation);
        }
        else {
            this.isVehiclePurchaseDetailsValid(false);
        }
    }

    pickUpNotesDoneOnClick() {
        this.strategy.notifyInputDataFormCompleted();
        this.navigateTo(this.screens.home);
    }

    // towCompanyOnSelect() {
    //     if (this.selectedTowCompanyId()) {
    //         this.towDrivers(
    //             this.allTowDrivers.filter(d => d.towCompanyId == this.selectedTowCompanyId())
    //         );
    //     }
    // }

    vehicleYearOnSelect() {
        this.loadVehicleModels();
    }

    vehicleMakeOnSelect() {
        this.loadVehicleModels();
    }

    loadVehicleModels(callbackAfterLoad) {
        if (this.selectedVehicleYear() && this.selectedVehicleMake()) {

            console.log("GET Request sent to api/vehicle/models");

            this.isLoadingRequest(true);

            this.domHelper.setTimeout(() => {
                if (this.isLoadingRequest()) {
                    this.isLoadingRequest(false);
                    this.manualVehicleModelEntryEnabled(true);
                }
            }, 2000);

            //setTimeout(() => {
            this.genericApiClient.getVehicleModels(
                this.selectedVehicleMake(),
                this.selectedVehicleYear()
            ).then(response => {
                this.alertsManager.dismiss(this.errorAlertsIds.unableToLoadModels);
                this.isLoadingRequest(false);
                this.manualVehicleModelEntryEnabled(false);
                this.vehicleModels(response.models);

                if (callbackAfterLoad) {
                    callbackAfterLoad();
                }
            }).catch(error => {
                console.log(error);

                this.isLoadingRequest(false);
                this.manualVehicleModelEntryEnabled(true);

                this.alertsManager.showWarning(
                    this.$("div#vehicle_description_alerts_container"),
                    "Could not load vehicle models. Please enter one manually.",
                    this.errorAlertsIds.unableToLoadModels
                );

                if (callbackAfterLoad) {
                    callbackAfterLoad();
                }
            });
            //}, 5000);
        }
    }
}
