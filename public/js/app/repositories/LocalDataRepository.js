export default class LocalDataRepository {

    constructor(indexedDbPromised) {
        this.LOCAL_DATABASE_NAME = "csar-buy-app-db";
        this.LOCAL_DATABASE_VERSION = 1;

        this.idb = indexedDbPromised;

        if ("indexedDB" in window) {
            this.idb.open(this.LOCAL_DATABASE_NAME, this.LOCAL_DATABASE_VERSION, function(upgradeDb) {
                if (!upgradeDb.objectStoreNames.contains("towOrders")) {
                    upgradeDb.createObjectStore("towOrders", {keyPath: "id", autoIncrement: true});
                }
            });
        }
    }

    saveTowOrder(towOrderData) {
        return this.idb.open(this.LOCAL_DATABASE_NAME, this.LOCAL_DATABASE_VERSION).then(db => {
            let tran = db.transaction("towOrders", "readwrite");
            let store = tran.objectStore("towOrders");

            store.add(towOrderData);

            return tran.complete;
        });
    }

    getAllTowOrders() {
        return this.idb.open(this.LOCAL_DATABASE_NAME, this.LOCAL_DATABASE_VERSION).then(db => {
            var tx = db.transaction("towOrders", "readonly");
            var store = tx.objectStore("towOrders");

            return store.getAll();
        });
    }

    removeTowOrder(rowKey) {
        return this.idb.open(this.LOCAL_DATABASE_NAME, this.LOCAL_DATABASE_VERSION).then(function(db) {
            var tx = db.transaction("towOrders", "readwrite");
            var store = tx.objectStore("towOrders");

            store.delete(rowKey);

            return tx.complete;
        });
    }

    getTowOrder(rowKey) {
        return this.idb.open(this.LOCAL_DATABASE_NAME, this.LOCAL_DATABASE_VERSION).then(function(db) {
            var tx = db.transaction("towOrders", "readonly");
            var store = tx.objectStore("towOrders");

            return store.get(rowKey);
        });
    }
}