export default class WorkCacheRepository {

    constructor(indexedDbPromised) {
        this.LOCAL_WORK_CACHE_NAME = "csar-buy-app-work-cache";
        this.LOCAL_DATABASE_VERSION = 1;

        this.idb = indexedDbPromised;

        if ("indexedDB" in window) {
            this.idb.open(this.LOCAL_WORK_CACHE_NAME, this.LOCAL_DATABASE_VERSION, function(upgradeDb) {
                if (!upgradeDb.objectStoreNames.contains("towOrderWorkCache")) {
                    upgradeDb.createObjectStore("towOrderWorkCache", {keyPath: "id", autoIncrement: true});
                }
            });
        }
    }

    saveWorkCache(towOrderData) {
        return this.idb.open(this.LOCAL_WORK_CACHE_NAME, this.LOCAL_DATABASE_VERSION).then(db => {
            let tran = db.transaction("towOrderWorkCache", "readwrite");
            let store = tran.objectStore("towOrderWorkCache");

            return store.getAll().then(rows => {
                rows.forEach(r => {
                    store.delete(r.id);
                });
            }).then(() => {
                store.add(towOrderData);
                return tran.complete;
            });
        });
    }

    getWorkCache() {
        return this.idb.open(this.LOCAL_WORK_CACHE_NAME, this.LOCAL_DATABASE_VERSION).then(db => {
            var tx = db.transaction("towOrderWorkCache", "readonly");
            var store = tx.objectStore("towOrderWorkCache");

            return store.getAll().then(rows => {
                if (rows.length != 0) {
                    return rows[0];
                }
                else {
                    return null;
                }
            });
        });
    }

    clearWorkCache() {
        return this.idb.open(this.LOCAL_WORK_CACHE_NAME, this.LOCAL_DATABASE_VERSION).then(db => {
            let tran = db.transaction("towOrderWorkCache", "readwrite");
            let store = tran.objectStore("towOrderWorkCache");

            store.clear();

            return tran.complete;
        });
    }
}