//import { describe, it } from "mocha";
import assert from "assert";
import sinon from "sinon";

import EnterNewDealViewModel from "../../app/viewmodels/EnterNewDealViewModel";

let mockTowCompanies = [
    { "id": 1, "name": "tow company 1" },
    { "id": 2, "name": "tow company 2" }
];
let mockTowDrivers = [
    { "id": 1, "name": "driver 1", "towCompanyId": 1, "towCompanyName": "tow company 1" },
    { "id": 2, "name": "driver 2", "towCompanyId": 2, "towCompanyName": "tow company 2" }
];

let mockTowFromLocations = [ "origin 1", "origin 2" ];
let mockTowToLocations = [ "destination 1", "destination 2" ];
let mockVehicleYears = [ "1990", "1991", "1992", "1993", "1994" ];
let mockVehicleMakes = [ "acura", "honda", "toyota", "ford", "subaru" ];
let mockVehicleWheelTypes = [ "aluminum", "steel" ];
let mockVehicleBodyStyles = [ "2 door", "4 door" ];

let mockAlertsManager = {
    showSuccess: sinon.spy(),
    showWarning: sinon.spy(),
    showDanger: sinon.spy(),
    dismiss: sinon.spy()
};

let mockTowDataValidator = {
    isValid: sinon.spy()
};
let mockCustomerDataValidator = {
    isValid: sinon.spy()
};
let mockVehicleDescriptionValidator = {
    isValid: sinon.spy()
};
let mockVehicleIdentificationValidator = {
    isValid: sinon.spy()
};
let mockVehiclePurchaseDetailsValidator = {
    isValid: sinon.spy()
};

let mockCustomerSignatureSketchpad = {
    baseFill: sinon.spy(),
    clear: sinon.spy(),
    getAsDataUrl: sinon.spy()
};

let mockRoutingEngine = {
    setup: sinon.stub().returns({
        run: sinon.spy()
    })
};

let mockGenericApiClient = {
    createTowOrder: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    getLocation: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    getVehicleModels: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    updateDeal: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    })
};

let mockUrlHelper = {
    setWindowLocationHash: sinon.spy(),
    getQueryStringParam: sinon.spy(),
    reloadPage: sinon.spy()
};

let mockImageProcessor = {
    shrinkImage: sinon.spy()
};

let mockFileReader = {
    readAsDataURL: sinon.spy()
};

let mockLocalDataRepository = {
    saveTowOrder: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    getAllTowOrders: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    removeTowOrder: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    getTowOrder: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    })
};

let mockWorkCacheRepository = {
    saveWorkCache: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    getWorkCache: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    }),
    clearWorkCache: sinon.stub().returns({
        then: sinon.stub().returns({
            catch: sinon.spy()
        })
    })
};

let mockDomHelper = {
    setTimeout: sinon.spy()
};

let mockDomSelector = sinon.spy();

describe("EnterNewDealViewModel", function() {
    describe("#constructor()", function() {
        it("should preload data when query string params are set", function() {
            // Arrange
            let mockUrlHelper = {
                getQueryStringParam: sinon.stub()
            };

            mockUrlHelper.getQueryStringParam.withArgs("preload").returns("true");
            mockUrlHelper.getQueryStringParam.withArgs("id").returns("10");

            let mockLocalDataRepository = {
                getTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            id: 10,

                            towCompanyId: "test_towCompanyId",
                            towFromLocation: "test_towFromLocation",
                            towToLocation: "test_towToLocation",
                            isHighPriorityTow: "test_isHighPriorityTow",

                            customerName: "test_customerName",
                            customerNamePicture: "test_customerNamePicture",
                            customerId: "test_customerId",
                            customerIdPicture: "test_customerIdPicture",
                            customerPhone: "test_customerPhone",
                            customerStreetAddress: "test_customerStreetAddress",
                            customerZip: "test_customerZip",
                            customerCity: "test_customerCity",

                            vehicleYear: "test_vehicleYear",
                            vehicleMake: "test_vehicleMake",
                            vehicleModel: "test_vehicleModel",
                            vehicleColor: "test_vehicleColor",

                            vehicleVin: "test_vehicleVin",
                            vehicleHasTitle: "test_vehicleHasTitle",
                            vehicleTitle: "test_vehicleTitle",

                            vehicleWheelType: "test_vehicleWheelType",
                            vehicleBodyStyle: "test_vehicleBodyStyle",
                            vehiclePurchasePrice: "test_vehiclePurchasePrice",

                            customerDataNotes: "test_customerDataNotes",
                            vehicleDescriptionNotes: "test_vehicleDescriptionNotes",
                            vehicleIdentificationNotes: "test_vehicleIdentificationNotes",
                            vehiclePurchaseDetailsNotes: "test_vehiclePurchaseDetailsNotes",

                            pickUpNotes: "test_pickUpNotes",
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let mockGenericApiClient = {
                getVehicleModels: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            models: ["toyota", "subaru"]
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            // Act
            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Assert
            assert.ok(mockLocalDataRepository.getTowOrder.calledWith(10));

            assert.equal(viewModel.isPreloaded(), true);
            assert.equal(viewModel.localTowOrderId(), 10);

            assert.equal(viewModel.selectedTowCompanyId(), "test_towCompanyId");
            assert.equal(viewModel.selectedTowFromLocation(), "test_towFromLocation");
            assert.equal(viewModel.selectedTowToLocation(), "test_towToLocation");
            assert.equal(viewModel.isHighPriorityTow(), "test_isHighPriorityTow");

            assert.equal(viewModel.customerName(), "test_customerName");
            assert.equal(viewModel.customerNamePicture(), "test_customerNamePicture");
            assert.equal(viewModel.customerId(), "test_customerId");
            assert.equal(viewModel.customerIdPicture(), "test_customerIdPicture");
            assert.equal(viewModel.customerPhone(), "test_customerPhone");
            assert.equal(viewModel.customerStreetAddress(), "test_customerStreetAddress");
            assert.equal(viewModel.customerZip(), "test_customerZip");
            assert.equal(viewModel.customerCity(), "test_customerCity");

            assert.equal(viewModel.selectedVehicleYear(), "test_vehicleYear");
            assert.equal(viewModel.selectedVehicleMake(), "test_vehicleMake");
            assert.equal(viewModel.selectedVehicleModel(), "test_vehicleModel");
            assert.equal(viewModel.vehicleColor(), "test_vehicleColor");

            assert.equal(viewModel.vehicleVin(), "test_vehicleVin");
            assert.equal(viewModel.vehicleHasTitle(), "test_vehicleHasTitle");
            assert.equal(viewModel.vehicleTitle(), "test_vehicleTitle");

            assert.equal(viewModel.selectedVehicleWheelType(), "test_vehicleWheelType");
            assert.equal(viewModel.selectedVehicleBodyStyle(), "test_vehicleBodyStyle");
            assert.equal(viewModel.vehiclePurchasePrice(), "test_vehiclePurchasePrice");

            assert.equal(viewModel.customerDataNotes(), "test_customerDataNotes");
            assert.equal(viewModel.vehicleDescriptionNotes(), "test_vehicleDescriptionNotes");
            assert.equal(viewModel.vehicleIdentificationNotes(), "test_vehicleIdentificationNotes");
            assert.equal(viewModel.vehiclePurchaseDetailsNotes(), "test_vehiclePurchaseDetailsNotes");

            assert.equal(viewModel.pickUpNotes(), "test_pickUpNotes");
        });

        it("should clone deal when query string params are set", function() {
            // Arrange
            let mockUrlHelper = {
                getQueryStringParam: sinon.stub()
            };

            mockUrlHelper.getQueryStringParam.withArgs("clone").returns("true");
            mockUrlHelper.getQueryStringParam.withArgs("id").returns("12345");

            let mockGenericApiClient = {
                getDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            deal: {
                                id: "12345",

                                sellerName: "test_sellerName",
                                sellerId: "test_sellerId",
                                sellerPhoneNumber: "test_sellerPhoneNumber",
                                vehicleStreetAddress: "test_vehicleStreetAddress",
                                vehicleZip: "test_vehicleZip",
                                vehicleCity: "test_vehicleCity",

                                vehicleYear: "test_vehicleYear",
                                vehicleMake: "test_vehicleMake",
                                vehicleModel: "test_vehicleModel",
                                vehicleColor: "test_vehicleColor",

                                vehicleVin: "test_vehicleVin",
                                vehicleHasTitle: "test_vehicleHasTitle",
                                vehicleTitle: "test_vehicleTitle",

                                vehicleWheelType: "test_vehicleWheelType",
                                vehicleDoors: "test_vehicleDoors",
                                vehiclePurchasePrice: "test_vehiclePurchasePrice",

                                notes: "test_notes",

                                pickUpNotes: "test_pickUpNotes",
                            }
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getVehicleModels: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            models: ["toyota", "subaru"]
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            // Act
            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Assert
            assert.ok(mockGenericApiClient.getDeal.calledWith("12345"));

            assert.equal(viewModel.isCloned(), true);
            assert.equal(viewModel.clonedDealReferenceNumber(), "12345");

            assert.equal(viewModel.customerName(), "test_sellerName");
            assert.equal(viewModel.customerId(), "test_sellerId");
            assert.equal(viewModel.customerPhone(), "test_sellerPhoneNumber");
            assert.equal(viewModel.customerStreetAddress(), "test_vehicleStreetAddress");
            assert.equal(viewModel.customerZip(), "test_vehicleZip");
            assert.equal(viewModel.customerCity(), "test_vehicleCity");

            assert.equal(viewModel.selectedVehicleYear(), "test_vehicleYear");
            assert.equal(viewModel.selectedVehicleMake(), "test_vehicleMake".toLowerCase());
            assert.equal(viewModel.selectedVehicleModel(), "test_vehicleModel".toLowerCase());
            assert.equal(viewModel.vehicleColor(), "test_vehicleColor");

            assert.equal(viewModel.vehicleVin(), "test_vehicleVin");
            assert.equal(viewModel.vehicleHasTitle(), "test_vehicleHasTitle");
            assert.equal(viewModel.vehicleTitle(), "test_vehicleTitle");

            assert.equal(viewModel.selectedVehicleWheelType(), "test_vehicleWheelType");
            assert.equal(viewModel.selectedVehicleBodyStyle(), "test_vehicleDoors");
            assert.equal(viewModel.vehiclePurchasePrice(), "test_vehiclePurchasePrice");

            assert.equal(viewModel.customerDataNotes(), "test_notes");
            assert.equal(viewModel.vehicleDescriptionNotes(), "test_notes");
            assert.equal(viewModel.vehicleIdentificationNotes(), "test_notes");
            assert.equal(viewModel.vehiclePurchaseDetailsNotes(), "test_notes");

            assert.equal(viewModel.pickUpNotes(), "test_pickUpNotes");
        });
    });

    describe("#navigateTo()", function() {
        it("should change the window location hash", function() {
            // Arrange
            let mockUrlHelper = {
                setWindowLocationHash: sinon.spy(),
                getQueryStringParam: sinon.spy(),
                reloadPage: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.navigateTo("test_screen_id");

            // Assert
            assert.ok(mockUrlHelper.setWindowLocationHash.calledWith("test_screen_id"));
        });
    });

    describe("#allDataIsValid()", function() {
        it("should pass validation only when all forms are valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.ok(result);
        });

        it("should fail validation if the tow form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockTowDataValidator = {
                isValid: () => false
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.equal(result, false);
        });

        it("should fail validation if the customer form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockCustomerDataValidator = {
                isValid: () => false
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockCustomerDataValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );
    
            // Act
            let result = viewModel.allDataIsValid();
    
            // Assert
            assert.equal(result, false);
        });

        it("should fail validation if the vehicle description form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };
    
            let mockVehicleDescriptionDataValidator = {
                isValid: () => false
            };
    
            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockVehicleDescriptionDataValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );
    
            // Act
            let result = viewModel.allDataIsValid();
    
            // Assert
            assert.equal(result, false);
        });

        it("should fail validation if the vehicle description form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };
    
            let mockVehicleIdentificationDataValidator = {
                isValid: () => false
            };
    
            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockVehicleIdentificationDataValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );
    
            // Act
            let result = viewModel.allDataIsValid();
    
            // Assert
            assert.equal(result, false);
        });

        it("should fail validation if the vehicle purchase details form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };
    
            let mockVehiclePurchaseDetailsDataValidator = {
                isValid: () => false
            };
    
            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockVehiclePurchaseDetailsDataValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );
    
            // Act
            let result = viewModel.allDataIsValid();
    
            // Assert
            assert.equal(result, false);
        });

        it("should show alert if the customer form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockCustomerDataValidator = {
                isValid: () => false
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockDomSelector = sinon.stub().returns("test_alerts_container");

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockCustomerDataValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.ok(mockAlertsManager.showDanger.withArgs("test_alerts_container").calledOnce);
        });

        it("should show alert if the vehicle description form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockVehicleDescriptionDataValidator = {
                isValid: () => false
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockDomSelector = sinon.stub().returns("test_alerts_container");

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockVehicleDescriptionDataValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.ok(mockAlertsManager.showDanger.withArgs("test_alerts_container").calledOnce);
        });

        it("should show alert if the vehicle identification form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockVehicleIdentificationDataValidator = {
                isValid: () => false
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockDomSelector = sinon.stub().returns("test_alerts_container");

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockVehicleIdentificationDataValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.ok(mockAlertsManager.showDanger.withArgs("test_alerts_container").calledOnce);
        });

        it("should show alert if the vehicle purchase details form is not valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockVehiclePurchaseDetailsDataValidator = {
                isValid: () => false
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockDomSelector = sinon.stub().returns("test_alerts_container");

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockVehiclePurchaseDetailsDataValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.ok(mockAlertsManager.showDanger.withArgs("test_alerts_container").calledOnce);
        });

        it("should dismiss all alerts when all forms are valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                dismiss: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            let result = viewModel.allDataIsValid();

            // Assert
            assert.equal(mockAlertsManager.dismiss.callCount, 4);
        });
    });

    describe("#orderTowOnClick()", function() {
        it("should not submit when there are invalid forms", function() {
            // Arrange
            let mockValidator = {
                isValid: () => false
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockGenericApiClient.createTowOrder.notCalled);
        });

        it("should submit when all forms are valid", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let createTowOrderThen = sinon.stub().returns({
                catch: sinon.spy()
            });

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: createTowOrderThen
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockGenericApiClient.createTowOrder.calledOnce);
            assert.ok(createTowOrderThen.calledOnce);
        });

        it("should populate deal reference number with 200 order tow response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealReferenceNumber: 12345
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.equal(viewModel.dealReferenceNumber(), 12345);
        });

        it("should show alert with 400 ZIP_NOT_SUPPORTED order tow response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400,
                            errorCode: "ZIP_NOT_SUPPORTED"
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });

        it("should show alert with 400 INPUT_DATA_INVALID order tow response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400,
                            errorCode: "INPUT_DATA_INVALID"
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });

        it("should show alert with anything other than 200 or 400 order tow response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 500
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });

        it("should store tow order data when order tow request fails", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("fake error for unit testing");
                        }
                    })
                })
            };

            let mockLocalDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockLocalDataRepository.saveTowOrder.calledOnce);
        });

        it("should show warning after tow order data is stored when order tow request fails", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                showWarning: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("fake error for unit testing");
                        }
                    })
                })
            };

            let mockLocalDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback();
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockAlertsManager.showWarning.calledOnce);
        });

        it("should show danger after tow order data storage fails when order tow request fails", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                showWarning: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                createTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("fake error for unit testing");
                        }
                    })
                })
            };

            let mockLocalDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("fake error for unit testing");
                        }
                    })
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.orderTowOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });
    });

    describe("#saveChangesOnClick()", function() {
        it("should not submit when there are invalid forms", function() {
            // Arrange
            let mockValidator = {
                isValid: () => false
            };

            let mockGenericApiClient = {
                updateDeal: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.saveChangesOnClick();

            // Assert
            assert.ok(mockGenericApiClient.updateDeal.notCalled);
        });

        it("should submit when all forms are valid", function() {
            // Arrange
            //debugger;
            let mockValidator = {
                isValid: () => true
            };

            let updateDealThen = sinon.stub().returns({
                catch: sinon.spy()
            });

            let mockGenericApiClient = {
                updateDeal: sinon.stub().returns({
                    then: updateDealThen
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.saveChangesOnClick();

            // Assert
            assert.ok(mockGenericApiClient.updateDeal.calledOnce);
            assert.ok(updateDealThen.calledOnce);
        });

        it("should populate deal reference number with 200 response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockGenericApiClient = {
                updateDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealReferenceNumber: 12345
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.saveChangesOnClick();

            // Assert
            assert.equal(viewModel.dealReferenceNumber(), 12345);
        });

        it("should show alert with 400 ZIP_NOT_SUPPORTED response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                updateDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400,
                            errorCode: "ZIP_NOT_SUPPORTED"
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.saveChangesOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });

        it("should show alert with 400 INPUT_DATA_INVALID response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                updateDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400,
                            errorCode: "INPUT_DATA_INVALID"
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.saveChangesOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });

        it("should show alert with anything other than 200 or 400 response", function() {
            // Arrange
            let mockValidator = {
                isValid: () => true
            };

            let mockAlertsManager = {
                showDanger: sinon.spy(),
                dismiss: sinon.spy()
            };

            let mockGenericApiClient = {
                updateDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 500
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockValidator, mockValidator, mockValidator, mockValidator, mockValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.saveChangesOnClick();

            // Assert
            assert.ok(mockAlertsManager.showDanger.calledOnce);
        });
    });

    describe("#findCityOnClick()", function() {
        it("should show warning when no zip code is set", function() {
            // Arrange
            let mockAlertsManager = {
                showWarning: sinon.spy(),
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.customerZip(null);

            // Act
            viewModel.findCityOnClick();

            // Assert
            assert.ok(mockAlertsManager.showWarning.called);
        });

        it("should request for city when zip code is set", function() {
            // Arrange
            let mockGenericApiClient = {
                getLocation: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.customerZip("02453");

            // Act
            viewModel.findCityOnClick();

            // Assert
            assert.ok(mockGenericApiClient.getLocation.calledWith("02453"));
        });

        it("should set city with location response when response OK", function() {
            // Arrange
            let mockGenericApiClient = {
                getLocation: sinon.stub().withArgs("02453").returns({
                    then: (callback) => {
                        callback({
                            status: "OK",
                            results: [
                                {
                                    address_components: [
                                        null,
                                        {
                                            long_name: "my test city"
                                        }
                                    ]
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.customerZip("02453");

            // Act
            viewModel.findCityOnClick();

            // Assert
            assert.equal(viewModel.customerCity(), "my test city");
        });

        it("should show alert when location response not OK", function() {
            // Arrange
            let mockAlertsManager = {
                showWarning: sinon.spy(),
            };

            let mockGenericApiClient = {
                getLocation: sinon.stub().withArgs("02453").returns({
                    then: (callback) => {
                        callback({
                            status: "NOT OK"
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.customerZip("02453");

            // Act
            viewModel.findCityOnClick();

            // Assert
            assert.ok(mockAlertsManager.showWarning.called);
        });

        it("should show alert when location response fails", function() {
            // Arrange
            let mockAlertsManager = {
                showWarning: sinon.spy(),
            };

            let mockGenericApiClient = {
                getLocation: sinon.stub().withArgs("02453").returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("fake error for unit testing");
                        }
                    })
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.customerZip("02453");

            // Act
            viewModel.findCityOnClick();

            // Assert
            assert.ok(mockAlertsManager.showWarning.called);
        });
    });

    describe("#captureSignatureClearOnClick()", function() {
        it("should clear the cketchpad", function() {
            // Arrange
            let mockCustomerSignatureSketchpad = {
                baseFill: sinon.spy(),
                clear: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.captureSignatureClearOnClick();

            // Assert
            assert.ok(mockCustomerSignatureSketchpad.clear.called);
        });
    });

    describe("#captureSignatureDoneOnClick()", function() {
        it("should get the signature data url and populate customer name picture", function() {
            // Arrange
            let mockCustomerSignatureSketchpad = {
                baseFill: sinon.spy(),
                clear: sinon.spy(),
                getAsDataUrl: sinon.stub().returns("test signature data url")
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.captureSignatureDoneOnClick();

            // Assert
            assert.ok(mockCustomerSignatureSketchpad.getAsDataUrl.called);
            assert.equal(viewModel.customerNamePicture(), "test signature data url");
        });
    });

    describe("#customerDataDoneOnClick()", function() {
        it("should validate", function() {
            // Arrange
            let mockCustomerDataValidator = {
                isValid: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.customerDataDoneOnClick();

            // Assert
            assert.ok(mockCustomerDataValidator.isValid.called);
        });

        it("should set state flag to true when valid", function() {
            // Arrange
            let mockCustomerDataValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.customerDataDoneOnClick();

            // Assert
            assert.ok(viewModel.isCustomerDataValid());
        });

        it("should set state flag to false when not valid", function() {
            // Arrange
            let mockCustomerDataValidator = {
                isValid: () => false
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.customerDataDoneOnClick();

            // Assert
            assert.equal(viewModel.isCustomerDataValid(), false);
        });

        it("should notify data input completed when valid", function() {
            // Arrange
            let mockCustomerDataValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.strategy.notifyInputDataFormCompleted = sinon.spy();

            // Act
            viewModel.customerDataDoneOnClick();

            // Assert
            assert.ok(viewModel.strategy.notifyInputDataFormCompleted.called);
        });
    });

    describe("#vehicleDescriptionDoneOnClick()", function() {
        it("should validate", function() {
            // Arrange
            let mockVehicleDescriptionValidator = {
                isValid: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehicleDescriptionDoneOnClick();

            // Assert
            assert.ok(mockVehicleDescriptionValidator.isValid.called);
        });

        it("should set state flag to true when valid", function() {
            // Arrange
            let mockVehicleDescriptionValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehicleDescriptionDoneOnClick();

            // Assert
            assert.ok(viewModel.isVehicleDescriptionValid());
        });

        it("should set state flag to false when not valid", function() {
            // Arrange
            let mockVehicleDescriptionValidator = {
                isValid: () => false
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehicleDescriptionDoneOnClick();

            // Assert
            assert.equal(viewModel.isCustomerDataValid(), false);
        });

        it("should notify data input completed when valid", function() {
            // Arrange
            let mockVehicleDescriptionValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.strategy.notifyInputDataFormCompleted = sinon.spy();

            // Act
            viewModel.vehicleDescriptionDoneOnClick();

            // Assert
            assert.ok(viewModel.strategy.notifyInputDataFormCompleted.called);
        });
    });

    describe("#vehicleIdentificationDoneOnClick()", function() {
        it("should validate", function() {
            // Arrange
            let mockVehicleIdentificationValidator = {
                isValid: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehicleIdentificationDoneOnClick();

            // Assert
            assert.ok(mockVehicleIdentificationValidator.isValid.called);
        });

        it("should set state flag to true when valid", function() {
            // Arrange
            let mockVehicleIdentificationValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehicleIdentificationDoneOnClick();

            // Assert
            assert.ok(viewModel.isVehicleIdentificationValid());
        });

        it("should set state flag to false when not valid", function() {
            // Arrange
            let mockVehicleIdentificationValidator = {
                isValid: () => false
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehicleIdentificationDoneOnClick();

            // Assert
            assert.equal(viewModel.isVehicleIdentificationValid(), false);
        });

        it("should notify data input completed when valid", function() {
            // Arrange
            let mockVehicleIdentificationValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.strategy.notifyInputDataFormCompleted = sinon.spy();

            // Act
            viewModel.vehicleIdentificationDoneOnClick();

            // Assert
            assert.ok(viewModel.strategy.notifyInputDataFormCompleted.called);
        });
    });

    describe("#vehiclePurchaseDetailsDoneOnClick()", function() {
        it("should validate", function() {
            // Arrange
            let mockVehiclePurchaseDetailsValidator = {
                isValid: sinon.spy()
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehiclePurchaseDetailsDoneOnClick();

            // Assert
            assert.ok(mockVehiclePurchaseDetailsValidator.isValid.called);
        });

        it("should set state flag to true when valid", function() {
            // Arrange
            let mockVehiclePurchaseDetailsValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehiclePurchaseDetailsDoneOnClick();

            // Assert
            assert.ok(viewModel.isVehiclePurchaseDetailsValid());
        });

        it("should set state flag to false when not valid", function() {
            // Arrange
            let mockVehiclePurchaseDetailsValidator = {
                isValid: () => false
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.vehiclePurchaseDetailsDoneOnClick();

            // Assert
            assert.equal(viewModel.isVehiclePurchaseDetailsValid(), false);
        });

        it("should notify data input completed when valid", function() {
            // Arrange
            let mockVehiclePurchaseDetailsValidator = {
                isValid: () => true
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.strategy.notifyInputDataFormCompleted = sinon.spy();

            // Act
            viewModel.vehiclePurchaseDetailsDoneOnClick();

            // Assert
            assert.ok(viewModel.strategy.notifyInputDataFormCompleted.called);
        });
    });

    describe("#loadVehicleModels()", function() {
        it("should not make request when vehicle year and make are not set", function() {
            // Arrange
            let mockGenericApiClient = {
                getVehicleModels: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            // Act
            viewModel.loadVehicleModels();

            // Assert
            assert.ok(mockGenericApiClient.getVehicleModels.notCalled);
        });

        it("should populate vehicle models when request is successful", function() {
            // Arrange
            let mockGenericApiClient = {
                getVehicleModels: sinon.stub().withArgs("1990", "toyota").returns({
                    then: (callback) => {
                        callback({
                            models: [
                                "corolla", "camry"
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.selectedVehicleMake("toyota");
            viewModel.selectedVehicleYear("1990");

            // Act
            viewModel.loadVehicleModels();

            // Assert
            assert.equal(viewModel.vehicleModels()[0], "corolla");
            assert.equal(viewModel.vehicleModels()[1], "camry");
        });

        it("should disable manual model entry when request is successful", function() {
            // Arrange
            let mockGenericApiClient = {
                getVehicleModels: sinon.stub().withArgs("1990", "toyota").returns({
                    then: (callback) => {
                        callback({
                            models: [
                                "corolla", "camry"
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.selectedVehicleMake("toyota");
            viewModel.selectedVehicleYear("1990");

            // Act
            viewModel.loadVehicleModels();

            // Assert
            assert.equal(viewModel.manualVehicleModelEntryEnabled(), false);
        });

        it("should enable manual model entry when request is not successful", function() {
            // Arrange
            let mockGenericApiClient = {
                getVehicleModels: sinon.stub().withArgs("1990", "toyota").returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("fake error for unit testing");
                        }
                    })
                })
            };

            let viewModel = new EnterNewDealViewModel(
                mockTowCompanies, mockTowDrivers, mockTowFromLocations, mockTowToLocations,
                mockVehicleYears, mockVehicleMakes, mockVehicleWheelTypes, mockVehicleBodyStyles,
                mockTowDataValidator, mockCustomerDataValidator, mockVehicleDescriptionValidator,
                mockVehicleIdentificationValidator, mockVehiclePurchaseDetailsValidator,
                mockAlertsManager,
                mockCustomerSignatureSketchpad, mockRoutingEngine, mockGenericApiClient, mockUrlHelper, mockImageProcessor, mockFileReader,
                mockLocalDataRepository, mockWorkCacheRepository,
                mockDomHelper, mockDomSelector
            );

            viewModel.selectedVehicleMake("toyota");
            viewModel.selectedVehicleYear("1990");

            // Act
            viewModel.loadVehicleModels();

            // Assert
            assert.equal(viewModel.manualVehicleModelEntryEnabled(), true);
        });
    });
});
