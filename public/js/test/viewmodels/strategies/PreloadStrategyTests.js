//import { describe, it } from "mocha";
import assert from "assert";
import sinon from "sinon";

import MockEnterNewDealViewModel from "../../mocks/MockEnterNewDealViewModel";
import PreloadStrategy from "../../../app/viewmodels/strategies/PreloadStrategy";

describe("PreloadStrategy", function() {
    describe("#initViewModel()", function() {
        it("should load the view model with the locally stored tow and deal data", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                getTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            id: 10,

                            towCompanyId: "test_towCompanyId",
                            towDriverId: "test_towDriverId",
                            towFromLocation: "test_towFromLocation",
                            towToLocation: "test_towToLocation",
                            isHighPriorityTow: "test_isHighPriorityTow",

                            customerName: "test_customerName",
                            customerNamePicture: "test_customerNamePicture",
                            customerId: "test_customerId",
                            customerIdPicture: "test_customerIdPicture",
                            customerPhone: "test_customerPhone",
                            customerStreetAddress: "test_customerStreetAddress",
                            customerZip: "test_customerZip",
                            customerCity: "test_customerCity",

                            vehicleYear: "test_vehicleYear",
                            vehicleMake: "test_vehicleMake",
                            vehicleModel: "test_vehicleModel",
                            vehicleColor: "test_vehicleColor",
                            vehiclePictures: ["vehiclePictures_1", "vehiclePictures_2"],

                            vehicleVin: "test_vehicleVin",
                            vehicleHasTitle: "test_vehicleHasTitle",
                            vehicleTitle: "test_vehicleTitle",

                            vehicleWheelType: "test_vehicleWheelType",
                            vehicleBodyStyle: "test_vehicleBodyStyle",
                            vehiclePurchasePrice: "test_vehiclePurchasePrice",

                            customerDataNotes: "test_customerDataNotes",
                            vehicleDescriptionNotes: "test_vehicleDescriptionNotes",
                            vehicleIdentificationNotes: "test_vehicleIdentificationNotes",
                            vehiclePurchaseDetailsNotes: "test_vehiclePurchaseDetailsNotes",

                            pickUpNotes: "test_pickUpNotes",
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let strategy = new PreloadStrategy(mockViewModel, 10);

            // Act
            strategy.initViewModel();

            // Assert
            assert.ok(mockViewModel.localDataRepository.getTowOrder.calledWith(10));

            assert.equal(mockViewModel.isPreloaded(), true);
            assert.equal(mockViewModel.localTowOrderId(), 10);

            assert.equal(mockViewModel.selectedTowCompanyId(), "test_towCompanyId");
            assert.equal(mockViewModel.selectedTowDriverId(), "test_towDriverId");
            assert.equal(mockViewModel.selectedTowFromLocation(), "test_towFromLocation");
            assert.equal(mockViewModel.selectedTowToLocation(), "test_towToLocation");
            assert.equal(mockViewModel.isHighPriorityTow(), "test_isHighPriorityTow");

            assert.equal(mockViewModel.customerName(), "test_customerName");
            assert.equal(mockViewModel.customerNamePicture(), "test_customerNamePicture");
            assert.equal(mockViewModel.customerId(), "test_customerId");
            assert.equal(mockViewModel.customerIdPicture(), "test_customerIdPicture");
            assert.equal(mockViewModel.customerPhone(), "test_customerPhone");
            assert.equal(mockViewModel.customerStreetAddress(), "test_customerStreetAddress");
            assert.equal(mockViewModel.customerZip(), "test_customerZip");
            assert.equal(mockViewModel.customerCity(), "test_customerCity");

            assert.equal(mockViewModel.selectedVehicleYear(), "test_vehicleYear");
            assert.equal(mockViewModel.selectedVehicleMake(), "test_vehicleMake");
            assert.equal(mockViewModel.selectedVehicleModel(), "test_vehicleModel");
            assert.equal(mockViewModel.vehicleColor(), "test_vehicleColor");

            assert.equal(mockViewModel.vehicleVin(), "test_vehicleVin");
            assert.equal(mockViewModel.vehicleHasTitle(), "test_vehicleHasTitle");
            assert.equal(mockViewModel.vehicleTitle(), "test_vehicleTitle");

            assert.equal(mockViewModel.selectedVehicleWheelType(), "test_vehicleWheelType");
            assert.equal(mockViewModel.selectedVehicleBodyStyle(), "test_vehicleBodyStyle");
            assert.equal(mockViewModel.vehiclePurchasePrice(), "test_vehiclePurchasePrice");

            assert.equal(mockViewModel.customerDataNotes(), "test_customerDataNotes");
            assert.equal(mockViewModel.vehicleDescriptionNotes(), "test_vehicleDescriptionNotes");
            assert.equal(mockViewModel.vehicleIdentificationNotes(), "test_vehicleIdentificationNotes");
            assert.equal(mockViewModel.vehiclePurchaseDetailsNotes(), "test_vehiclePurchaseDetailsNotes");

            assert.equal(mockViewModel.pickUpNotes(), "test_pickUpNotes");
        });
    });

    describe("#notifyDealSubmitted()", function() {
        it("should remove locally stored tow order", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                removeTowOrder: sinon.stub()
            };

            mockViewModel.localTowOrderId(10);

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new PreloadStrategy(mockViewModel, 10);

            // Act
            strategy.notifyDealSubmitted();

            // Assert
            assert.ok(mockViewModel.localDataRepository.removeTowOrder.calledOnce);
            assert.ok(mockViewModel.localDataRepository.removeTowOrder.calledWith(10));
        });

        it("should show success alert", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                removeTowOrder: sinon.spy()
            };

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new PreloadStrategy(mockViewModel, 10);

            // Act
            strategy.notifyDealSubmitted();

            // Assert
            assert.ok(mockViewModel.alertsManager.showSuccess.calledOnce);
        });
    });

    describe("#notifyTowOrderStoredLocally()", function() {
        it("should remove previous locally stored tow order", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                removeTowOrder: sinon.stub()
            };

            mockViewModel.localTowOrderId(10);

            let strategy = new PreloadStrategy(mockViewModel, 10);

            // Act
            strategy.notifyTowOrderStoredLocally();

            // Assert
            assert.ok(mockViewModel.localDataRepository.removeTowOrder.calledOnce);
            assert.ok(mockViewModel.localDataRepository.removeTowOrder.calledWith(10));
        });
    });
});
