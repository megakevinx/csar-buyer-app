//import { describe, it } from "mocha";
import assert from "assert";
import sinon from "sinon";

import MockEnterNewDealViewModel from "../../mocks/MockEnterNewDealViewModel";
import EditStrategy from "../../../app/viewmodels/strategies/EditStrategy";

describe("EditStrategy", function() {
    describe("#initViewModel()", function() {
        it("should load the view model with the incoming deal and tow data", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                getDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            deal: {
                                id: 123456,

                                sellerName: "test_customerName",
                                sellerNamePicture: "test_customerNamePicture",
                                sellerId: "test_customerId",
                                sellerIdPicture: "test_customerIdPicture",
                                sellerGender: "test_customerGender",
                                sellerPhoneNumber: "test_customerPhone",
                                vehicleStreetAddress: "test_customerStreetAddress",
                                vehicleZip: "test_customerZip",
                                vehicleCity: "test_customerCity",

                                vehicleYear: "test_vehicleYear",
                                vehicleMake: "test_vehicleMake",
                                vehicleModel: "test_vehicleModel",
                                vehicleColor: "test_vehicleColor",

                                vehicleVin: "test_vehicleVin",
                                vehicleHasTitle: "test_vehicleHasTitle",
                                vehicleTitle: "test_vehicleTitle",

                                vehicleWheelType: "test_vehicleWheelType",
                                vehicleDoors: "test_vehicleBodyStyle",
                                vehiclePurchasePrice: "test_vehiclePurchasePrice",

                                notes: "test_notes",

                                pickUpNotes: "test_pickUpNotes",
                            },
                            tows: [
                                {
                                    towCompanyId: "test_towCompanyId_1",
                                    buyerId: "test_buyerId_1",
                                    towFromLocation: "test_towFromLocation_1",
                                    towToLocation: "test_towToLocation_1",
                                    isHighPriorityTow: 1,
                                },
                                {
                                    towCompanyId: "test_towCompanyId_2",
                                    buyerId: "test_buyerId_2",
                                    towFromLocation: "test_towFromLocation_2",
                                    towToLocation: "test_towToLocation_2",
                                    isHighPriorityTow: 0,
                                },
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let strategy = new EditStrategy(mockViewModel, 123456);

            // Act
            strategy.initViewModel();

            // Assert
            assert.ok(mockViewModel.genericApiClient.getDeal.calledWith(123456));

            assert.equal(mockViewModel.isEditing(), true);
            assert.equal(mockViewModel.editingDealNumber(), 123456);

            assert.equal(mockViewModel.selectedTowCompanyId(), "test_towCompanyId_1");
            assert.equal(mockViewModel.selectedTowDriverId(), "test_buyerId_1");
            assert.equal(mockViewModel.selectedTowFromLocation(), "test_towFromLocation_1");
            assert.equal(mockViewModel.selectedTowToLocation(), "test_towToLocation_1");
            assert.equal(mockViewModel.isHighPriorityTow(), "yes");

            assert.equal(mockViewModel.pastTowOrders().length, 2);

            assert.equal(mockViewModel.customerName(), "test_customerName");
            assert.equal(mockViewModel.customerNamePicture(), "test_customerNamePicture");
            assert.equal(mockViewModel.customerId(), "test_customerId");
            assert.equal(mockViewModel.customerIdPicture(), "test_customerIdPicture");
            assert.equal(mockViewModel.selectedCustomerGender(), "test_customerGender");
            assert.equal(mockViewModel.customerPhone(), "test_customerPhone");
            assert.equal(mockViewModel.customerStreetAddress(), "test_customerStreetAddress");
            assert.equal(mockViewModel.customerZip(), "test_customerZip");
            assert.equal(mockViewModel.customerCity(), "test_customerCity");

            assert.equal(mockViewModel.selectedVehicleYear(), "test_vehicleYear");
            assert.equal(mockViewModel.selectedVehicleMake(), "test_vehicleMake".toLowerCase());
            assert.equal(mockViewModel.selectedVehicleModel(), "test_vehicleModel".toLowerCase());
            assert.equal(mockViewModel.vehicleColor(), "test_vehicleColor");

            assert.equal(mockViewModel.vehicleVin(), "test_vehicleVin");
            assert.equal(mockViewModel.vehicleHasTitle(), "test_vehicleHasTitle");
            assert.equal(mockViewModel.vehicleTitle(), "test_vehicleTitle");

            assert.equal(mockViewModel.selectedVehicleWheelType(), "test_vehicleWheelType");
            assert.equal(mockViewModel.selectedVehicleBodyStyle(), "test_vehicleBodyStyle");
            assert.equal(mockViewModel.vehiclePurchasePrice(), "test_vehiclePurchasePrice");

            assert.equal(mockViewModel.customerDataNotes(), "test_notes");
            assert.equal(mockViewModel.vehicleDescriptionNotes(), "test_notes");
            assert.equal(mockViewModel.vehicleIdentificationNotes(), "test_notes");
            assert.equal(mockViewModel.vehiclePurchaseDetailsNotes(), "test_notes");

            assert.equal(mockViewModel.pickUpNotes(), "test_pickUpNotes");
        });

        it("should show danger when request fails", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                getDeal: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => callback("mock error")
                    })
                })
            };

            mockViewModel.alertsManager = {
                showDanger: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new EditStrategy(mockViewModel, 123456);

            // Act
            strategy.initViewModel();

            // Assert
            assert.ok(mockViewModel.alertsManager.showDanger.calledOnce);
        });

        it("should not put the view model in editing mode if request fails", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.alertsManager = {
                showDanger: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            var strategy = new EditStrategy(mockViewModel, 123456);

            mockViewModel.genericApiClient = {
                getDeal: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("mock error");
                        }
                    })
                })
            };

            // Act
            strategy.initViewModel();

            assert.equal(mockViewModel.isEditing(), false);
        });
    });

    describe("#notifyDealSubmitted()", function() {
        it("should show success", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                getDeal: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.stub()
                    })
                })
            };

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new EditStrategy(mockViewModel, 123456);

            // Act
            strategy.notifyDealSubmitted();

            // Assert
            assert.ok(mockViewModel.alertsManager.showSuccess.calledOnce);
        });
    });

    describe("#getSubmitTowOrderApiCall()", function() {
        it("should return update tow order method", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                updateTowOrder: () => "this is a function"
            };

            let strategy = new EditStrategy(mockViewModel);

            // Act
            let result = strategy.getSubmitTowOrderApiCall();

            // Assert
            assert.equal(result(), "this is a function");
        });
    });

    describe("#prepareTowOrderData()", function() {
        it("should add the editing deal number as a deal number to the data", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();
            mockViewModel.editingDealNumber(12345);
            let strategy = new EditStrategy(mockViewModel);

            // Act
            let result = strategy.prepareTowOrderData({});

            // Assert
            assert.equal(result.dealNumber, 12345);
        });
    });

    describe("#notifyDealSubmissionFailed()", function() {
        it("should show danger", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.alertsManager = {
                showDanger: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new EditStrategy(mockViewModel, 123456);

            // Act
            strategy.notifyDealSubmissionFailed(null, { messsage: "this is an error message" });

            // Assert
            assert.ok(mockViewModel.alertsManager.showDanger.calledOnce);
        });
    });
});
