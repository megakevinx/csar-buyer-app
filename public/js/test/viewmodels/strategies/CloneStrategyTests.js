//import { describe, it } from "mocha";
import assert from "assert";
import sinon from "sinon";

import MockEnterNewDealViewModel from "../../mocks/MockEnterNewDealViewModel";
import CloneStrategy from "../../../app/viewmodels/strategies/CloneStrategy";

describe("CloneStrategy", function() {
    describe("#initViewModel()", function() {
        it("should load the view model with the incoming request data", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                getDeal: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            deal: {
                                id: 123456,

                                sellerName: "test_customerName",
                                sellerNamePicture: "test_customerNamePicture",
                                sellerId: "test_customerId",
                                sellerIdPicture: "test_customerIdPicture",
                                sellerGender: "test_customerGender",
                                sellerPhoneNumber: "test_customerPhone",
                                vehicleStreetAddress: "test_customerStreetAddress",
                                vehicleZip: "test_customerZip",
                                vehicleCity: "test_customerCity",

                                vehicleYear: "test_vehicleYear",
                                vehicleMake: "test_vehicleMake",
                                vehicleModel: "test_vehicleModel",
                                vehicleColor: "test_vehicleColor",

                                vehicleVin: "test_vehicleVin",
                                vehicleHasTitle: "test_vehicleHasTitle",
                                vehicleTitle: "test_vehicleTitle",

                                vehicleWheelType: "test_vehicleWheelType",
                                vehicleDoors: "test_vehicleBodyStyle",
                                vehiclePurchasePrice: "test_vehiclePurchasePrice",

                                notes: "test_notes",

                                pickUpNotes: "test_pickUpNotes",
                            }
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            // Act
            let strategy = new CloneStrategy(mockViewModel, 123456);

            strategy.initViewModel();

            // Assert
            assert.ok(mockViewModel.genericApiClient.getDeal.calledWith(123456));

            assert.equal(mockViewModel.isCloned(), true);
            assert.equal(mockViewModel.clonedDealReferenceNumber(), 123456);

            assert.equal(mockViewModel.customerName(), "test_customerName");
            assert.equal(mockViewModel.customerNamePicture(), "test_customerNamePicture");
            assert.equal(mockViewModel.customerId(), "test_customerId");
            assert.equal(mockViewModel.customerIdPicture(), "test_customerIdPicture");
            assert.equal(mockViewModel.selectedCustomerGender(), "test_customerGender");
            assert.equal(mockViewModel.customerPhone(), "test_customerPhone");
            assert.equal(mockViewModel.customerStreetAddress(), "test_customerStreetAddress");
            assert.equal(mockViewModel.customerZip(), "test_customerZip");
            assert.equal(mockViewModel.customerCity(), "test_customerCity");

            assert.equal(mockViewModel.selectedVehicleYear(), "test_vehicleYear");
            assert.equal(mockViewModel.selectedVehicleMake(), "test_vehicleMake".toLowerCase());
            assert.equal(mockViewModel.selectedVehicleModel(), "test_vehicleModel".toLowerCase());
            assert.equal(mockViewModel.vehicleColor(), "test_vehicleColor");

            assert.equal(mockViewModel.vehicleVin(), "test_vehicleVin");
            assert.equal(mockViewModel.vehicleHasTitle(), "test_vehicleHasTitle");
            assert.equal(mockViewModel.vehicleTitle(), "test_vehicleTitle");

            assert.equal(mockViewModel.selectedVehicleWheelType(), "test_vehicleWheelType");
            assert.equal(mockViewModel.selectedVehicleBodyStyle(), "test_vehicleBodyStyle");
            assert.equal(mockViewModel.vehiclePurchasePrice(), "test_vehiclePurchasePrice");

            assert.equal(mockViewModel.customerDataNotes(), "test_notes");
            assert.equal(mockViewModel.vehicleDescriptionNotes(), "test_notes");
            assert.equal(mockViewModel.vehicleIdentificationNotes(), "test_notes");
            assert.equal(mockViewModel.vehiclePurchaseDetailsNotes(), "test_notes");

            assert.equal(mockViewModel.pickUpNotes(), "test_pickUpNotes");
        });

        it("should show alert when request fails", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                getDeal: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => callback("mock error")
                    })
                })
            };

            mockViewModel.alertsManager = {
                showDanger: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            // Act
            let strategy = new CloneStrategy(mockViewModel, 123456);

            strategy.initViewModel();

            // Assert
            assert.ok(mockViewModel.alertsManager.showDanger.calledOnce);
        });
    });

    describe("#notifyDealSubmitted()", function() {
        it("should show alert", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            // Act
            let strategy = new CloneStrategy(mockViewModel, 123456);

            strategy.notifyDealSubmitted();

            // Assert
            assert.ok(mockViewModel.alertsManager.showSuccess.calledOnce);
        });
    });
});
