//import { describe, it } from "mocha";
import assert from "assert";
import sinon from "sinon";

import MockEnterNewDealViewModel from "../../mocks/MockEnterNewDealViewModel";
import CreateStrategy from "../../../app/viewmodels/strategies/CreateStrategy";

describe("CreateStrategy", function() {
    describe("#initViewModel()", function() {
        it("should do nothing when there's no work cache", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.workCacheRepository = {
                getWorkCache: sinon.stub().returns({
                    then: (callback) => {

                        callback(null); // callback with null means that there's no work cache

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.initViewModel();

            // Assert
            assert.equal(mockViewModel.customerName(), null);
            assert.equal(mockViewModel.customerNamePicture(), null);
            assert.equal(mockViewModel.customerId(), null);
            assert.equal(mockViewModel.customerIdPicture(), null);
            assert.equal(mockViewModel.customerPhone(), null);
            assert.equal(mockViewModel.customerStreetAddress(), null);
            assert.equal(mockViewModel.customerZip(), null);
            assert.equal(mockViewModel.customerCity(), null);

            assert.equal(mockViewModel.selectedVehicleYear(), null);
            assert.equal(mockViewModel.selectedVehicleMake(), null);
            assert.equal(mockViewModel.selectedVehicleModel(), null);
            assert.equal(mockViewModel.vehicleColor(), null);

            assert.equal(mockViewModel.vehicleVin(), null);
            assert.equal(mockViewModel.vehicleHasTitle(), null);
            assert.equal(mockViewModel.vehicleTitle(), null);

            assert.equal(mockViewModel.selectedVehicleWheelType(), null);
            assert.equal(mockViewModel.selectedVehicleBodyStyle(), null);
            assert.equal(mockViewModel.vehiclePurchasePrice(), null);

            assert.equal(mockViewModel.customerDataNotes(), null);
            assert.equal(mockViewModel.vehicleDescriptionNotes(), null);
            assert.equal(mockViewModel.vehicleIdentificationNotes(), null);
            assert.equal(mockViewModel.vehiclePurchaseDetailsNotes(), null);

            assert.equal(mockViewModel.pickUpNotes(), null);
        });

        it("should load view model when there's a work cache", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            mockViewModel.workCacheRepository = {
                getWorkCache: sinon.stub().returns({
                    then: (callback) => {

                        callback({
                            id: 10,

                            towCompanyId: "test_towCompanyId",
                            towDriverId: "test_towDriverId",
                            towFromLocation: "test_towFromLocation",
                            towToLocation: "test_towToLocation",
                            isHighPriorityTow: "test_isHighPriorityTow",

                            customerName: "test_customerName",
                            customerNamePicture: "test_customerNamePicture",
                            customerId: "test_customerId",
                            customerIdPicture: "test_customerIdPicture",
                            customerPhone: "test_customerPhone",
                            customerStreetAddress: "test_customerStreetAddress",
                            customerZip: "test_customerZip",
                            customerCity: "test_customerCity",

                            vehicleYear: "test_vehicleYear",
                            vehicleMake: "test_vehicleMake",
                            vehicleModel: "test_vehicleModel",
                            vehicleColor: "test_vehicleColor",
                            vehiclePictures: ["vehiclePictures_1", "vehiclePictures_2"],

                            vehicleVin: "test_vehicleVin",
                            vehicleHasTitle: "test_vehicleHasTitle",
                            vehicleTitle: "test_vehicleTitle",

                            vehicleWheelType: "test_vehicleWheelType",
                            vehicleBodyStyle: "test_vehicleBodyStyle",
                            vehiclePurchasePrice: "test_vehiclePurchasePrice",

                            customerDataNotes: "test_customerDataNotes",
                            vehicleDescriptionNotes: "test_vehicleDescriptionNotes",
                            vehicleIdentificationNotes: "test_vehicleIdentificationNotes",
                            vehiclePurchaseDetailsNotes: "test_vehiclePurchaseDetailsNotes",

                            pickUpNotes: "test_pickUpNotes",
                        });
                    }
                })
            };

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.initViewModel();

            // Assert
            assert.equal(mockViewModel.selectedTowCompanyId(), "test_towCompanyId");
            assert.equal(mockViewModel.selectedTowDriverId(), "test_towDriverId");
            assert.equal(mockViewModel.selectedTowFromLocation(), "test_towFromLocation");
            assert.equal(mockViewModel.selectedTowToLocation(), "test_towToLocation");
            assert.equal(mockViewModel.isHighPriorityTow(), "test_isHighPriorityTow");

            assert.equal(mockViewModel.customerName(), "test_customerName");
            assert.equal(mockViewModel.customerNamePicture(), "test_customerNamePicture");
            assert.equal(mockViewModel.customerId(), "test_customerId");
            assert.equal(mockViewModel.customerIdPicture(), "test_customerIdPicture");
            assert.equal(mockViewModel.customerPhone(), "test_customerPhone");
            assert.equal(mockViewModel.customerStreetAddress(), "test_customerStreetAddress");
            assert.equal(mockViewModel.customerZip(), "test_customerZip");
            assert.equal(mockViewModel.customerCity(), "test_customerCity");

            assert.equal(mockViewModel.selectedVehicleYear(), "test_vehicleYear");
            assert.equal(mockViewModel.selectedVehicleMake(), "test_vehicleMake");
            assert.equal(mockViewModel.selectedVehicleModel(), "test_vehicleModel");
            assert.equal(mockViewModel.vehicleColor(), "test_vehicleColor");

            assert.equal(mockViewModel.vehicleVin(), "test_vehicleVin");
            assert.equal(mockViewModel.vehicleHasTitle(), "test_vehicleHasTitle");
            assert.equal(mockViewModel.vehicleTitle(), "test_vehicleTitle");

            assert.equal(mockViewModel.selectedVehicleWheelType(), "test_vehicleWheelType");
            assert.equal(mockViewModel.selectedVehicleBodyStyle(), "test_vehicleBodyStyle");
            assert.equal(mockViewModel.vehiclePurchasePrice(), "test_vehiclePurchasePrice");

            assert.equal(mockViewModel.customerDataNotes(), "test_customerDataNotes");
            assert.equal(mockViewModel.vehicleDescriptionNotes(), "test_vehicleDescriptionNotes");
            assert.equal(mockViewModel.vehicleIdentificationNotes(), "test_vehicleIdentificationNotes");
            assert.equal(mockViewModel.vehiclePurchaseDetailsNotes(), "test_vehiclePurchaseDetailsNotes");

            assert.equal(mockViewModel.pickUpNotes(), "test_pickUpNotes");
        });
    });

    describe("#notifyInputDataFormCompleted()", function() {
        it("should save view model state in work cache", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.getTowOrderDataToPersist = sinon.spy();

            mockViewModel.workCacheRepository = {
                saveWorkCache: sinon.stub().returns({
                    then: sinon.spy()
                })
            };

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyInputDataFormCompleted();

            // Assert
            assert.ok(mockViewModel.getTowOrderDataToPersist.calledOnce);
            assert.ok(mockViewModel.workCacheRepository.saveWorkCache.calledOnce);
        });
    });

    describe("#notifyDealSubmitted()", function() {
        it("should clear work cache", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.workCacheRepository = {
                clearWorkCache: sinon.stub().returns({
                    then: sinon.spy()
                })
            };

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyDealSubmitted();

            // Assert
            assert.ok(mockViewModel.workCacheRepository.clearWorkCache.calledOnce);
        });

        it("should show success alert", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.workCacheRepository = {
                clearWorkCache: sinon.stub().returns({
                    then: sinon.spy()
                })
            };

            mockViewModel.alertsManager = {
                showSuccess: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyDealSubmitted();

            // Assert
            assert.ok(mockViewModel.alertsManager.showSuccess.calledOnce);
        });
    });

    describe("#getSubmitTowOrderApiCall()", function() {
        it("should return create tow order method", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.genericApiClient = {
                createTowOrder: () => "this is a function"
            };

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            let result = strategy.getSubmitTowOrderApiCall();

            // Assert
            assert.equal(result(), "this is a function");
        });
    });

    describe("#prepareTowOrderData()", function() {
        it("should do nothing to the incoming data", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();
            let strategy = new CreateStrategy(mockViewModel);

            // Act
            let result = strategy.prepareTowOrderData("this is some data");

            // Assert
            assert.equal(result, "this is some data");
        });
    });

    describe("#notifyDealSubmissionFailed()", function() {
        it("should store the deal data locally", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.stub()
                    })
                })
            };

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyDealSubmissionFailed("this is some data");

            // Assert
            assert.ok(mockViewModel.localDataRepository.saveTowOrder.calledWith("this is some data"));
        });

        it("should notify view model that tow was stored locally", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback();
                        return {
                            catch: sinon.stub()
                        };
                    }
                })
            };

            mockViewModel.alertsManager = {
                showWarning: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyDealSubmissionFailed("this is some data", { message: "this is a test error message" });

            // Assert
            assert.equal(mockViewModel.towOrderStoredLocally(), true);
        });

        it("should show a warning when tow data is stored locally", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: (callback) => {
                        callback();
                        return {
                            catch: sinon.stub()
                        };
                    }
                })
            };

            mockViewModel.alertsManager = {
                showWarning: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyDealSubmissionFailed("this is some data", { message: "this is a test error message" });

            // Assert
            assert.ok(mockViewModel.alertsManager.showWarning.calledOnce);
        });

        it("should show danger when data fails to store locally", function() {
            // Arrange
            let mockViewModel = new MockEnterNewDealViewModel();

            mockViewModel.localDataRepository = {
                saveTowOrder: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => callback("mock error")
                    })
                })
            };

            mockViewModel.alertsManager = {
                showDanger: sinon.spy()
            };

            mockViewModel.$ = sinon.spy();

            let strategy = new CreateStrategy(mockViewModel);

            // Act
            strategy.notifyDealSubmissionFailed("this is some data");

            // Assert
            assert.ok(mockViewModel.alertsManager.showDanger.calledOnce);
        });
    });
});
