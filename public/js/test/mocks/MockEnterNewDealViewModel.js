export default class MockEntreNewDealViewModel {
    constructor() {
        this.dealReferenceNumber = this.getMockObservable();
        this.towNumber = this.getMockObservable();
        this.towOrderStoredLocally = this.getMockObservable();

        this.isLoadingRequest = this.getMockObservable();

        this.isCloned = this.getMockObservable();
        this.clonedDealReferenceNumber = this.getMockObservable();

        this.isEditing = this.getMockObservable();
        this.editingDealNumber = this.getMockObservable();

        this.isPreloaded = this.getMockObservable();
        this.localTowOrderId = this.getMockObservable();

        this.selectedTowCompanyId = this.getMockObservable();
        this.selectedTowDriverId = this.getMockObservable();
        this.selectedTowFromLocation = this.getMockObservable();
        this.selectedTowToLocation = this.getMockObservable();
        this.isHighPriorityTow = this.getMockObservable();

        this.pastTowOrders = this.getMockObservable();

        this.customerName = this.getMockObservable();
        this.customerNamePicture = this.getMockObservable();
        this.customerId = this.getMockObservable();
        this.customerIdPicture = this.getMockObservable();
        this.selectedCustomerGender = this.getMockObservable();
        this.customerPhone = this.getMockObservable();
        this.customerStreetAddress = this.getMockObservable();
        this.customerZip = this.getMockObservable();
        this.customerCity = this.getMockObservable();

        this.selectedVehicleYear = this.getMockObservable();
        this.selectedVehicleMake = this.getMockObservable();
        this.selectedVehicleModel = this.getMockObservable();
        this.vehicleColor = this.getMockObservable();
        this.vehiclePictures = this.getMockObservable();

        this.vehicleVin = this.getMockObservable();
        this.vehicleHasTitle = this.getMockObservable();
        this.vehicleTitle = this.getMockObservable();

        this.selectedVehicleWheelType = this.getMockObservable();
        this.selectedVehicleBodyStyle = this.getMockObservable();
        this.vehiclePurchasePrice = this.getMockObservable();

        this.customerDataNotes = this.getMockObservable();
        this.vehicleDescriptionNotes = this.getMockObservable();
        this.vehicleIdentificationNotes = this.getMockObservable();
        this.vehiclePurchaseDetailsNotes = this.getMockObservable();

        this.pickUpNotes = this.getMockObservable();

        this.loadVehicleModels = (callback) => callback();
        this.towCompanyOnSelect = () => {};
    }

    getMockObservable() {

        let stored = null;
    
        return (value) => {
            if (value !== null && value !== undefined) {
                stored = value;
            }
            else {
                return stored;
            }
        };
    }
}