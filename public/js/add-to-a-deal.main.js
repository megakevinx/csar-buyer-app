import ko from "knockout";

import AddToADealViewModel from "./app/viewmodels/AddToADealViewModel";

$(document).ready(function () {

    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('./service-worker.js')
            .then(function() { console.log('Service Worker Registered'); });
    }

    fetch("api/configuration/add-to-a-deal", { credentials: 'include' })
    .then(response => response.json())
    .then(data => {
        let recentDeals = data.recentDeals;

        ko.applyBindings(
            new AddToADealViewModel(recentDeals)
        );

        $("#overlay").fadeOut();
    })
    .catch(error => {
        console.log(error);

        ko.applyBindings(
            new AddToADealViewModel(null)
        );

        $("#overlay").fadeOut();
    });
});
