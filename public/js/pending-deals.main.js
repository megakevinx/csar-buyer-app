import ko from "knockout";
import idb from "idb";


import PendingDealsViewModel from "./app/viewmodels/PendingDealsViewModel";

import AlertsManager from "./app/alerts/AlertsManager";
import GenericApiClient from "./app/apiclients/GenericApiClient";
import LocalDataRepository from "./app/repositories/LocalDataRepository";

$(document).ready(function () {

    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('./service-worker.js')
            .then(function() { console.log('Service Worker Registered'); });
    }

    $("div.validation-error-alert a.close").click(function(eventObject) {
        $(eventObject.target.parentElement).hide();
    });

    ko.applyBindings(
        new PendingDealsViewModel(
            new AlertsManager(),
            new GenericApiClient(),
            new LocalDataRepository(idb),
            $
        )
    );

    $("#overlay").fadeOut();
});
