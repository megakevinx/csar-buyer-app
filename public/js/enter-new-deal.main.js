import ko from "knockout";
import "sammy";
import idb from "idb";

import RoutingEngine from "./app/routing/RoutingEngine";

import AlertsManager from "./app/alerts/AlertsManager";

import TowDataValidator from "./app/validators/TowDataValidator";
import CustomerDataValidator from "./app/validators/CustomerDataValidator";
import VehicleDescriptionValidator from "./app/validators/VehicleDescriptionValidator";
import VehicleIdentificationValidator from "./app/validators/VehicleIdentificationValidator";
import VehiclePurchaseDetailsValidator from "./app/validators/VehiclePurchaseDetailsValidator";

import GenericApiClient from "./app/apiclients/GenericApiClient";

import UrlHelper from "./app/helpers/UrlHelper";
import ImageProcessor from "./app/helpers/ImageProcessor";
import PromisedFileReader from "./app/helpers/PromisedFileReader";
import SketchpadHelper from "./app/helpers/SketchpadHelper";
import LocalDataRepository from "./app/repositories/LocalDataRepository";
import WorkCacheRepository from "./app/repositories/WorkCacheRepository";
import DomHelper from "./app/helpers/DomHelper";

import EnterNewDealViewModel from "./app/viewmodels/EnterNewDealViewModel";

//import OperationSelectorComponent from "./app/viewmodels/components/OperationSelectorComponent";

$(document).ready(function () {

    //var fs = require("fs");

    // ko.components.register("operation-selector", {
    //     viewModel: OperationSelectorComponent,
    //     template: fs.readFileSync("app/viewmodels/templates/operation-selector-template.html", "utf8")
    // });

    if ("serviceWorker" in navigator) {
        navigator.serviceWorker
            .register("./service-worker.js")
            .then(function() { console.log("Service Worker Registered"); });
    }

    $("div.validation-error-alert a.close").click(function(eventObject) {
        $(eventObject.target.parentElement).hide();
    });

    ko.bindingHandlers.fadeVisible = {
        init: function(element, valueAccessor) {
            // Initially set the element to be instantly visible/hidden depending on the value
            var value = valueAccessor();
            $(element).toggle(ko.unwrap(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
        },
        update: function(element, valueAccessor) {
            // Whenever the value subsequently changes, slowly fade the element in or out
            var value = valueAccessor();
            ko.unwrap(value) ? $(element).slideDown() : $(element).slideUp();
        }
    };

    fetch("api/configuration/enter-new-deal")
        .then(response => response.json())
        .then(data => {

            let vehicleYears = data.vehicleYears;
            let vehicleMakes = data.vehicleMakes;

            let vehicleWheelTypes = data.vehicleWheelTypes;
            let vehicleBodyStyles = data.vehicleBodyStyles;

            var sketchpadHelper = new SketchpadHelper(
                "#capture-signature-sketchpad",
                "#capture-signature-sketchpad-container"
            );

            $(window).bind("orientationchange resize", function(event) {
                sketchpadHelper.reset();
            });

            ko.applyBindings(
                new EnterNewDealViewModel(
                    vehicleYears, vehicleMakes, vehicleWheelTypes, vehicleBodyStyles,
                    new TowDataValidator("form#enter_new_deal_form", "div#tow_data_validation_error_container"),
                    new CustomerDataValidator("form#customer_data_form", "div#customer_data_validation_error_container"),
                    new VehicleDescriptionValidator("form#vehicle_description_form", "div#vehicle_description_validation_error_container"),
                    new VehicleIdentificationValidator("form#vehicle_identification_form", "div#vehicle_identification_validation_error_container"),
                    new VehiclePurchaseDetailsValidator("form#vehicle_purchase_details_form", "div#vehicle_purchase_details_validation_error_container"),
                    new AlertsManager(),
                    sketchpadHelper,
                    new RoutingEngine(),
                    new GenericApiClient(),
                    new UrlHelper(),
                    new ImageProcessor(),
                    new PromisedFileReader(),
                    new LocalDataRepository(idb), new WorkCacheRepository(idb),
                    new DomHelper(),
                    $
                )
            );

            $("#overlay").fadeOut();
        })
        .catch(error => {
            console.log(error);
            $("#page-loading-error").text(error);
        });
});
