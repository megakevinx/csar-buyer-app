CREATE TABLE `TowPricingRule` (
  `TowPricingRule_ID` INT NOT NULL AUTO_INCREMENT,
  `TowCompany_ID` INT NULL,
  `TowFromLocation` VARCHAR(45) NOT NULL,
  `TowToLocation` VARCHAR(45) NOT NULL,
  `Area_ID` INT NULL,
  `TowPrice` DECIMAL(6,2) NOT NULL,
  PRIMARY KEY (`TowPricingRule_ID`));

INSERT INTO `TowPricingRule` (`TowCompany_ID`,`TowFromLocation`,`TowToLocation`,`Area_ID`,`TowPrice`) VALUES
-- From Customer to Lot
-- CS Towing
(2,'Customer','LOT',1,60.00),
(2,'Customer','LOT',2,60.00),
(2,'Customer','LOT',4,60.00),

(2,'Customer','LOT',3,70.00),
(2,'Customer','LOT',5,70.00),

-- Walter
(3,'Customer','LOT',1,40.00),
(3,'Customer','LOT',4,40.00),

(3,'Customer','LOT',2,60.00),

(3,'Customer','LOT',3,70.00),
(3,'Customer','LOT',5,70.00),

-- Lopez
(4,'Customer','LOT',1,40.00),
(4,'Customer','LOT',4,40.00),

(4,'Customer','LOT',2,60.00),

(4,'Customer','LOT',3,70.00),
(4,'Customer','LOT',5,70.00),

-- from Lot
(null,'LOT','LKQ',null,10.00),
(null,'LOT','TMR',null,10.00),
(null,'LOT','PAR',null,60.00);
